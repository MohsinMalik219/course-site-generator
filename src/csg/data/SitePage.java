package csg.data;

import csg.CSG;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a page for the site.
 * @author Mohsin
 */
public class SitePage implements Comparable<SitePage> {
    
    private static final String[] HTML_FILES = {"index.html", "syllabus.html", "schedule.html", "hws.html", "projects.html"};
    private static final String[] JS_FILES = {"HomeBuilder.js", "SyllabusBuilder.js", "ScheduleBuilder.js", "HWsBuilder.js", "ProjectsBuilder.js"};
    private static final String[] NAMES = {"Home", "Syllabus", "Schedule", "HWs", "Projects"};
    
    private StringProperty html;
    private StringProperty js;
    private StringProperty name;
    private BooleanProperty use;
    private int index;
    
    public SitePage(int index, boolean use, CSG csg) {
        if(index < 0 || index >= HTML_FILES.length) {
            html = js = name = null;
            this.use = null;
        } else {
            html = new SimpleStringProperty(HTML_FILES[index]);
            js = new SimpleStringProperty(JS_FILES[index]);
            name = new SimpleStringProperty(NAMES[index]);
            this.use = new SimpleBooleanProperty(true);
        }
        this.index = index;
        this.use.setValue(use);
        if(csg != null) {
            this.use.addListener(e -> {
                csg.getDataHandler().setAsChanged();
            });
        }
    }
    
    public StringProperty htmlProperty() {
        return html;
    }
    
    public StringProperty jsProperty() {
        return js;
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    
    public BooleanProperty useProperty() {
        return use;
    }

    public int getIndex() {
        return index;
    }
    
    /**
     * Gets the array index of the provided HTML file name.
     * @param html The HTML file name.
     * @return -1 if the file is not proper. Otherwise the array index of the HTML file name.
     */
    public static int getFileNameIndex(String html) {
        if(!html.endsWith(".html")) {
            return -1;
        }
        int i = 0;
        for(String s : HTML_FILES) {
            if(s.equals(html)) {
                return i;
            }
            i++;
        }
        return -1;
    }
    
    /**
     * Gets the array index of the provided JS file name.
     * @param js The JS file name.
     * @return -1 if the file is not proper. Otherwise the array index of the JS file name.
     */
    public static int getJSFileIndex(String js) {
        if(js.endsWith(".js")) {
            int i = 0;
            for(String s : JS_FILES) {
                if(s.equals(js)) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    @Override
    public int compareTo(SitePage page) {
        if(index == page.index) {
            return 0;
        } else if(index < page.index) {
            return -1;
        } else {
            return 1;
        }
    }
    
}