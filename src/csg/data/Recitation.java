/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a recitation.
 * @author Mohsin
 */
public class Recitation {
    
    private StringProperty section;
    private StringProperty inst;
    private StringProperty dayTime;
    private StringProperty location;
    private StringProperty ta1;
    private StringProperty ta2;
    
    public Recitation(String section, String inst, String dayTime, String location, String ta1, String ta2) {
        this.section = new SimpleStringProperty(section);
        this.inst = new SimpleStringProperty(inst);
        this.dayTime = new SimpleStringProperty(dayTime);
        this.location = new SimpleStringProperty(location);
        this.ta1 = new SimpleStringProperty(ta1);
        this.ta2 = new SimpleStringProperty(ta2);
    }
    
    public StringProperty sectionProperty() {
        return section;
    }
    
    public StringProperty instProperty() {
        return inst;
    }
    
    public StringProperty dayTimeProperty() {
        return dayTime;
    }
    
    public StringProperty locationProperty() {
        return location;
    }
    
    public StringProperty ta1Property() {
        return ta1;
    }
    
    public StringProperty ta2Property() {
        return ta2;
    }
    
}