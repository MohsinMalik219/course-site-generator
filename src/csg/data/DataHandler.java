package csg.data;

import csg.CSG;
import csg.CSGProp;
import csg.actions.SavedRecitation;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

/**
 * This class deals with storing data for the entire application.
 * @author Mohsin
 */
public class DataHandler {
    
    private CSG csg;
    private static final int DEFAULT_START = 8;
    private static final int DEFAULT_END = 20;
    private List<ScheduleItem> allScheduleItems;
    private String savePath = null;
    private boolean changeMade = false;
    
    // Course data
    private StringProperty exportDir;
    private StringProperty templateDir;
    private StringProperty title;
    private StringProperty instName;
    private StringProperty instHome;
    private StringProperty subject;
    private StringProperty semester;
    private StringProperty number;
    private StringProperty year;
    private StringProperty stylesheet;
    private String bannerImagePath;
    private String leftImagePath;
    private String rightImagePath;
    private ObservableList<SitePage> pages;
    
    // TA data
    private Map<String, StringProperty> officeHourStringProps;
    private ObservableList<TeachingAssistant> tas;
    private int startHour;
    private int endHour;
    
    // Recitation data
    private ObservableList<Recitation> recitations;
    
    // Schedule data
    private ObjectProperty<LocalDate> startingMonday;
    private ObjectProperty<LocalDate> endingFriday;
    private ObservableList<ScheduleItem> scheduleItems;
    
    // Projects data
    private ObservableList<Student> students;
    private ObservableList<Team> teams;
    
    public DataHandler() {
        this(null);
    }
    
    public DataHandler(CSG csg) {
        this.csg = csg;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        allScheduleItems = new ArrayList<>();
        
        // Course data
        exportDir = new SimpleStringProperty(props.getProperty(CSGProp.NONE_SELECTED));
        templateDir = new SimpleStringProperty(props.getProperty(CSGProp.NONE_SELECTED));
        title = new SimpleStringProperty("");
        instName = new SimpleStringProperty("");
        instHome = new SimpleStringProperty("");
        subject = new SimpleStringProperty("");
        semester = new SimpleStringProperty("");
        number = new SimpleStringProperty("");
        year = new SimpleStringProperty("");
        stylesheet = new SimpleStringProperty("");
        bannerImagePath = "";
        leftImagePath = "";
        rightImagePath = "";
        pages = FXCollections.observableArrayList();
        
        // TA data
        officeHourStringProps = new HashMap<>();
        tas = FXCollections.observableArrayList();
        startHour = DEFAULT_START;
        endHour = DEFAULT_END;
        
        // Recitation data
        recitations = FXCollections.observableArrayList();
        
        // Schedule data
        startingMonday = new SimpleObjectProperty<>();
        endingFriday = new SimpleObjectProperty<>();
        scheduleItems = FXCollections.observableArrayList();
        
        // Project data
        students = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
    }
    
    /**
     * Resets all data back to default.
     */
    public void resetData() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String noneSelected = props.getProperty(CSGProp.NONE_SELECTED);
        
        savePath = null;
        
        title.setValue("");
        instName.setValue("");
        instHome.setValue("");
        subject.setValue("");
        number.setValue("");
        semester.setValue("");
        year.setValue("");
        exportDir.setValue(noneSelected);
        templateDir.setValue(noneSelected);
        pages.clear();
        bannerImagePath = "";
        leftImagePath = "";
        rightImagePath = "";
        stylesheet.setValue("");
        
        setStartAndEndHour(DEFAULT_START, DEFAULT_END);
        officeHourStringProps.clear();
        tas.clear();
        
        recitations.clear();
        
        startingMonday.setValue(null);
        endingFriday.setValue(null);
        scheduleItems.clear();
        allScheduleItems.clear();
        
        students.clear();
        teams.clear();
    }
    
    /**
     * Gets the recitation with the section
     * @param section
     * @return 
     */
    public Recitation getRecitation(String section) {
        for(Recitation recitation : recitations) {
            if(recitation.sectionProperty().getValue().equals(section)) {
                return recitation;
            }
        }
        return null;
    }
    
    /**
     * Gets the schedule item with the specified ID.
     * @param id The ID of the schedule item.
     * @return A schedule item.
     */
    public ScheduleItem getScheduleItem(int id) {
        for(ScheduleItem item : scheduleItems) {
            if(item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    
    public boolean isScheduleDateValid(LocalDate date) {
        return !((startingMonday.getValue() != null && date.compareTo(startingMonday.getValue()) < 0) || (endingFriday.getValue() != null && date.compareTo(endingFriday.getValue()) > 0));
    }
    
    /**
     * Removes ScheduleItem objects if they are outside the starting and ending dates.
     */
    public void fixScheduleTable() {
        for(ScheduleItem item : allScheduleItems) {
            updateScheduleItem(item);
        }
    }
    
    public void removeScheduleItem(ScheduleItem item) {
        if(allScheduleItems.contains(item)) {
            allScheduleItems.remove(item);
        }
        if(scheduleItems.contains(item)) {
            scheduleItems.remove(item);
        }
    }
    
    public void addScheduleItem(ScheduleItem item) {
        if(!allScheduleItems.contains(item)) {
            allScheduleItems.add(item);
        }
        if(isScheduleDateValid(stringToLocalDate(item.dateProperty().getValue())) && !scheduleItems.contains(item)) {
            scheduleItems.add(item);
        }
    }
    
    /**
     * Updates the item as to if it should be viewed or not.
     * @param item The item to update.
     */
    public void updateScheduleItem(ScheduleItem item) {
        LocalDate date = stringToLocalDate(item.dateProperty().getValue());
        boolean scheduleDateValid = isScheduleDateValid(date);
        if(scheduleDateValid && !scheduleItems.contains(item)) {
            scheduleItems.add(item);
        } else if(!scheduleDateValid && scheduleItems.contains(item)) {
            scheduleItems.remove(item);
        }
    }
    
    /**
     * Gets a student based on the given first name and last name.
     * @param firstName The students first name.
     * @param lastName The students last name.
     * @return The student.
     */
    public Student getStudent(String firstName, String lastName) {
        for(Student student : students) {
            if(student.firstNameProperty().getValue().equals(firstName) && student.lastNameProperty().getValue().equals(lastName)) {
                return student;
            }
        }
        return null;
    }
    
    public boolean willScheduleItemsBeRemoved() {
        for(ScheduleItem scheduleItem : scheduleItems) {
            LocalDate localDate = stringToLocalDate(scheduleItem.dateProperty().getValue());
            if(!isScheduleDateValid(localDate)) {
                return true;
            }
        }
        return false;
    }
    
    public List<ScheduleItem> removeConflictingScheduleItems() {
        List<ScheduleItem> itemsRemoved = new ArrayList<>();
        for(ScheduleItem scheduleItem : scheduleItems) {
            LocalDate localDate = stringToLocalDate(scheduleItem.dateProperty().getValue());
            if(!isScheduleDateValid(localDate)) {
                itemsRemoved.add(scheduleItem);
            }
        }
        for(ScheduleItem scheduleItem : itemsRemoved) {
            scheduleItems.remove(scheduleItem);
        }
        return itemsRemoved;
    }
    
    public LocalDate stringToLocalDate(String dateString) {
        String[] date = dateString.split("/");
        return LocalDate.of(Integer.valueOf(date[2]), Integer.valueOf(date[0]), Integer.valueOf(date[1]));
    }
    
    public Team getTeam(String name) {
        for(Team team : teams) {
            if(team.nameProperty().getValue().equals(name)) {
                return team;
            }
        }
        return null;
    }
    
    public boolean willStudentsGetRemoved(Team team) {
        for(Student student : students) {
            if(student.teamProperty().getValue().equals(team.nameProperty().getValue())) {
                return true;
            }
        }
        return false;
    }
    
    public List<Student> removeStudents(Team team) {
        List<Student> toRemove = new ArrayList<>();
        for(Student student : students) {
            if(student.teamProperty().getValue().equals(team.nameProperty().getValue())) {
                toRemove.add(student);
            }
        }
        for(Student student : toRemove) {
            students.remove(student);
        }
        return toRemove;
    }
    
    public void updateStudents(String oldTeamName, String newTeamName) {
        for(Student student : students) {
            if(student.teamProperty().getValue().equals(oldTeamName)) {
                student.teamProperty().setValue(newTeamName);
            }
        }
    }
    
    /**
     * Checks if the given JS file name will be used when exporting.
     * @param jsFileName The JS file name.
     * @return true if the JS file should be exported. Otherwise false.
     */
    public boolean isJSFileUsed(String jsFileName) {
        int index = SitePage.getJSFileIndex(jsFileName);
        if(index == -1) {
            return false;
        }
        for(SitePage page : pages) {
            if(page.getIndex() == index && page.useProperty().getValue()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if a template is valid.
     * @param path The path to the template root directory.
     * @return true if the template contains at least an index.html file. Otherwise false.
     */
    public boolean isTemplateValid(String path) {
        File file = new File(path);
        if(file.exists() && file.isDirectory()) {
            for(File f : file.listFiles()) {
                String fileName = f.getName();
                int index = SitePage.getFileNameIndex(fileName);
                if(index == 0) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Replaces a TAs name in all recitations with another name.
     * @param ta The original TAs name.
     * @param replace The name to replace with.
     * @return A list of the recitations which have been replaced.
     */
    public List<SavedRecitation> replaceTAInRecitation(String ta, String replace) {
        List<SavedRecitation> savedRecitations = new ArrayList<>();
        for(Recitation recitation : this.recitations) {
            boolean add = false;
            if(recitation.ta1Property().getValue().equals(ta)) {
                recitation.ta1Property().setValue(replace);
                savedRecitations.add(new SavedRecitation(recitation.sectionProperty().getValue(), true));
            } else if(recitation.ta2Property().getValue().equals(ta)) {
                recitation.ta2Property().setValue(replace);
                savedRecitations.add(new SavedRecitation(recitation.sectionProperty().getValue(), false));
            }
        }
        return savedRecitations;
    }
    
    /**
     * Adds the TA to the specified cell.
     * @param name The TAs name.
     * @param key The cell key.
     */
    public void addTAToCell(String name, String key) {
        StringProperty textProp = officeHourStringProps.get(key);
        
        String text = textProp.getValue();
        if(text == null || text.equals("")) {
            textProp.setValue(name);
        } else {
            textProp.setValue(text + "\n" + name);
        }
    }
    
    /**
     * Removes the TA to the specified cell.
     * @param name The TAs name.
     * @param key The cell key.
     */
    public void removeTAFromCell(String name, String key) {
        StringProperty textProp = officeHourStringProps.get(key);
        String text = textProp.getValue();
        if(text == null) {
            return;
        }
        if(text.equals(name)) {
            textProp.setValue("");
        } else if(text.contains("\n" + name)) {
            textProp.setValue(text.replace("\n" + name, ""));
        } else if(text.contains(name + "\n")) {
            textProp.setValue(text.replace(name + "\n", ""));
        }
    }
    
    /**
     * Removes the specified TA from all cells.
     * @param name The TAs name.
     */
    public void removeTAFromAllCells(String name) {
        for(String key : officeHourStringProps.keySet()) {
            removeTAFromCell(name, key);
        }
    }
    
    /**
     * Sets the start and end time then rebuilds the grid.
     * @param start The new start time.
     * @param end The new end time.
     */
    public void setStartAndEndHour(int start, int end) {
        startHour = start;
        endHour = end;
        // If the workspace is active, build the grid.
        if(csg != null) {
            csg.getWorkspaceHandler().getTaTab().buildOfficeHoursGrid(startHour, endHour);
            
        }
        // If it is not active, we are testing data loading/saving.
        // We create the office hours grid string properties.
        else {
            for(int i = 1; i <= getNumRows(); i ++) {
                for(int j = 2; j < 7; j++) {
                    String key = buildKey(j, i);
                    officeHourStringProps.put(key, new SimpleStringProperty());
                }
            }
        }
    }
    
    public String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    /**
     * Gets the number of rows in the grid.
     * @return Number of rows in grid.
     */
    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }
    
    /**
     * Adds a new undergraduate TA.
     * @param name The name of the TA.
     * @param email The email of the TA.
     */
    public void addTA(String name, String email) {
        addTA(name, email, true);
    }
    
    /**
     * Adds a new TA.
     * @param name The name of the TA.
     * @param email The email of the TA.
     * @param undergrad true if the TA is an undergraduate student. Otherwise false.
     */
    public void addTA(String name, String email, boolean undergrad) {
        final TeachingAssistant ta = new TeachingAssistant(name, email, undergrad, csg);
        tas.add(ta);
    }
    
    /**
     * Removes the TA.
     * @param name The name of the TA.
     */
    public void removeTA(String name) {
        TeachingAssistant ta = getTA(name);
        if(ta != null) {
            tas.remove(ta);
        }
    }
    
    /**
     * Gets the specified TA.
     * @param name The name of the TA.
     * @return The TA specified.
     */
    public TeachingAssistant getTA(String name) {
        for(TeachingAssistant ta : tas) {
            if(ta.nameProperty().getValue().equalsIgnoreCase(name)) {
                return ta;
            }
        }
        return null;
    }
    
    public void setAsChanged() {
        changeMade = true;
        if(savePath != null) {
            csg.getWorkspaceHandler().getSaveButton().setDisable(false);
        }
    }
    
    /**
     * Gets the number of rows (excluding the header row).
     * @return The number of rows.
     */
    public int getNumberOfRows() {
        return (endHour - startHour) * 2;
    }

    public String getBannerImagePath() {
        return bannerImagePath;
    }

    public String getLeftImagePath() {
        return leftImagePath;
    }

    public String getRightImagePath() {
        return rightImagePath;
    }

    public ObservableList<SitePage> getPages() {
        return pages;
    }

    public Map<String, StringProperty> getOfficeHourStringProps() {
        return officeHourStringProps;
    }

    public ObservableList<TeachingAssistant> getTas() {
        return tas;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public ObservableList<Recitation> getRecitations() {
        return recitations;
    }

    public ObservableList<ScheduleItem> getScheduleItems() {
        return scheduleItems;
    }

    public ObservableList<Student> getStudents() {
        return students;
    }

    public ObservableList<Team> getTeams() {
        return teams;
    }
    
    public StringProperty exportDirProperty() {
        return exportDir;
    }
    
    public StringProperty templateDirProperty() {
        return templateDir;
    }
    
    public StringProperty titleProperty() {
        return title;
    }
    
    public StringProperty instNameProperty() {
        return instName;
    }
    
    public StringProperty instHomeProperty() {
        return instHome;
    }
    
    public StringProperty subjectProperty() {
        return subject;
    }
    
    public StringProperty numberProperty() {
        return number;
    }
    
    public StringProperty yearProperty() {
        return year;
    }
    
    public StringProperty semesterProperty() {
        return semester;
    }
    
    public StringProperty stylesheetProperty() {
        return stylesheet;
    }
    
    public ObjectProperty<LocalDate> startingMondayProperty() {
        return startingMonday;
    }
    
    public ObjectProperty<LocalDate> endingFridayProperty() {
        return endingFriday;
    }

    public List<ScheduleItem> getAllScheduleItems() {
        return allScheduleItems;
    }

    public String getSavePath() {
        return savePath;
    }

    public boolean isChangeMade() {
        return changeMade;
    }

    public void setChangeMade(boolean changeMade) {
        this.changeMade = changeMade;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public void setBannerImagePath(String bannerImagePath) {
        this.bannerImagePath = bannerImagePath;
    }

    public void setLeftImagePath(String leftImagePath) {
        this.leftImagePath = leftImagePath;
    }

    public void setRightImagePath(String rightImagePath) {
        this.rightImagePath = rightImagePath;
    }
    
}