/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a 
 * @author Mohsin
 */
public class ScheduleItem {
    
    public static enum ScheduleItemType {HOLIDAY, LECTURE, REFERENCE, RECITATION, HOMEWORK};
    private StringProperty type;
    private StringProperty date;
    private StringProperty title;
    private StringProperty topic;
    private StringProperty time;
    private StringProperty link;
    private StringProperty criteria;
    private int id;
    private static int currentID = 0;
    
    public ScheduleItem(int id, ScheduleItemType type, String date, String title, String topic, String time, String link, String criteria) {
        this.id = id;
        this.type = new SimpleStringProperty(type.toString());
        this.date = new SimpleStringProperty(date);
        this.title = new SimpleStringProperty(title);
        this.topic = new SimpleStringProperty(topic);
        this.time = new SimpleStringProperty(time);
        this.link = new SimpleStringProperty(link);
        this.criteria = new SimpleStringProperty(criteria);
    }
    
    public ScheduleItem(ScheduleItemType type, String date, String title, String topic, String time, String link, String criteria) {
        this(currentID++, type, date, title, topic, time, link, criteria);
    }
    
    public StringProperty typeProperty() {
        return type;
    }
    
    public StringProperty dateProperty() {
        return date;
    }
    
    public StringProperty titleProperty() {
        return title;
    }
    
    public StringProperty topicProperty() {
        return topic;
    }
    
    public StringProperty timeProperty() {
        return time;
    }
    
    public StringProperty linkProperty() {
        return link;
    }
    
    public StringProperty criteriaProperty() {
        return criteria;
    }

    public ScheduleItemType getScheduleItemType() {
        return ScheduleItemType.valueOf(type.getValue());
    }

    public int getId() {
        return id;
    }
    
}