/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Mohsin
 */
public class Team {
    
    private StringProperty name;
    private StringProperty color;
    private StringProperty textColor;
    private StringProperty link;
    
    public Team(String name, String color, String textColor, String link) {
        this.name = new SimpleStringProperty(name);
        this.color = new SimpleStringProperty(color);
        this.textColor = new SimpleStringProperty(textColor);
        this.link = new SimpleStringProperty(link);
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    
    public StringProperty colorProperty() {
        return color;
    }
    
    public StringProperty textColorProperty() {
        return textColor;
    }
    
    public StringProperty linkProperty() {
        return link;
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
    
}