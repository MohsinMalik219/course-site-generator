package csg.data;

import csg.CSG;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a teaching assistant.
 * @author Mohsin
 */
public class TeachingAssistant {
    
    private StringProperty name;
    private StringProperty email;
    private BooleanProperty undergrad;
    
    public TeachingAssistant(String name, String email, boolean undergrad, CSG csg) {
        this.name = new SimpleStringProperty(name);
        this.email = new SimpleStringProperty(email);
        this.undergrad = new SimpleBooleanProperty(undergrad);
        if(csg != null) {
            this.undergrad.addListener(e -> {
                csg.getDataHandler().setAsChanged();
            });
        }
    }
    
    public BooleanProperty undergradProperty() {
        return undergrad;
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
    
}