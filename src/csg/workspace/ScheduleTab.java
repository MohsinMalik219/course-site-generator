package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import csg.data.ScheduleItem.ScheduleItemType;
import java.time.LocalDate;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * Represents the Schedule tab.
 * @author Mohsin
 */
public class ScheduleTab extends Tab {
    
    private CSG csg;
    private LocalDate previousStartingMonday;
    private LocalDate previousEndingFriday;
    private boolean propertyMonday = false;
    private boolean propertyFriday = false;
    
    private Pane schedulePane;
    private Label scheduleHeaderLabel;
    
    // Calendar Boundaries section
    private Pane calendarBoundariesPane;
    private Label calendarBoundariesHeader;
    private Label startingMondayLabel;
    private Label endingFridayLabel;
    private DatePicker startingMondayDatePicker;
    private DatePicker endingFridayDatePicker;
    
    // Schedule Items section
    private Pane scheduleItemsPane;
    private TableView scheduleTable;
    private Label scheduleItemsHeader;
    private Label addEditLabel;
    private Label typeLabel;
    private Label dateLabel;
    private Label timeLabel;
    private Label titleLabel;
    private Label topicLabel;
    private Label linkLabel;
    private Label criteriaLabel;
    private ComboBox typeComboBox;
    private DatePicker dateDatePicker;
    private TextField timeTextField;
    private TextField titleTextField;
    private TextField topicTextField;
    private TextField linkTextField;
    private TextField criteriaTextField;
    private Button deleteButton;
    private Button submitButton;
    private Button clearButton;
    
    public ScheduleTab(CSG csg, String title) {
        super(title);
        this.csg = csg;
        setClosable(false);
        
        schedulePane = buildSchedulePane();
        calendarBoundariesPane = buildCalendarBoundariesPane();
        scheduleItemsPane = buildScheduleItemsPane();
        
        VBox vbox = new VBox(schedulePane, calendarBoundariesPane, scheduleItemsPane);
        vbox.setSpacing(20);
        
        ScrollPane scrollPane = new ScrollPane(vbox);
        scrollPane.setPadding(new Insets(10, 10, 10, 10));
        
        setContent(scrollPane);
    }
    
    private Pane buildSchedulePane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        scheduleHeaderLabel = new Label(props.getProperty(CSGProp.SCHEDULE_HEADER));
        
        return new Pane(scheduleHeaderLabel);
    }
    
    private Pane buildCalendarBoundariesPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        calendarBoundariesHeader = new Label(props.getProperty(CSGProp.CALENDAR_BOUNDARIES_HEADER));
        startingMondayLabel = new Label(props.getProperty(CSGProp.CALENDAR_BOUNDARIES_STARTING_MONDAY));
        endingFridayLabel = new Label(props.getProperty(CSGProp.CALENDAR_BOUNDARIES_ENDING_FRIDAY));
        startingMondayDatePicker = new DatePicker();
        endingFridayDatePicker = new DatePicker();
        
        HBox monday = new HBox(startingMondayLabel, startingMondayDatePicker);
        HBox friday = new HBox(endingFridayLabel, endingFridayDatePicker);
        monday.setSpacing(5);
        friday.setSpacing(5);
        HBox datePickers = new HBox(monday, friday);
        datePickers.setSpacing(30);
        VBox vbox = new VBox(calendarBoundariesHeader, datePickers);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        startingMondayDatePicker.valueProperty().bindBidirectional(csg.getDataHandler().startingMondayProperty());
        endingFridayDatePicker.valueProperty().bindBidirectional(csg.getDataHandler().endingFridayProperty());
        
        return new Pane(vbox);
    }
    
    private Pane buildScheduleItemsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        scheduleItemsHeader = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_HEADER));
        addEditLabel = new Label(props.getProperty(CSGProp.ADD_EDIT));
        typeLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_TYPE));
        dateLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_DATE));
        timeLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_TIME));
        titleLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_TITLE));
        topicLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_TOPIC));
        linkLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_LINK));
        criteriaLabel = new Label(props.getProperty(CSGProp.SCHEDULE_ITEM_CRITERIA));
        
        typeComboBox = new ComboBox();
        dateDatePicker = new DatePicker();
        timeTextField = new TextField();
        titleTextField = new TextField();
        topicTextField = new TextField();
        linkTextField = new TextField();
        criteriaTextField = new TextField();
        
        submitButton = new Button(props.getProperty(CSGProp.BUTTON_ADD));
        clearButton = new Button(props.getProperty(CSGProp.BUTTON_CLEAR));
        deleteButton = new Button(props.getProperty(CSGProp.BUTTON_DELETE));
        
        scheduleTable = new TableView();
        
        criteriaLabel.setPadding(new Insets(0, 60, 0, 0));
        
        for(ScheduleItemType type : ScheduleItemType.values()) {
            typeComboBox.getItems().add(type.toString());
        }
        
        List<String> tableHeaders = props.getPropertyOptionsList(CSGProp.SCHEDULE_TABLE_HEADERS);
        int i = 0;
        TableColumn col1 = new TableColumn(tableHeaders.get(i++));
        TableColumn col2 = new TableColumn(tableHeaders.get(i++));
        TableColumn col3 = new TableColumn(tableHeaders.get(i++));
        TableColumn col4 = new TableColumn(tableHeaders.get(i++));
        TableColumn col5 = new TableColumn(tableHeaders.get(i++));
        TableColumn col6 = new TableColumn(tableHeaders.get(i++));
        TableColumn col7 = new TableColumn(tableHeaders.get(i++));
        scheduleTable.getColumns().addAll(col1, col2, col3, col4, col5, col6, col7);
        scheduleTable.setMinWidth(1350);
        
        double width = 1350D / 7D;
        col1.setPrefWidth(width);
        col2.setPrefWidth(width);
        col3.setPrefWidth(width);
        col4.setPrefWidth(width);
        col5.setPrefWidth(width);
        col6.setPrefWidth(width);
        col7.setPrefWidth(width);
        
        col1.setCellValueFactory(new PropertyValueFactory("type"));
        col2.setCellValueFactory(new PropertyValueFactory("date"));
        col3.setCellValueFactory(new PropertyValueFactory("title"));
        col4.setCellValueFactory(new PropertyValueFactory("topic"));
        col5.setCellValueFactory(new PropertyValueFactory("time"));
        col6.setCellValueFactory(new PropertyValueFactory("link"));
        col7.setCellValueFactory(new PropertyValueFactory("criteria"));
        
        scheduleTable.setItems(csg.getDataHandler().getScheduleItems());
        
        HBox header = new HBox(scheduleItemsHeader, deleteButton);
        header.setSpacing(5);
        HBox buttons = new HBox(submitButton, clearButton);
        buttons.setSpacing(5);
        
        GridPane dataInput = new GridPane();
        dataInput.add(addEditLabel, 0, 0);
        
        dataInput.add(typeLabel, 0, 1);
        dataInput.add(dateLabel, 0, 2);
        dataInput.add(timeLabel, 0, 3);
        dataInput.add(titleLabel, 0, 4);
        dataInput.add(topicLabel, 0, 5);
        dataInput.add(linkLabel, 0, 6);
        dataInput.add(criteriaLabel, 0, 7);
        dataInput.add(buttons, 0, 8);
        
        dataInput.add(typeComboBox, 1, 1);
        dataInput.add(dateDatePicker, 1, 2);
        dataInput.add(timeTextField, 1, 3);
        dataInput.add(titleTextField, 1, 4);
        dataInput.add(topicTextField, 1, 5);
        dataInput.add(linkTextField, 1, 6);
        dataInput.add(criteriaTextField, 1, 7);
        
        VBox upper = new VBox(header, scheduleTable);
        upper.setSpacing(5);
        
        VBox vbox = new VBox(upper, dataInput);
        vbox.setSpacing(20);
        
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        submitButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleSubmit();
        });
        
        clearButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleClear();
        });
        
        scheduleTable.setOnMouseClicked(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleTableClick();
        });
        
        deleteButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleRemoveButton();
        });
        
        scheduleTable.setOnKeyPressed(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleKeyPress(e.getCode());
        });
        
        startingMondayDatePicker.setOnAction(e -> {
            if(propertyMonday) {
                propertyMonday = false;
                return;
            }
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleStartingMondayChange();
        });
        
        endingFridayDatePicker.setOnAction(e -> {
            if(propertyFriday) {
                propertyFriday = false;
                return;
            }
            csg.getWorkspaceHandler().getControllerHandler().getScheduleController().handleEndingFridayChange();
        });
        
        return new Pane(vbox);
    }
    
    // ALL GETTERS

    public Pane getSchedulePane() {
        return schedulePane;
    }

    public Label getScheduleHeaderLabel() {
        return scheduleHeaderLabel;
    }

    public Pane getCalendarBoundariesPane() {
        return calendarBoundariesPane;
    }

    public Label getCalendarBoundariesHeader() {
        return calendarBoundariesHeader;
    }

    public Label getStartingMondayLabel() {
        return startingMondayLabel;
    }

    public Label getEndingFridayLabel() {
        return endingFridayLabel;
    }

    public DatePicker getStartingMondayDatePicker() {
        return startingMondayDatePicker;
    }

    public DatePicker getEndingFridayDatePicker() {
        return endingFridayDatePicker;
    }

    public Pane getScheduleItemsPane() {
        return scheduleItemsPane;
    }

    public TableView getScheduleTable() {
        return scheduleTable;
    }

    public Label getScheduleItemsHeader() {
        return scheduleItemsHeader;
    }

    public Label getAddEditLabel() {
        return addEditLabel;
    }

    public Label getTypeLabel() {
        return typeLabel;
    }

    public Label getDateLabel() {
        return dateLabel;
    }

    public Label getTimeLabel() {
        return timeLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getTopicLabel() {
        return topicLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Label getCriteriaLabel() {
        return criteriaLabel;
    }

    public ComboBox getTypeComboBox() {
        return typeComboBox;
    }

    public DatePicker getDateDatePicker() {
        return dateDatePicker;
    }

    public TextField getTimeTextField() {
        return timeTextField;
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }

    public TextField getTopicTextField() {
        return topicTextField;
    }

    public TextField getLinkTextField() {
        return linkTextField;
    }

    public TextField getCriteriaTextField() {
        return criteriaTextField;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Button getSubmitButton() {
        return submitButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public LocalDate getPreviousStartingMonday() {
        return previousStartingMonday;
    }

    public LocalDate getPreviousEndingFriday() {
        return previousEndingFriday;
    }

    public void setPreviousStartingMonday(LocalDate previousStartingMonday) {
        this.previousStartingMonday = previousStartingMonday;
    }

    public void setPreviousEndingFriday(LocalDate previousEndingFriday) {
        this.previousEndingFriday = previousEndingFriday;
    }

    public void setPropertyMonday(boolean propertyMonday) {
        this.propertyMonday = propertyMonday;
    }

    public void setPropertyFriday(boolean propertyFriday) {
        this.propertyFriday = propertyFriday;
    }
    
}