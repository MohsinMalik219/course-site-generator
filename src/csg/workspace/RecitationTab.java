package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * Represents the Recitations tab.
 * @author Mohsin
 */
public class RecitationTab extends Tab {
    
    private CSG csg;
    
    // Recitations section
    private Pane recitationsPane;
    private Label recitationsHeaderLabel;
    private TableView recitationsTable;
    private Button recitationsDeleteButton;
    
    // Add/Edit section
    private Pane addEditPane;
    private Label addEditHeaderLabel;
    private Label addEditSectionLabel;
    private Label addEditInstLabel;
    private Label addEditDayTimeLabel;
    private Label addEditLocationLabel;
    private Label addEditTAOneLabel;
    private Label addEditTATwoLabel;
    private TextField addEditSectionTextField;
    private TextField addEditInstTextField;
    private TextField addEditDayTimeTextField;
    private TextField addEditLocationTextField;
    private ComboBox addEditTAOneComboBox;
    private ComboBox addEditTATwoComboBox;
    private Button addEditSubmitButton;
    private Button addEditClearButton;
    
    public RecitationTab(CSG csg, String title) {
        super(title);
        this.csg = csg;
        setClosable(false);
        
        recitationsPane = buildRecitationsPane();
        addEditPane = buildAddEditPane();
        
        VBox vbox = new VBox(recitationsPane, addEditPane);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.setSpacing(20);
        setContent(new ScrollPane(vbox));
    }
    
    /**
     * Builds the pane for the Recitations section.
     * @return The Recitations section pane.
     */
    private Pane buildRecitationsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        recitationsHeaderLabel = new Label(props.getProperty(CSGProp.RECITATIONS_HEADER));
        recitationsDeleteButton = new Button(props.getProperty(CSGProp.BUTTON_DELETE));
        recitationsTable = new TableView();
        
        List<String> tableHeaders = props.getPropertyOptionsList(CSGProp.RECITATIONS_TABLE_HEADERS);
        int i = 0;
        TableColumn col1 = new TableColumn(tableHeaders.get(i++));
        TableColumn col2 = new TableColumn(tableHeaders.get(i++));
        TableColumn col3 = new TableColumn(tableHeaders.get(i++));
        TableColumn col4 = new TableColumn(tableHeaders.get(i++));
        TableColumn col5 = new TableColumn(tableHeaders.get(i++));
        TableColumn col6 = new TableColumn(tableHeaders.get(i++));
        
        recitationsTable.getColumns().addAll(col1, col2, col3, col4, col5, col6);
        recitationsTable.setMinWidth(1000);
        
        col1.setPrefWidth(200);
        col2.setPrefWidth(200);
        col3.setPrefWidth(200);
        col4.setPrefWidth(200);
        col5.setPrefWidth(200);
        col6.setPrefWidth(200);
        
        col1.setCellValueFactory(new PropertyValueFactory("section"));
        col2.setCellValueFactory(new PropertyValueFactory("inst"));
        col3.setCellValueFactory(new PropertyValueFactory("dayTime"));
        col4.setCellValueFactory(new PropertyValueFactory("location"));
        col5.setCellValueFactory(new PropertyValueFactory("ta1"));
        col6.setCellValueFactory(new PropertyValueFactory("ta2"));
        
        recitationsTable.setItems(csg.getDataHandler().getRecitations());
        
        HBox header = new HBox(recitationsHeaderLabel, recitationsDeleteButton);
        VBox vbox = new VBox(header, recitationsTable);
        
        header.setSpacing(5);
        vbox.setSpacing(5);
        
        recitationsTable.setOnMouseClicked(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getRecitationController().handleTableClick();
        });
        
        recitationsTable.setOnKeyPressed(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getRecitationController().handleKeyPress(e.getCode());
        });
        
        recitationsDeleteButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getRecitationController().handleRemoveRecitation();
        });
        
        return new Pane(vbox);
    }
    
    /**
     * Builds the pane for the Add/Edit section
     * @return The Add/Edit section pane.
     */
    private Pane buildAddEditPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        List<String> headers = props.getPropertyOptionsList(CSGProp.RECITATIONS_TABLE_HEADERS);
                
        addEditHeaderLabel = new Label(props.getProperty(CSGProp.ADD_EDIT));
        addEditSectionLabel = new Label(headers.get(0) + ":");
        addEditInstLabel = new Label(headers.get(1) + ":");
        addEditDayTimeLabel = new Label(headers.get(2) + ":");
        addEditLocationLabel = new Label(headers.get(3) + ":");
        String supervisingTaText = props.getProperty(CSGProp.RECITATIONS_SUPERVISING_TA);
        addEditTAOneLabel = new Label(supervisingTaText);
        addEditTATwoLabel = new Label(supervisingTaText);
        
        addEditSectionTextField = new TextField();
        addEditInstTextField = new TextField();
        addEditDayTimeTextField = new TextField();
        addEditLocationTextField = new TextField();
        addEditTAOneComboBox = new ComboBox();
        addEditTATwoComboBox = new ComboBox();
        
        addEditSubmitButton = new Button(props.getProperty(CSGProp.BUTTON_ADD));
        addEditClearButton = new Button(props.getProperty(CSGProp.BUTTON_CLEAR));
        
        /*addEditTAOneComboBox.setCellFactory(new Callback<ListView<TeachingAssistant>, ListCell<TeachingAssistant>>() {
            @Override
            public ListCell<TeachingAssistant> call(ListView<TeachingAssistant> p) {
                
                final ListCell<TeachingAssistant> cell = new ListCell<TeachingAssistant>(){
                    @Override
                    protected void updateItem(TeachingAssistant ta, boolean bln) {
                        super.updateItem(ta, bln);
                        if(ta != null) {
                            setText("");
                        }
                    }
                };
                
                return cell;
            }
        });*/
        
        addEditTAOneComboBox.setItems(csg.getDataHandler().getTas());
        addEditTATwoComboBox.setItems(csg.getDataHandler().getTas());
        
        GridPane gridPane = new GridPane();
        HBox hbox = new HBox(addEditSubmitButton, addEditClearButton);
        hbox.setSpacing(5);
        
        // Place items in first column
        gridPane.add(addEditSectionLabel, 0, 0);
        gridPane.add(addEditInstLabel, 0, 1);
        gridPane.add(addEditDayTimeLabel, 0, 2);
        gridPane.add(addEditLocationLabel, 0, 3);
        gridPane.add(addEditTAOneLabel, 0, 4);
        gridPane.add(addEditTATwoLabel, 0, 5);
        gridPane.add(hbox, 0, 6);
        
        // Place items in second column
        gridPane.add(addEditSectionTextField, 1, 0);
        gridPane.add(addEditInstTextField, 1, 1);
        gridPane.add(addEditDayTimeTextField, 1, 2);
        gridPane.add(addEditLocationTextField, 1, 3);
        gridPane.add(addEditTAOneComboBox, 1, 4);
        gridPane.add(addEditTATwoComboBox, 1, 5);
        
        addEditTAOneLabel.setPadding(new Insets(0, 20, 0, 0));
        
        VBox vbox = new VBox(addEditHeaderLabel, gridPane);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        addEditSubmitButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getRecitationController().handleSubmit();
        });
        
        addEditClearButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getRecitationController().handleClear();
        });
        
        return new Pane(vbox);
    }
    
    // ALL GETTERS

    public Pane getRecitationsPane() {
        return recitationsPane;
    }

    public Label getRecitationsHeaderLabel() {
        return recitationsHeaderLabel;
    }

    public TableView getRecitationsTable() {
        return recitationsTable;
    }

    public Button getRecitationsDeleteButton() {
        return recitationsDeleteButton;
    }

    public Pane getAddEditPane() {
        return addEditPane;
    }

    public Label getAddEditHeaderLabel() {
        return addEditHeaderLabel;
    }

    public Label getAddEditSectionLabel() {
        return addEditSectionLabel;
    }

    public Label getAddEditInstLabel() {
        return addEditInstLabel;
    }

    public Label getAddEditDayTimeLabel() {
        return addEditDayTimeLabel;
    }

    public Label getAddEditLocationLabel() {
        return addEditLocationLabel;
    }

    public Label getAddEditTAOneLabel() {
        return addEditTAOneLabel;
    }

    public Label getAddEditTATwoLabel() {
        return addEditTATwoLabel;
    }

    public TextField getAddEditSectionTextField() {
        return addEditSectionTextField;
    }

    public TextField getAddEditInstTextField() {
        return addEditInstTextField;
    }

    public TextField getAddEditDayTimeTextField() {
        return addEditDayTimeTextField;
    }

    public TextField getAddEditLocationTextField() {
        return addEditLocationTextField;
    }

    public ComboBox getAddEditTAOneComboBox() {
        return addEditTAOneComboBox;
    }

    public ComboBox getAddEditTATwoComboBox() {
        return addEditTATwoComboBox;
    }

    public Button getAddEditSubmitButton() {
        return addEditSubmitButton;
    }

    public Button getAddEditClearButton() {
        return addEditClearButton;
    }
    
}