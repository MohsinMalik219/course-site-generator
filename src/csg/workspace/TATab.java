package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * Represents the TA tab.
 * @author Mohsin
 */
public class TATab extends Tab {
    
    private CSG csg;
    private boolean firstTime = true;
    
    // Teaching Assistant section
    private Pane taPane;
    private Label taHeaderLabel;
    private Button taRemoveButton;
    private TableView taTable;
    private TextField taNameTextField;
    private TextField taEmailTextField;
    private Button taSubmitButton;
    private Button taClearButton;
    
    // Office Hours section
    private ScrollPane officeHoursPane;
    private Label officeHoursHeaderLabel;
    private Label startTimeLabel;
    private Label endTimeLabel;
    private ComboBox startTimeComboBox;
    private ComboBox endTimeComboBox;
    private GridPane officeHoursGrid;
    private Map<String, Pane> officeHoursGridHeaderPanes;
    private Map<String, Label> officeHoursGridHeaderLabels;
    private Map<String, Pane> officeHoursGridTimePanes;
    private Map<String, Label> officeHoursGridTimeLabels;
    private Map<String, Pane> officeHoursGridSlotPanes;
    private Map<String, Label> officeHoursGridSlotLabels;
    
    private static int test = 0;
    
    /**
     * Constructs a new TATab.
     * @param csg The Application.
     * @param title The title for the tab.
     */
    public TATab(CSG csg, String title) {
        super(title);
        this.csg = csg;
        setClosable(false);
        taPane = buildTeachingAssistantsPane();
        officeHoursPane = buildOfficeHoursPane();
        Insets inset = new Insets(10, 10, 10, 10);
        officeHoursPane.setPadding(inset);
        SplitPane splitPane = new SplitPane(taPane, officeHoursPane);
        splitPane.setDividerPositions(.45);
        setContent(splitPane);
    }
    
    /**
     * Builds the Pane for the Teaching Assistants section.
     * @return The Teaching Assistants Pane.
     */
    private Pane buildTeachingAssistantsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        taHeaderLabel = new Label(props.getProperty(CSGProp.TA_HEADER));
        taRemoveButton = new Button(props.getProperty(CSGProp.BUTTON_DELETE));
        taTable = new TableView();
        taNameTextField = new TextField();
        taEmailTextField = new TextField();
        taSubmitButton = new Button(props.getProperty(CSGProp.BUTTON_ADD));
        taClearButton = new Button(props.getProperty(CSGProp.BUTTON_CLEAR));
        
        taNameTextField.setPromptText(props.getProperty(CSGProp.TA_NAME));
        taEmailTextField.setPromptText(props.getProperty(CSGProp.TA_EMAIL));
        
        taTable.setMinWidth(540);
        
        //taRemoveButton.setPadding(new Insets(10, 20, 10, 20));
        
        HBox header = new HBox(taHeaderLabel, taRemoveButton);
        header.setSpacing(5);
        
        HBox textFieldAndButtons = new HBox(taNameTextField, taEmailTextField, taSubmitButton, taClearButton);
        textFieldAndButtons.setSpacing(5);
        
        List<String> tableHeaders = props.getPropertyOptionsList(CSGProp.TA_TABLE_HEADERS);
        int i = 0;
        TableColumn col1 = new TableColumn(tableHeaders.get(i++));
        TableColumn col2 = new TableColumn(tableHeaders.get(i++));
        TableColumn col3 = new TableColumn(tableHeaders.get(i++));
        
        
        col1.setCellValueFactory(new PropertyValueFactory("undergrad"));
        col2.setCellValueFactory(new PropertyValueFactory("name"));
        col3.setCellValueFactory(new PropertyValueFactory("email"));
        
        col1.setCellFactory(CheckBoxTableCell.forTableColumn(col1));
        
        taTable.getColumns().addAll(col1, col2, col3);
        taTable.setItems(csg.getDataHandler().getTas());
        taTable.setEditable(true);
        col1.setEditable(true);
        
        col1.setPrefWidth(95);
        col2.setPrefWidth(175);
        col3.setPrefWidth(270);
        
        VBox vbox = new VBox(header, taTable, textFieldAndButtons);
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        
        taSubmitButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleTASubmit();
        });
        taTable.setOnMouseClicked(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleTableClick();
        });
        taClearButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleClearButton();
        });
        taRemoveButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleTARemove();
        });
        taTable.setOnKeyPressed(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleKeyPress(e.getCode());
        });
        
        return new Pane(vbox);
    }
    
    /**
     * Builds the Pane for the Office Hours section.
     * @return The Office Hours pane.
     */
    private ScrollPane buildOfficeHoursPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        officeHoursHeaderLabel = new Label(props.getProperty(CSGProp.OFFICE_HOURS_HEADER));
        startTimeLabel = new Label(props.getProperty(CSGProp.OFFICE_HOURS_START_TIME));
        endTimeLabel = new Label(props.getProperty(CSGProp.OFFICE_HOURS_END_TIME));
        startTimeComboBox = new ComboBox();
        endTimeComboBox = new ComboBox();
        officeHoursGrid = new GridPane();
        officeHoursGridHeaderLabels = new HashMap<>();
        officeHoursGridHeaderPanes = new HashMap<>();
        officeHoursGridTimeLabels = new HashMap<>();
        officeHoursGridTimePanes = new HashMap<>();
        officeHoursGridSlotLabels = new HashMap<>();
        officeHoursGridSlotPanes = new HashMap<>();
        
        HBox startTime = new HBox(startTimeLabel, startTimeComboBox);
        startTime.setSpacing(5);
        
        HBox endTime = new HBox(endTimeLabel, endTimeComboBox);
        endTime.setSpacing(5);
        
        HBox header = new HBox(officeHoursHeaderLabel, startTime, endTime);
        header.setSpacing(25);
        
        for(int i = 0; i < 24; i++) {
            String format;
            if(i < 12) {
                format = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_AM);
            } else {
                format = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_PM);
            }
            int hour = militaryToStandard(i);
            String time = hour + format;
            startTimeComboBox.getItems().add(time);
            endTimeComboBox.getItems().add(time);
        }
        
        int startHour = csg.getDataHandler().getStartHour();
        int endHour = csg.getDataHandler().getEndHour();
        
        startTimeComboBox.setValue(startTimeComboBox.getItems().get(startHour));
        endTimeComboBox.setValue(endTimeComboBox.getItems().get(endHour));
        buildOfficeHoursGrid(startHour, endHour);
        
        VBox vbox = new VBox();
        vbox.getChildren().add(header);
        vbox.getChildren().add(officeHoursGrid);
        vbox.setSpacing(10);
        
        startTimeComboBox.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleGridTimeChange();
        });
        endTimeComboBox.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getTaController().handleGridTimeChange();
        });
        
        return new ScrollPane(vbox);
    }
    
    /**
     * Builds the office hours grid, resetting all cells.
     * @param startHour The start hour in military time.
     * @param endHour The end hour in military time.
     */
    public void buildOfficeHoursGrid(int startHour, int endHour) {
        // Clear everything out of the pane.
        officeHoursGrid.getChildren().clear();
        officeHoursGridHeaderLabels.clear();
        officeHoursGridHeaderPanes.clear();
        officeHoursGridTimeLabels.clear();
        officeHoursGridTimePanes.clear();
        officeHoursGridSlotLabels.clear();
        officeHoursGridSlotPanes.clear();
        csg.getDataHandler().getOfficeHourStringProps().clear();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        Insets headerPadding = new Insets(10, 20, 10, 20);
        Insets timePadding = new Insets(5, 10, 5, 10);
        Insets slotPadding = new Insets(5, 20, 5, 20);
        
        // Build the headers first.
        List<String> gridHeaders = props.getPropertyOptionsList(CSGProp.OFFICE_HOURS_GRID_HEADERS);
        for(int i = 0; i < 7; i++) {
            Pane pane = new Pane();
            Label label = new Label(gridHeaders.get(i));
            pane.getChildren().add(label);
            label.setPadding(headerPadding);
            String key = buildKey(i, 0);
            officeHoursGridHeaderLabels.put(key, label);
            officeHoursGridHeaderPanes.put(key, pane);
            officeHoursGrid.add(pane, i, 0);
        }
        
        String amTime = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_AM);
        String pmTime = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_PM);
        String amTimeHalf = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_HALF_AM);
        String pmTimeHalf = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_HALF_PM);
        
        // Then the times on the side.
        int row = 1;
        for(int i = startHour; i < endHour; i++) {
            int hour = militaryToStandard(i);
            String startTime;
            String endTime;
            String endTimeHalf;
            if(i <= 11) {
                startTime = hour + amTime;
                endTime = hour + amTimeHalf;
                if(i == 11) {
                    endTimeHalf = "12" + pmTime;
                } else {
                    endTimeHalf = ((hour + 1) % 12) + amTime;
                }
            } else {
                startTime = hour + pmTime;
                endTime = hour + pmTimeHalf;
                if(i == 23) {
                    endTimeHalf = "12" + amTime;
                } else {
                    endTimeHalf = ((hour + 1) % 12) + pmTime;
                }
            }
            
            Label startTimeLabel = new Label(startTime);
            Label endTimeLabel = new Label(endTime);
            Pane startTimePane = new Pane(startTimeLabel);
            Pane endTimePane = new Pane(endTimeLabel);
            
            startTimeLabel.setPadding(timePadding);
            endTimeLabel.setPadding(timePadding);
            
            officeHoursGrid.add(startTimePane, 0, row);
            officeHoursGrid.add(endTimePane, 1, row);
            
            officeHoursGridTimePanes.put(buildKey(0, row), startTimePane);
            officeHoursGridTimePanes.put(buildKey(1, row), endTimePane);
            officeHoursGridTimeLabels.put(buildKey(0, row), startTimeLabel);
            officeHoursGridTimeLabels.put(buildKey(1, row), endTimeLabel);
            
            row++;
            
            Label startTimeHalfLabel = new Label(endTime);
            Label endTimeHalfLabel = new Label(endTimeHalf);
            Pane startTimeHalfPane = new Pane(startTimeHalfLabel);
            Pane endTimeHalfPane = new Pane(endTimeHalfLabel);
            
            startTimeHalfLabel.setPadding(timePadding);
            endTimeHalfLabel.setPadding(timePadding);
            
            officeHoursGrid.add(startTimeHalfPane, 0, row);
            officeHoursGrid.add(endTimeHalfPane, 1, row);
            
            officeHoursGridTimePanes.put(buildKey(0, row), startTimeHalfPane);
            officeHoursGridTimePanes.put(buildKey(1, row), endTimeHalfPane);
            officeHoursGridTimeLabels.put(buildKey(0, row), startTimeHalfLabel);
            officeHoursGridTimeLabels.put(buildKey(1, row), endTimeHalfLabel);
            
            row++;
        }
        
        // Finally create empty panes and labels for the time slots.
        int numRows = csg.getDataHandler().getNumberOfRows();
        Map<String, StringProperty> stringProps = csg.getDataHandler().getOfficeHourStringProps();
        for(int i = 1; i <= numRows; i++) {
            for(int j = 2; j < 7; j++) {
                final String key = buildKey(j, i);
                Pane pane = new Pane();
                Label label = new Label("");
                StringProperty stringProp = new SimpleStringProperty();
                label.textProperty().bind(stringProp);
                stringProps.put(key, stringProp);
                pane.getChildren().add(label);
                label.setPadding(slotPadding);
                officeHoursGrid.add(pane, j, i);
                officeHoursGridSlotPanes.put(key, pane);
                officeHoursGridSlotLabels.put(key, label);
                
                pane.setOnMouseClicked(e -> {
                    csg.getWorkspaceHandler().getControllerHandler().getTaController().handleToggleOfficeHours(key);
                });
                pane.setOnMouseEntered(e -> {
                    changeBackgroundColors(key, "-fx-background-color: #d3d3d3;");
                });
                pane.setOnMouseExited(e -> {
                    changeBackgroundColors(key, "-fx-background-color: #f4f4f4;");
                });
            }
        }
        
        if(firstTime) {
            firstTime = false;
        } else {
            csg.getStyleHandler().styleGrid();
        }
        
    }
    
    private void changeBackgroundColors(String key, String style) {
        int row = getRow(key);
        int col = getCol(key);
        
        for(int i = 1; i < row; i++) {
            String thisKey = buildKey(col, i);
            officeHoursGridSlotPanes.get(thisKey).setStyle(style);
        }
        
        for(int i = 0; i < 2; i++) {
            String thisKey = buildKey(i, row);
            officeHoursGridTimePanes.get(thisKey).setStyle(style);
        }
        
        for(int i = 2; i <= col; i++) {
            String thisKey = buildKey(i, row);
            officeHoursGridSlotPanes.get(thisKey).setStyle(style);
        }
    }
    
    /**
     * Returns a key for the grid.
     * @param col The column.
     * @param row The row.
     * @return The key as a String.
     */
    private String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    /**
     * Converts the given hour from military to standard.
     * @param hour The hour in military.
     * @return The hour in standard.
     */
    private int militaryToStandard(int hour) {
        if(hour == 0) {
            return 12;
        }
        if(hour <= 12) {
            return hour;
        } else {
            return hour - 12;
        }
    }
    
    private String militaryToStandardString(int hour) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String format;
        if(hour <= 11) {
            format = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_AM);
        } else {
            format = props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_PM);
        }
        return militaryToStandard(hour) + format;
    }
    
    public void updateComboBoxes() {
        int start = csg.getDataHandler().getStartHour();
        int end = csg.getDataHandler().getEndHour();
        startTimeComboBox.getSelectionModel().select(militaryToStandardString(start));
        endTimeComboBox.getSelectionModel().select(militaryToStandardString(end));
    }
    
    private int getCol(String key) {
        return Integer.valueOf(key.split("_")[0]);
    }
    
    private int getRow(String key) {
        return Integer.valueOf(key.split("_")[1]);
    }
    
    // ALL GETTERS

    public Pane getTaPane() {
        return taPane;
    }

    public Label getTaHeaderLabel() {
        return taHeaderLabel;
    }

    public Button getTaRemoveButton() {
        return taRemoveButton;
    }

    public TableView getTaTable() {
        return taTable;
    }

    public TextField getTaNameTextField() {
        return taNameTextField;
    }

    public TextField getTaEmailTextField() {
        return taEmailTextField;
    }

    public Button getTaSubmitButton() {
        return taSubmitButton;
    }

    public Button getTaClearButton() {
        return taClearButton;
    }

    public ScrollPane getOfficeHoursPane() {
        return officeHoursPane;
    }

    public Label getOfficeHoursHeaderLabel() {
        return officeHoursHeaderLabel;
    }

    public Label getStartTimeLabel() {
        return startTimeLabel;
    }

    public Label getEndTimeLabel() {
        return endTimeLabel;
    }

    public ComboBox getStartTimeComboBox() {
        return startTimeComboBox;
    }

    public ComboBox getEndTimeComboBox() {
        return endTimeComboBox;
    }

    public GridPane getOfficeHoursGrid() {
        return officeHoursGrid;
    }

    public Map<String, Pane> getOfficeHoursGridHeaderPanes() {
        return officeHoursGridHeaderPanes;
    }

    public Map<String, Label> getOfficeHoursGridHeaderLabels() {
        return officeHoursGridHeaderLabels;
    }

    public Map<String, Pane> getOfficeHoursGridTimePanes() {
        return officeHoursGridTimePanes;
    }

    public Map<String, Label> getOfficeHoursGridTimeLabels() {
        return officeHoursGridTimeLabels;
    }

    public Map<String, Pane> getOfficeHoursGridSlotPanes() {
        return officeHoursGridSlotPanes;
    }

    public Map<String, Label> getOfficeHoursGridSlotLabels() {
        return officeHoursGridSlotLabels;
    }
    
}