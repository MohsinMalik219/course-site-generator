package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Calendar;
import static java.util.Calendar.YEAR;
import java.util.List;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * Represents the Course Info tab.
 * @author Mohsin
 */
public class CourseTab extends Tab {
    
    private CSG csg;
    
    // Course Info box
    private Pane courseInfoPane;
    private ComboBox subjectComboBox;
    private ComboBox courseNumberComboBox;
    private ComboBox semesterComboBox;
    private ComboBox yearComboBox;
    private TextField titleTextField;
    private TextField instNameTextField;
    private TextField instHomeTextField;
    private Label exportDirLabel;
    private Label courseInfoHeaderLabel;
    private Label subjectLabel;
    private Label semesterLabel;
    private Label courseNumberLabel;
    private Label yearLabel;
    private Label titleLabel;
    private Label instNameLabel;
    private Label instHomeLabel;
    private Label exportDirDescLabel;
    private Button exportDirChangeButton;
    
    // Site Template box
    private Pane siteTemplatePane;
    private Label siteTemplateHeaderLabel;
    private Label siteTemplateDescLabel;
    private Label siteTemplateDirLabel;
    private Label sitePagesLabel;
    private Button siteTemplateSelectButton;
    private TableView sitePagesTable;
    
    // Page Style box
    private Pane pageStylePane;
    private Label pageStyleHeaderLabel;
    private Label bannerSchoolImageLabel;
    private Label leftFooterImageLabel;
    private Label rightFooterImageLabel;
    private Label leftImageNoneLabel;
    private Label bannerImageNoneLabel;
    private Label rightImageNoneLabel;
    private Label pageStyleStylesheetLabel;
    private Label pageStyleNoteLabel;
    private Pane bannerSchoolImagePane;
    private Pane leftFooterImagePane;
    private Pane rightFooterImagePane;
    private ImageView bannerImageView;
    private ImageView leftImageView;
    private ImageView rightImageView;
    private Button bannerSchoolImageChangeButton;
    private Button leftFooterImageChangeButton;
    private Button rightFooterImageChangeButton;
    private ComboBox pageStyleStylesheetComboBox;
    
    public CourseTab(CSG csg, String title) {
        super(title);
        this.csg = csg;
        setClosable(false);
        courseInfoPane = buildCourseInfoSectionPane();
        siteTemplatePane = buildSiteTemplateSectionPane();
        pageStylePane = buildPageStyleSectionPane();
        
        VBox vbox = new VBox(courseInfoPane, siteTemplatePane, pageStylePane);
        vbox.setPadding(new Insets(10, 0, 10, 10));
        vbox.setSpacing(20);
        ScrollPane scrollPane = new ScrollPane(vbox);
        setContent(scrollPane);
    }
    
    private Pane buildCourseInfoSectionPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // Initialize controls
        subjectComboBox = new ComboBox();
        courseNumberComboBox = new ComboBox();
        semesterComboBox = new ComboBox();
        yearComboBox = new ComboBox();
        titleTextField = new TextField();
        instNameTextField = new TextField();
        instHomeTextField = new TextField();
        exportDirLabel = new Label();
        exportDirChangeButton = new Button(props.getProperty(CSGProp.BUTTON_CHANGE));
        
        subjectComboBox.getItems().addAll("CSE", "ITS", "ISE");
        
        List<String> semesters = props.getPropertyOptionsList(CSGProp.SEMESTERS);
        int i = 0;
        semesterComboBox.getItems().addAll(semesters.get(i++), semesters.get(i++), semesters.get(i++), semesters.get(i++));
        courseNumberComboBox.getItems().addAll(219 + "", 308 + "", 380 + "", 102 + "");
        
        int year = Calendar.getInstance().get(YEAR);
        for(i = year + 2; i >= 1990; i--) {
            yearComboBox.getItems().add(i + "");
        }
        
        subjectComboBox.valueProperty().bindBidirectional(csg.getDataHandler().subjectProperty());
        courseNumberComboBox.valueProperty().bindBidirectional(csg.getDataHandler().numberProperty());
        semesterComboBox.valueProperty().bindBidirectional(csg.getDataHandler().semesterProperty());
        yearComboBox.valueProperty().bindBidirectional(csg.getDataHandler().yearProperty());
        
        titleTextField.textProperty().bindBidirectional(csg.getDataHandler().titleProperty());
        instNameTextField.textProperty().bindBidirectional(csg.getDataHandler().instNameProperty());
        instHomeTextField.textProperty().bindBidirectional(csg.getDataHandler().instHomeProperty());
        
        StringProperty exportDirProp = csg.getDataHandler().exportDirProperty();
        exportDirLabel.textProperty().bind(exportDirProp);
        exportDirProp.setValue(props.getProperty(CSGProp.NONE_SELECTED));
        
        subjectLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_SUBJECT));
        semesterLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_SEMESTER));
        courseNumberLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_NUMBER));
        yearLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_YEAR));
        titleLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_TITLE));
        instNameLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_INST_NAME));
        instHomeLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_INST_HOME));
        exportDirDescLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_EXPORT_DIR));
        
        exportDirDescLabel.setPadding(new Insets(5, 0, 5, 0));
        
        instNameLabel.setPadding(new Insets(0, 50, 0, 0));
        
        GridPane grid = new GridPane();
        grid.add(subjectLabel, 0, 0);
        grid.add(semesterLabel, 0, 1);
        grid.add(courseNumberLabel, 0, 2);
        grid.add(yearLabel, 0, 3);
        grid.add(titleLabel, 0, 4);
        grid.add(instNameLabel, 0, 5);
        grid.add(instHomeLabel, 0, 6);
        grid.add(exportDirDescLabel, 0, 7);
        
        grid.add(subjectComboBox, 1, 0);
        grid.add(semesterComboBox, 1, 1);
        grid.add(courseNumberComboBox, 1, 2);
        grid.add(yearComboBox, 1, 3);
        grid.add(titleTextField, 1, 4);
        grid.add(instNameTextField, 1, 5);
        grid.add(instHomeTextField, 1, 6);
        grid.add(exportDirLabel, 1, 7);
        
        grid.add(exportDirChangeButton, 1, 8);
        
        courseInfoHeaderLabel = new Label(props.getProperty(CSGProp.COURSE_INFO_HEADER));
        
        VBox vbox = new VBox(courseInfoHeaderLabel, grid);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        exportDirChangeButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getCourseController().handleExportDirChange();
        });
        
        subjectComboBox.setOnAction(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        semesterComboBox.setOnAction(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        yearComboBox.setOnAction(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        courseNumberComboBox.setOnAction(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        titleTextField.setOnKeyTyped(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        instHomeTextField.setOnKeyTyped(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        instNameTextField.setOnKeyTyped(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        return new Pane(vbox);
    }
    
    private Pane buildSiteTemplateSectionPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        siteTemplateHeaderLabel = new Label(props.getProperty(CSGProp.SITE_TEMPLATE_HEADER));
        siteTemplateDescLabel = new Label(props.getProperty(CSGProp.SITE_TEMPLATE_DESC));
        siteTemplateDirLabel = new Label();
        siteTemplateDirLabel.textProperty().bind(csg.getDataHandler().templateDirProperty());
        siteTemplateSelectButton = new Button(props.getProperty(CSGProp.SITE_TEMPLATE_SELECT_TEMPLATE_BUTTON));
        sitePagesLabel = new Label(props.getProperty(CSGProp.SITE_PAGES));
        sitePagesTable = new TableView();
        
        List<String> sitePagesTableHeaders = props.getPropertyOptionsList(CSGProp.SITE_PAGES_TABLE_HEADERS);
        
        TableColumn col1 = new TableColumn(sitePagesTableHeaders.get(0));
        TableColumn col2 = new TableColumn(sitePagesTableHeaders.get(1));
        TableColumn col3 = new TableColumn(sitePagesTableHeaders.get(2));
        TableColumn col4 = new TableColumn(sitePagesTableHeaders.get(3));
        
        sitePagesTable.getColumns().addAll(col1, col2, col3, col4);
        sitePagesTable.setMaxWidth(700);
        
        double width = 700D / 4D;
        col1.setPrefWidth(width);
        col2.setPrefWidth(width);
        col3.setPrefWidth(width);
        col4.setPrefWidth(width);
        
        col1.setCellValueFactory(new PropertyValueFactory("use"));
        col2.setCellValueFactory(new PropertyValueFactory("name"));
        col3.setCellValueFactory(new PropertyValueFactory("html"));
        col4.setCellValueFactory(new PropertyValueFactory("js"));
        
        col1.setCellFactory(CheckBoxTableCell.forTableColumn(col1));
        col1.setEditable(true);
        
        sitePagesTable.setEditable(true);
        sitePagesTable.setItems(csg.getDataHandler().getPages());
        
        VBox vbox = new VBox(siteTemplateHeaderLabel, siteTemplateDescLabel, siteTemplateDirLabel,
                siteTemplateSelectButton, sitePagesLabel, sitePagesTable);
        
        vbox.setPadding(new Insets(20, 20, 20, 20));
        vbox.setSpacing(8);
        
        siteTemplateSelectButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getCourseController().handleTemplateDirChange();
        });
        
        return new Pane(vbox);
    }
    
    private Pane buildPageStyleSectionPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        pageStyleHeaderLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_HEADER));
        bannerSchoolImageLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_BANNER_SCHOOL_IMAGE));
        leftFooterImageLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_LEFT_FOOTER_IMAGE));
        rightFooterImageLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_RIGHT_FOOTER_IMAGE));
        String changeText = props.getProperty(CSGProp.BUTTON_CHANGE);
        bannerSchoolImageChangeButton = new Button(changeText);
        leftFooterImageChangeButton = new Button(changeText);
        rightFooterImageChangeButton = new Button(changeText);
        pageStyleStylesheetLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_STYLESHEET));
        pageStyleStylesheetComboBox = new ComboBox();
        pageStyleNoteLabel = new Label(props.getProperty(CSGProp.PAGE_STYLE_NOTE));
        bannerSchoolImagePane = new Pane();
        leftFooterImagePane = new Pane();
        rightFooterImagePane = new Pane();
        bannerImageView = new ImageView();
        rightImageView = new ImageView();
        leftImageView = new ImageView();
        
        String noneSelectedText = props.getProperty(CSGProp.NONE_SELECTED);
        rightImageNoneLabel = new Label(noneSelectedText);
        bannerImageNoneLabel = new Label(noneSelectedText);
        leftImageNoneLabel = new Label(noneSelectedText);
        
        File file = new File("work/css");
        for(File cssFile : file.listFiles()) {
            String fileName = cssFile.getName();
            if(fileName.endsWith(".css")) {
                pageStyleStylesheetComboBox.getItems().add(fileName);
            }
        }
        pageStyleStylesheetComboBox.valueProperty().bindBidirectional(csg.getDataHandler().stylesheetProperty());
        
        Insets inset = new Insets(0, 50, 0, 50);
        
        rightImageNoneLabel.setPadding(inset);
        bannerImageNoneLabel.setPadding(inset);
        leftImageNoneLabel.setPadding(inset);
        
        bannerSchoolImagePane.getChildren().add(bannerImageNoneLabel);
        rightFooterImagePane.getChildren().add(rightImageNoneLabel);
        leftFooterImagePane.getChildren().add(leftImageNoneLabel);
        
        GridPane imageGrid = new GridPane();
        imageGrid.add(bannerSchoolImageLabel, 0, 0);
        imageGrid.add(bannerSchoolImagePane, 1, 0);
        imageGrid.add(bannerSchoolImageChangeButton, 2, 0);
        imageGrid.add(leftFooterImageLabel, 0, 1);
        imageGrid.add(leftFooterImagePane, 1, 1);
        imageGrid.add(leftFooterImageChangeButton, 2, 1);
        imageGrid.add(rightFooterImageLabel, 0, 2);
        imageGrid.add(rightFooterImagePane, 1, 2);
        imageGrid.add(rightFooterImageChangeButton, 2, 2);
        
        HBox hbox = new HBox(pageStyleStylesheetLabel, pageStyleStylesheetComboBox);
        hbox.setSpacing(30);
        
        VBox vbox = new VBox(pageStyleHeaderLabel, imageGrid, hbox, pageStyleNoteLabel);
        //vbox.setPadding(new Insets(0, 0, 5, 0));
        vbox.setPadding(new Insets(20, 20, 20, 20));
        vbox.setSpacing(8);
        
        bannerSchoolImageChangeButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getCourseController().handleBannerChange();
        });
        
        leftFooterImageChangeButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getCourseController().handleLeftImageChange();
        });
        
        rightFooterImageChangeButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getCourseController().handleRightImageChange();
        });
       
        pageStyleStylesheetComboBox.setOnAction(e -> {
            csg.getDataHandler().setAsChanged();
        });
        
        return new Pane(vbox);
    }
    
    /**
     * Removes the image in the pane and makes it a label again.
     */
    public void resetImages() {
        resetBanner();
        resetLeftFooter();
        resetRightFooter();
    }
    
    public void resetBanner() {
        bannerSchoolImagePane.getChildren().clear();
        bannerSchoolImagePane.getChildren().add(bannerImageNoneLabel);
    }
    
    public void resetLeftFooter() {
        leftFooterImagePane.getChildren().clear();
        leftFooterImagePane.getChildren().add(leftImageNoneLabel);
    }
    
    public void resetRightFooter() {
        rightFooterImagePane.getChildren().clear();
        rightFooterImagePane.getChildren().add(rightImageNoneLabel);
    }
    
    /**
     * Sets the banners image according to the URL.
     */
    public void setBanner() {
        String url = csg.getDataHandler().getBannerImagePath();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(!url.equals(props.getProperty(CSGProp.NONE_SELECTED))) {
            File file = new File(url);
            if(file.exists()) {
                if(isFileImage(file)) {
                    Image image = null;
                    try {
                        image = new Image(file.toURI().toURL().toString());
                    } catch (MalformedURLException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    bannerImageView.setImage(image);
                    bannerSchoolImagePane.getChildren().clear();
                    bannerSchoolImagePane.getChildren().add(bannerImageView);
                } else {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.FILE_NOT_IMAGE_HEADER), props.getProperty(CSGProp.FILE_NOT_IMAGE_TEXT));
                }
            }
        }
    }
    
    public void setLeftImage() {
        String url = csg.getDataHandler().getLeftImagePath();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(!url.equals(props.getProperty(CSGProp.NONE_SELECTED))) {
            File file = new File(url);
            if(file.exists()) {
                if(isFileImage(file)) {
                    Image image = null;
                    try {
                        image = new Image(file.toURI().toURL().toString());
                    } catch (MalformedURLException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    leftImageView.setImage(image);
                    leftFooterImagePane.getChildren().clear();
                    leftFooterImagePane.getChildren().add(leftImageView);
                } else {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.FILE_NOT_IMAGE_HEADER), props.getProperty(CSGProp.FILE_NOT_IMAGE_TEXT));
                }
            }
        }
    }
    
    public void setRightImage() {
        String url = csg.getDataHandler().getRightImagePath();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(!url.equals(props.getProperty(CSGProp.NONE_SELECTED))) {
            File file = new File(url);
            if(file.exists()) {
                if(isFileImage(file)) {
                    Image image = null;
                    try {
                        image = new Image(file.toURI().toURL().toString());
                    } catch (MalformedURLException ex) {
                        ex.printStackTrace();
                        return;
                    }
                    rightImageView.setImage(image);
                    rightFooterImagePane.getChildren().clear();
                    rightFooterImagePane.getChildren().add(rightImageView);
                } else {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.FILE_NOT_IMAGE_HEADER), props.getProperty(CSGProp.FILE_NOT_IMAGE_TEXT));
                }
            }
        }
    }
    
    private boolean isFileImage(File file) {
        //return new MimetypesFileTypeMap().getContentType(file).substring(0, 5).equalsIgnoreCase("image");
        return true;
    }
    
    // ALL GETTERS

    public Pane getCourseInfoPane() {
        return courseInfoPane;
    }

    public ComboBox getSubjectComboBox() {
        return subjectComboBox;
    }

    public ComboBox getCourseNumberComboBox() {
        return courseNumberComboBox;
    }

    public ComboBox getSemesterComboBox() {
        return semesterComboBox;
    }

    public ComboBox getYearComboBox() {
        return yearComboBox;
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }

    public TextField getInstNameTextField() {
        return instNameTextField;
    }

    public TextField getInstHomeTextField() {
        return instHomeTextField;
    }

    public Label getExportDirLabel() {
        return exportDirLabel;
    }

    public Label getCourseInfoHeaderLabel() {
        return courseInfoHeaderLabel;
    }

    public Label getSubjectLabel() {
        return subjectLabel;
    }

    public Label getSemesterLabel() {
        return semesterLabel;
    }

    public Label getCourseNumberLabel() {
        return courseNumberLabel;
    }

    public Label getYearLabel() {
        return yearLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getInstNameLabel() {
        return instNameLabel;
    }

    public Label getInstHomeLabel() {
        return instHomeLabel;
    }

    public Label getExportDirDescLabel() {
        return exportDirDescLabel;
    }

    public Button getExportDirChangeButton() {
        return exportDirChangeButton;
    }

    public Pane getSiteTemplatePane() {
        return siteTemplatePane;
    }

    public Label getSiteTemplateHeaderLabel() {
        return siteTemplateHeaderLabel;
    }

    public Label getSiteTemplateDescLabel() {
        return siteTemplateDescLabel;
    }

    public Label getSiteTemplateDirLabel() {
        return siteTemplateDirLabel;
    }

    public Label getSitePagesLabel() {
        return sitePagesLabel;
    }

    public Button getSiteTemplateSelectButton() {
        return siteTemplateSelectButton;
    }

    public TableView getSitePagesTable() {
        return sitePagesTable;
    }

    public Pane getPageStylePane() {
        return pageStylePane;
    }

    public Label getPageStyleHeaderLabel() {
        return pageStyleHeaderLabel;
    }

    public Label getBannerSchoolImageLabel() {
        return bannerSchoolImageLabel;
    }

    public Label getLeftFooterImageLabel() {
        return leftFooterImageLabel;
    }

    public Label getRightFooterImageLabel() {
        return rightFooterImageLabel;
    }

    public Label getPageStyleStylesheetLabel() {
        return pageStyleStylesheetLabel;
    }

    public Label getPageStyleNoteLabel() {
        return pageStyleNoteLabel;
    }

    public Pane getBannerSchoolImagePane() {
        return bannerSchoolImagePane;
    }

    public Pane getLeftFooterImagePane() {
        return leftFooterImagePane;
    }

    public Pane getRightFooterImagePane() {
        return rightFooterImagePane;
    }

    public Button getBannerSchoolImageChangeButton() {
        return bannerSchoolImageChangeButton;
    }

    public Button getLeftFooterImageChangeButton() {
        return leftFooterImageChangeButton;
    }

    public Button getRightFooterImageChangeButton() {
        return rightFooterImageChangeButton;
    }

    public ComboBox getPageStyleStylesheetComboBox() {
        return pageStyleStylesheetComboBox;
    }

    public ImageView getBannerImageView() {
        return bannerImageView;
    }

    public ImageView getLeftImageView() {
        return leftImageView;
    }

    public ImageView getRightImageView() {
        return rightImageView;
    }

    public Label getLeftImageNoneLabel() {
        return leftImageNoneLabel;
    }

    public Label getBannerImageNoneLabel() {
        return bannerImageNoneLabel;
    }

    public Label getRightImageNoneLabel() {
        return rightImageNoneLabel;
    }
    
}