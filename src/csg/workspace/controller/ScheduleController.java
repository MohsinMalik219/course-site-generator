package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.actions.AddScheduleItem;
import csg.actions.ChangeEndingFriday;
import csg.actions.ChangeStartingMonday;
import csg.actions.EditScheduleItem;
import csg.actions.RemoveScheduleItem;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import csg.workspace.ScheduleTab;
import java.time.DayOfWeek;
import java.time.LocalDate;
import javafx.scene.input.KeyCode;
import properties_manager.PropertiesManager;

/**
 * Represents all method which are called when controls in the Schedule tab are
 * used.
 * @author Mohsin
 */
public class ScheduleController {
    
    private CSG csg;
    
    public ScheduleController(CSG csg) {
        this.csg = csg;
    }
    
    public void handleClear() {
        clear();
    }
    
    public void handleSubmit() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String addText = props.getProperty(CSGProp.BUTTON_ADD);
        String updateText = props.getProperty(CSGProp.BUTTON_UPDATE);
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        Object obj = tab.getTypeComboBox().getValue();
        String typeText = "";
        if(obj != null) {
            typeText = (String) obj;
        }
        String date = "";
        LocalDate localDate = tab.getDateDatePicker().getValue();
        if(localDate != null) {
            date = getStringDate(tab.getDateDatePicker().getValue());
        }
        String time = tab.getTimeTextField().getText();
        String title = tab.getTitleTextField().getText();
        String topic = tab.getTopicTextField().getText();
        String link = tab.getLinkTextField().getText();
        String criteria = tab.getCriteriaTextField().getText();
        
        if(typeText.equals("") || date.equals("") || title.equals("")) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_EMPTY_FIELDS_HEADER), props.getProperty(CSGProp.SCHEDULE_EMPTY_FIELDS_TEXT));
            return;
        }
        
        /*if(!csg.getDataHandler().isScheduleDateValid(localDate)) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_ITEM_INVALID_DATE_HEADER), props.getProperty(CSGProp.SCHEDULE_ITEM_INVALID_DATE_TEXT));
            return;
        }*/
        
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        if(dayOfWeek == DayOfWeek.SUNDAY || dayOfWeek == DayOfWeek.SATURDAY) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_ITEM_INVALID_DATE_HEADER), props.getProperty(CSGProp.SCHEDULE_ITEM_INVALID_DATE_TEXT));
            return;
        }
        
        ScheduleItemType type = ScheduleItemType.valueOf(typeText);
        
        if(tab.getSubmitButton().getText().equals(addText)) {
            AddScheduleItem addScheduleItem = new AddScheduleItem(csg, type, date, title, topic, time, link, criteria);
            csg.getTPS().addTransaction(addScheduleItem);
        } else {
            ScheduleItem scheduleItem = (ScheduleItem) tab.getScheduleTable().getSelectionModel().getSelectedItem();
            if(scheduleItem != null) {
                int id = scheduleItem.getId();
                ScheduleItemType oldType = scheduleItem.getScheduleItemType();
                String oldDate = scheduleItem.dateProperty().getValue();
                String oldTime = scheduleItem.timeProperty().getValue();
                String oldTitle = scheduleItem.titleProperty().getValue();
                String oldTopic = scheduleItem.topicProperty().getValue();
                String oldLink = scheduleItem.linkProperty().getValue();
                String oldCriteria = scheduleItem.criteriaProperty().getValue();
                EditScheduleItem editScheduleItem = new EditScheduleItem(csg, id, type, date, time, title, topic, link, criteria,
                    oldType, oldDate, oldTime, oldTitle, oldTopic, oldLink, oldCriteria);
                csg.getTPS().addTransaction(editScheduleItem);
            }
        }
        
        clear();
    }
    
    public void handleTableClick() {
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ScheduleItem scheduleItem = (ScheduleItem) tab.getScheduleTable().getSelectionModel().getSelectedItem();
        if(scheduleItem != null) {
            String[] date = scheduleItem.dateProperty().getValue().split("/");
            LocalDate localDate = LocalDate.of(Integer.valueOf(date[2]), Integer.valueOf(date[0]), Integer.valueOf(date[1]));
            
            tab.getTypeComboBox().setValue(scheduleItem.typeProperty().getValue());
            tab.getDateDatePicker().setValue(localDate);
            tab.getTimeTextField().setText(scheduleItem.timeProperty().getValue());
            tab.getTitleTextField().setText(scheduleItem.titleProperty().getValue());
            tab.getTopicTextField().setText(scheduleItem.topicProperty().getValue());
            tab.getLinkTextField().setText(scheduleItem.linkProperty().getValue());
            tab.getCriteriaTextField().setText(scheduleItem.criteriaProperty().getValue());
            tab.getSubmitButton().setText(props.getProperty(CSGProp.BUTTON_UPDATE));
        }
    }
    
    public void handleRemoveButton() {
        remove();
    }
    
    public void handleKeyPress(KeyCode code) {
        if(code == KeyCode.DELETE) {
            remove();
        }
    }
    
    public void handleStartingMondayChange() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        LocalDate monday = tab.getStartingMondayDatePicker().getValue();
        LocalDate friday = tab.getEndingFridayDatePicker().getValue();
        LocalDate old = tab.getPreviousStartingMonday();
        if(monday == null) {
            return;
        }
        if(monday.getDayOfWeek() != DayOfWeek.MONDAY) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_INVALID_MONDAY_HEADER), props.getProperty(CSGProp.SCHEDULE_INVALID_MONDAY_TEXT));
            tab.setPropertyMonday(true);
            csg.getDataHandler().startingMondayProperty().setValue(old);
            return;
        }
        if(friday != null && monday.compareTo(friday) > 0) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_INVALID_BOUNDARIES_MONDAY_HEADER), props.getProperty(CSGProp.SCHEDULE_INVALID_BOUNDARIES_MONDAY_TEXT));
            tab.setPropertyMonday(true);
            csg.getDataHandler().startingMondayProperty().setValue(old);
            return;
        }
        /*if(csg.getDataHandler().willScheduleItemsBeRemoved()) {
            Dialogs.showYesNoCancelDialog(props.getProperty(CSGProp.SCHEDULE_ITEMS_WILL_BE_REMOVED_HEADER), props.getProperty(CSGProp.SCHEDULE_ITEMS_WILL_BE_REMOVED_TEXT));
            String response = Dialogs.getResponse();
            if(response.equals(Dialogs.NO) || response.equals(Dialogs.CANCEL)) {
                tab.setPropertyMonday(true);
                csg.getDataHandler().startingMondayProperty().setValue(old);
                return;
            }
        }*/
        if(monday.equals(old)) {
            return;
        }
        csg.getTPS().addTransaction(new ChangeStartingMonday(csg, monday, old));
    }
    
    public void handleEndingFridayChange() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        LocalDate monday = tab.getStartingMondayDatePicker().getValue();
        LocalDate friday = tab.getEndingFridayDatePicker().getValue();
        LocalDate old = tab.getPreviousEndingFriday();
        if(friday == null) {
            return;
        }
        if(friday.getDayOfWeek() != DayOfWeek.FRIDAY) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_INVALID_FRIDAY_HEADER), props.getProperty(CSGProp.SCHEDULE_INVALID_FRIDAY_TEXT));
            tab.setPropertyFriday(true);
            csg.getDataHandler().endingFridayProperty().setValue(old);
            return;
        }
        if(monday != null && monday.compareTo(friday) > 0) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SCHEDULE_INVALID_BOUNDARIES_FRIDAY_HEADER), props.getProperty(CSGProp.SCHEDULE_INVALID_BOUNDARIES_FRIDAY_TEXT));
            tab.setPropertyFriday(true);
            csg.getDataHandler().endingFridayProperty().setValue(old);
            return;
        }
        /*if(csg.getDataHandler().willScheduleItemsBeRemoved()) {
            Dialogs.showYesNoCancelDialog(props.getProperty(CSGProp.SCHEDULE_ITEMS_WILL_BE_REMOVED_HEADER), props.getProperty(CSGProp.SCHEDULE_ITEMS_WILL_BE_REMOVED_TEXT));
            String response = Dialogs.getResponse();
            if(response.equals(Dialogs.NO) || response.equals(Dialogs.CANCEL)) {
                tab.setPropertyFriday(true);
                csg.getDataHandler().endingFridayProperty().setValue(old);
                return;
            }
        }*/
        if(friday.equals(old)) {
            return;
        }
        csg.getTPS().addTransaction(new ChangeEndingFriday(csg, friday, old));
    }
    
    private void remove() {
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        ScheduleItem scheduleItem = (ScheduleItem) tab.getScheduleTable().getSelectionModel().getSelectedItem();
        if(scheduleItem != null) {
            int id = scheduleItem.getId();
            ScheduleItemType type = scheduleItem.getScheduleItemType();
            String date = scheduleItem.dateProperty().getValue();
            String time = scheduleItem.timeProperty().getValue();
            String title = scheduleItem.titleProperty().getValue();
            String topic = scheduleItem.topicProperty().getValue();
            String link = scheduleItem.linkProperty().getValue();
            String criteria = scheduleItem.criteriaProperty().getValue();
            RemoveScheduleItem removeScheduleItem = new RemoveScheduleItem(csg, id, type, date, time, title, topic, link, criteria);
            csg.getTPS().addTransaction(removeScheduleItem);
            clear();
        }
    }
    
    private void clear() {
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        tab.getTypeComboBox().setValue("");
        tab.getDateDatePicker().setValue(null);
        tab.getTimeTextField().setText("");
        tab.getTitleTextField().setText("");
        tab.getTopicTextField().setText("");
        tab.getLinkTextField().setText("");
        tab.getCriteriaTextField().setText("");
        tab.getSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
    }
    
    private String getStringDate(LocalDate localDate) {
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();
        int year = localDate.getYear();
        return month + "/" + day + "/" + year;
    }
    
}