package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.actions.AddRecitation;
import csg.actions.EditRecitation;
import csg.actions.RemoveRecitation;
import csg.data.Recitation;
import csg.data.TeachingAssistant;
import csg.workspace.RecitationTab;
import javafx.scene.input.KeyCode;
import properties_manager.PropertiesManager;

/**
 * Represents all method which are called when controls in the Recitation tab
 * are used.
 * @author Mohsin
 */
public class RecitationController {
    
    private CSG csg;
    
    public RecitationController(CSG csg) {
        this.csg = csg;
    }
    
    public void handleSubmit() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        RecitationTab tab = csg.getWorkspaceHandler().getRecitationTab();
        String addText = props.getProperty(CSGProp.BUTTON_ADD);
        String updateText = props.getProperty(CSGProp.BUTTON_UPDATE);
        String submitBtnText = tab.getAddEditSubmitButton().getText();
        
        String section = tab.getAddEditSectionTextField().getText();
        String inst = tab.getAddEditInstTextField().getText();
        String dayTime = tab.getAddEditDayTimeTextField().getText();
        String location = tab.getAddEditLocationTextField().getText();
        Object obj = tab.getAddEditTAOneComboBox().getValue();
        String ta1 = obj == null ? "" : obj.toString();
        obj = tab.getAddEditTATwoComboBox().getValue();
        String ta2 = obj == null ? "" : obj.toString();
        
        if(section.equals("") || inst.equals("") || dayTime.equals("") || location.equals("") || ta1.equals("") || ta2.equals("")) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.EMPTY_FIELDS_HEADER), props.getProperty(CSGProp.EMPTY_FIELDS_TEXT));
            return;
        }

        if(ta1.equals(ta2)) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SAME_TAS_HEADER), props.getProperty(CSGProp.SAME_TAS_TEXT));
            return;
        }
        
        if(submitBtnText.equals(addText)) {
            
            if(csg.getDataHandler().getRecitation(section) != null) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.RECITATION_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.RECITATION_ALREADY_EXISTS_TEXT));
                return;
            }
            
            csg.getTPS().addTransaction(new AddRecitation(csg, section, inst, dayTime, location, ta1, ta2));
        } else if(submitBtnText.equals(updateText)) {
            
            Recitation recitation = (Recitation) tab.getRecitationsTable().getSelectionModel().getSelectedItem();
            Recitation recitation2 = csg.getDataHandler().getRecitation(section);
            
            if(recitation2 != null && recitation2 != recitation) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.RECITATION_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.RECITATION_ALREADY_EXISTS_TEXT));
                return;
            }
            
            String oldSection = recitation.sectionProperty().getValue();
            String oldInst = recitation.instProperty().getValue();
            String oldDayTime = recitation.dayTimeProperty().getValue();
            String oldLocation = recitation.locationProperty().getValue();
            String oldTA1 = recitation.ta1Property().getValue();
            String oldTA2 = recitation.ta2Property().getValue();
            
            if(!oldSection.equals(section) || !oldInst.equals(inst) || !oldDayTime.equals(dayTime) || !oldLocation.equals(location) || !oldTA1.equals(ta1) || !oldTA2.equals(ta2)) {
                EditRecitation editRecitation = new EditRecitation(csg, section, inst, dayTime, location, ta1, ta2,
                    oldSection, oldInst, oldDayTime, oldLocation, oldTA1, oldTA2);
                csg.getTPS().addTransaction(editRecitation);
            }
            
            tab.getAddEditSubmitButton().setText(addText);
        }
        
        tab.getAddEditSectionTextField().setText("");
        tab.getAddEditInstTextField().setText("");
        tab.getAddEditDayTimeTextField().setText("");
        tab.getAddEditLocationTextField().setText("");
        tab.getAddEditTAOneComboBox().setValue(null);
        tab.getAddEditTATwoComboBox().setValue(null);
    }
    
    public void handleClear() {
        RecitationTab tab = csg.getWorkspaceHandler().getRecitationTab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String addText = props.getProperty(CSGProp.BUTTON_ADD);
        tab.getAddEditSubmitButton().setText(addText);
        tab.getAddEditSectionTextField().setText("");
        tab.getAddEditInstTextField().setText("");
        tab.getAddEditDayTimeTextField().setText("");
        tab.getAddEditLocationTextField().setText("");
        tab.getAddEditTAOneComboBox().setValue(null);
        tab.getAddEditTATwoComboBox().setValue(null);
    }
    
    public void handleTableClick() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        RecitationTab tab = csg.getWorkspaceHandler().getRecitationTab();
        Recitation recitation = (Recitation) tab.getRecitationsTable().getSelectionModel().getSelectedItem();
        if(recitation != null) {
            String section = recitation.sectionProperty().getValue();
            String inst = recitation.instProperty().getValue();
            String dayTime = recitation.dayTimeProperty().getValue();
            String location = recitation.locationProperty().getValue();
            String ta1 = recitation.ta1Property().getValue();
            String ta2 = recitation.ta2Property().getValue();
            TeachingAssistant taOne = csg.getDataHandler().getTA(ta1);
            TeachingAssistant taTwo = csg.getDataHandler().getTA(ta2);
            tab.getAddEditSectionTextField().setText(section);
            tab.getAddEditInstTextField().setText(inst);
            tab.getAddEditDayTimeTextField().setText(dayTime);
            tab.getAddEditLocationTextField().setText(location);
            tab.getAddEditTAOneComboBox().setValue(taOne);
            tab.getAddEditTATwoComboBox().setValue(taTwo);
            tab.getAddEditSubmitButton().setText(props.getProperty(CSGProp.BUTTON_UPDATE));
        }
    }
    
    public void handleKeyPress(KeyCode code) {
        if(code == KeyCode.DELETE) {
            removeRecitation();
        }
    }
    
    public void handleRemoveRecitation() {
        removeRecitation();
    }
    
    private void removeRecitation() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        RecitationTab tab = csg.getWorkspaceHandler().getRecitationTab();
        Recitation recitation = (Recitation) tab.getRecitationsTable().getSelectionModel().getSelectedItem();
        if(recitation != null) {
            String section = recitation.sectionProperty().getValue();
            String inst = recitation.instProperty().getValue();
            String dayTime = recitation.dayTimeProperty().getValue();
            String location = recitation.locationProperty().getValue();
            String ta1 = recitation.ta1Property().getValue();
            String ta2 = recitation.ta2Property().getValue();
            RemoveRecitation removeRecitation = new RemoveRecitation(csg, section, inst, dayTime, location, ta1, ta2);
            csg.getTPS().addTransaction(removeRecitation);
            if(tab.getAddEditSubmitButton().getText().equals(props.getProperty(CSGProp.BUTTON_UPDATE))) {
                tab.getAddEditSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
                tab.getAddEditSectionTextField().setText("");
                tab.getAddEditInstTextField().setText("");
                tab.getAddEditDayTimeTextField().setText("");
                tab.getAddEditLocationTextField().setText("");
                tab.getAddEditTAOneComboBox().setValue(null);
                tab.getAddEditTATwoComboBox().setValue(null);
            }
        }
    }
    
}