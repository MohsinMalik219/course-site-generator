package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.actions.AddOfficeHours;
import csg.actions.AddTA;
import csg.actions.EditTA;
import csg.actions.OfficeHoursTimeChange;
import csg.actions.RemoveOfficeHours;
import csg.actions.RemoveTA;
import csg.data.DataHandler;
import csg.data.TeachingAssistant;
import csg.workspace.TATab;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import properties_manager.PropertiesManager;

/**
 * Represents all method which are called when controls in the TA tab are
 * used.
 * @author Mohsin
 */
public class TAController {
    
    private CSG csg;
    private static final String EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    
    public TAController(CSG csg) {
        this.csg = csg;
    }
    
    public void handleTASubmit() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        
        String btnText = tab.getTaSubmitButton().getText();
        TextField nameTextField = tab.getTaNameTextField();
        TextField emailTextField = tab.getTaEmailTextField();
        String nameText = nameTextField.getText();
        String emailText = emailTextField.getText();
        
        if(btnText.equals(props.getProperty(CSGProp.BUTTON_ADD))) {
            if(nameText.equals("") || emailText.equals("")) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_TEXT_FIELD_EMPTY_HEADER), props.getProperty(CSGProp.TA_TEXT_FIELD_EMPTY_TEXT));
            } else if(csg.getDataHandler().getTA(nameText) != null) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.TA_ALREADY_EXISTS_TEXT));
            } else {
                Matcher matcher = pattern.matcher(emailText);
                if(!matcher.matches()) {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_INVALID_EMAIL_HEADER), props.getProperty(CSGProp.TA_INVALID_EMAIL_TEXT));
                } else {
                    csg.getTPS().addTransaction(new AddTA(csg, nameText, emailText));
                    nameTextField.setText("");
                    emailTextField.setText("");
                }
            }
        } else {
            Object obj = tab.getTaTable().getSelectionModel().getSelectedItem();
            TeachingAssistant ta = null;
            if(obj != null && obj instanceof TeachingAssistant) {
                ta = (TeachingAssistant) obj;
            }
            if(ta == null) {
                tab.getTaSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
                nameTextField.setText("");
                emailTextField.setText("");
            } else if(nameText.equals("") || emailText.equals("")) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_TEXT_FIELD_EMPTY_HEADER), props.getProperty(CSGProp.TA_TEXT_FIELD_EMPTY_TEXT));
            } else if(csg.getDataHandler().getTA(nameText) != null) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.TA_ALREADY_EXISTS_TEXT));
            } else {
                Matcher matcher = pattern.matcher(emailText);
                if(!matcher.matches()) {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.TA_INVALID_EMAIL_HEADER), props.getProperty(CSGProp.TA_INVALID_EMAIL_TEXT));
                } else {
                    csg.getTPS().addTransaction(new EditTA(csg, nameText, emailText, ta.nameProperty().getValue(), ta.emailProperty().getValue()));
                    nameTextField.setText("");
                    emailTextField.setText("");
                    tab.getTaSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
                }
            }
        }
    }
    
    public void handleTableClick() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        Object obj = tab.getTaTable().getSelectionModel().getSelectedItem();
        if(obj != null && obj instanceof TeachingAssistant) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            TeachingAssistant ta = (TeachingAssistant) obj;
            tab.getTaNameTextField().setText(ta.nameProperty().getValue());
            tab.getTaEmailTextField().setText(ta.emailProperty().getValue());
            tab.getTaSubmitButton().setText(props.getProperty(CSGProp.BUTTON_UPDATE));
        }
    }
    
    public void handleClearButton() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        tab.getTaNameTextField().setText("");
        tab.getTaEmailTextField().setText("");
        tab.getTaSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
    }
    
    public void handleTARemove() {
        removeTA();
    }
    
    public void handleKeyPress(KeyCode code) {
        if(code == KeyCode.DELETE) {
            removeTA();
        }
    }
    
    public void handleToggleOfficeHours(String key) {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        Object obj = tab.getTaTable().getSelectionModel().getSelectedItem();
        if(obj != null && obj instanceof TeachingAssistant) {
            TeachingAssistant ta = (TeachingAssistant) obj;
            StringProperty textProp = csg.getDataHandler().getOfficeHourStringProps().get(key);
            String text = textProp.getValue();
            String name = ta.nameProperty().getValue();
            if(text != null && text.contains(name)) {
                csg.getTPS().addTransaction(new RemoveOfficeHours(csg, name, key));
            } else {
                csg.getTPS().addTransaction(new AddOfficeHours(csg, name, key));
            }
        }
    }
    
    public void handleGridTimeChange() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        ComboBox startBox = tab.getStartTimeComboBox();
        ComboBox endBox = tab.getEndTimeComboBox();
        String startTime = (String) startBox.getSelectionModel().getSelectedItem();
        String endTime = (String) endBox.getSelectionModel().getSelectedItem();
        int startHour = standardStringToMilitary(startTime);
        int endHour = standardStringToMilitary(endTime);
        if(startHour == csg.getDataHandler().getStartHour() && endHour == csg.getDataHandler().getEndHour()) {
            return;
        }
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(startHour >= endHour) {
            tab.updateComboBoxes();
            Dialogs.showMessageDialog(props.getProperty(CSGProp.OFFICE_HOURS_GRID_INVALID_TIME_HEADER), props.getProperty(CSGProp.OFFICE_HOURS_GRID_INVALID_TIME_TEXT));
        } else {
            if((startHour != csg.getDataHandler().getStartHour() && newStartTimeReplace(startHour)) || 
                    (endHour != csg.getDataHandler().getEndHour() && newEndTimeReplace(endHour))) {
                Dialogs.showYesNoCancelDialog(props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_CHANGE_REPLACE_HEADER), props.getProperty(CSGProp.OFFICE_HOURS_GRID_TIME_CHANGE_REPLACE_TEXT));
                if(Dialogs.getResponse().equals(Dialogs.NO) || Dialogs.getResponse().equals(Dialogs.CANCEL)) {
                    csg.getWorkspaceHandler().getTaTab().updateComboBoxes();
                    return;
                }
            }
            csg.getTPS().addTransaction(new OfficeHoursTimeChange(csg, startHour, endHour, csg.getDataHandler().getStartHour(), csg.getDataHandler().getEndHour()));
        }
    }
    
    private boolean newStartTimeReplace(int newStartTime) {
        DataHandler data = csg.getDataHandler();
        int currentStartTime = data.getStartHour();
        if(newStartTime > currentStartTime) {
            for(int i = 1; i < getTimeRow(newStartTime); i++) {
                for(int col = 2; col < 7; col++) {
                    String key = buildKey(col, i);
                    String labelText = csg.getDataHandler().getOfficeHourStringProps().get(key).getValue();
                    if(labelText != null && !labelText.isEmpty()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean newEndTimeReplace(int newEndTime) {
        DataHandler data = csg.getDataHandler();
        int currentEndTime = data.getEndHour();
        if(newEndTime < currentEndTime) {
            for(int i = getTimeRow(newEndTime); i < data.getNumRows(); i++) {
                for(int col = 2; col < 7; col++) {
                    String key = buildKey(col, i);
                    String labelText = data.getOfficeHourStringProps().get(key).getValue();
                    if(labelText != null && !labelText.isEmpty()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    private int getTimeRow(int hour) {
        return ((hour - csg.getDataHandler().getStartHour()) * 2) + 1;
    }
    
    private void removeTA() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        Object obj = tab.getTaTable().getSelectionModel().getSelectedItem();
        if(obj != null && obj instanceof TeachingAssistant) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            TeachingAssistant ta = (TeachingAssistant) obj;
            csg.getTPS().addTransaction(new RemoveTA(csg, ta.nameProperty().getValue(), ta.emailProperty().getValue(), ta.undergradProperty().getValue()));
            tab.getTaNameTextField().setText("");
            tab.getTaEmailTextField().setText("");
            tab.getTaSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
        }
    }
    
    /**
     * Converts a standard time string into military time.
     * @param time The standard time.
     * @return The military time hour.
     */
    private int standardStringToMilitary(String time) {
        int hour = Integer.valueOf(time.split(":")[0]);
        if(time.contains("am") && hour == 12) {
            hour -= 12;
        } else if(time.contains("pm") && hour != 12) {
            hour += 12;
        }
        return hour;
    }
    
}