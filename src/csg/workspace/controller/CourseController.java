package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.actions.ChangeExportDir;
import csg.actions.ChangeTemplateDir;
import java.io.File;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

/**
 * Represents all method which are called when controls in the Course tab are
 * used.
 * @author Mohsin
 */
public class CourseController {
    
    private CSG csg;
    
    public CourseController(CSG csg) {
        this.csg = csg;
    }
    
    public void handleBannerChange() {
        File file = selectFile();
        if(file != null) {
            csg.getDataHandler().setBannerImagePath(file.getAbsolutePath());
            csg.getWorkspaceHandler().getCourseTab().setBanner();
            csg.getDataHandler().setAsChanged();
        }
    }
    
    public void handleLeftImageChange() {
        File file = selectFile();
        if(file != null) {
            csg.getDataHandler().setLeftImagePath(file.getAbsolutePath());
            csg.getWorkspaceHandler().getCourseTab().setLeftImage();
            csg.getDataHandler().setAsChanged();
        }
    }
    
    public void handleRightImageChange() {
        File file = selectFile();
        if(file != null) {
            csg.getDataHandler().setRightImagePath(file.getAbsolutePath());
            csg.getWorkspaceHandler().getCourseTab().setRightImage();
            csg.getDataHandler().setAsChanged();
        }
    }
    
    private File selectFile() {
        FileChooser fc = new FileChooser();
        fc.setTitle(PropertiesManager.getPropertiesManager().getProperty(CSGProp.SELECT_IMAGE_HEADER));
        fc.setInitialDirectory(new File("work"));
        return fc.showOpenDialog(csg.getWorkspaceHandler().getPrimaryStage());
    }
    
    public void handleExportDirChange() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File("work"));
        dc.setTitle(props.getProperty(CSGProp.EXPORT_DIR_CHANGE_HEADER));
        File newExportDir = dc.showDialog(csg.getWorkspaceHandler().getPrimaryStage());
        if(newExportDir != null) {
            ChangeExportDir ced = new ChangeExportDir(csg, newExportDir.getAbsolutePath(), csg.getDataHandler().exportDirProperty().getValue());
            ced.doTransaction();
        }
    }
    
    public void handleTemplateDirChange() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File("work"));
        dc.setTitle(props.getProperty(CSGProp.TEMPLATE_SELECT_HEADER));
        File newTemplateDir = dc.showDialog(csg.getWorkspaceHandler().getPrimaryStage());
        if(newTemplateDir != null) {
            if(csg.getDataHandler().isTemplateValid(newTemplateDir.getAbsolutePath())) {
                ChangeTemplateDir ctd = new ChangeTemplateDir(csg, newTemplateDir.getAbsolutePath(), csg.getDataHandler().templateDirProperty().getValue());
                ctd.doTransaction();
            } else {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TEMPLATE_DIR_EMPTY_HEADER), props.getProperty(CSGProp.TEMPLATE_DIR_EMPTY_TEXT));
            }
        }
    }
    
}