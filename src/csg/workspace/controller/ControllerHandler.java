package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.workspace.WorkspaceHandler;
import java.io.File;
import javafx.scene.input.KeyCode;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import properties_manager.PropertiesManager;

/**
 * This class will handle all methods which are ran when controls are used.
 * @author Mohsin
 */
public class ControllerHandler {
    
    private CSG csg;
    private CourseController courseController;
    private ProjectController projectController;
    private RecitationController recitationController;
    private ScheduleController scheduleController;
    private TAController taController;
    private boolean ctrl = false;
    
    public ControllerHandler(CSG csg, WorkspaceHandler workspace) {
        this.csg = csg;
        courseController = new CourseController(csg);
        projectController = new ProjectController(csg);
        recitationController = new RecitationController(csg);
        scheduleController = new ScheduleController(csg);
        taController = new TAController(csg);
    }
    
    public void handleKeyPressed(KeyCode code) {
        if(code == KeyCode.CONTROL) {
            ctrl = true;
        } else if(ctrl) {
            if(code == KeyCode.Z) {
                csg.getTPS().undoTransaction();
            } else if(code == KeyCode.Y) {
                csg.getTPS().doTransaction();
            }
        }
    }
    
    public void handleKeyReleased(KeyCode code) {
        if(code == KeyCode.CONTROL) {
            ctrl = false;
        }
    }
    
    // Toolbar button handle methods.
    
    public void handleNew() {
        if(!checkForChanges()) {
            return;
        }
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        csg.getDataHandler().resetData();
        csg.getWorkspaceHandler().getTaTab().updateComboBoxes();
        csg.getWorkspaceHandler().getCourseTab().resetImages();
        Dialogs.showMessageDialog(props.getProperty(CSGProp.NEW_WORKSPACE_HEADER), props.getProperty(CSGProp.NEW_WORKSPACE_TEXT));
        csg.getDataHandler().setChangeMade(false);
        csg.getTPS().reset();
    }
    
    public void handleLoad() {
        if(!checkForChanges()) {
            return;
        }
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // TODO: See if there are any changes, ask if they want to save first.
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("work"));
        ExtensionFilter ef = new ExtensionFilter(props.getProperty(CSGProp.FILE_SELECT_FILTER), "*.json");
        fileChooser.getExtensionFilters().add(ef);
        fileChooser.setTitle(props.getProperty(CSGProp.LOAD_TITLE));
        File jsonFile = fileChooser.showOpenDialog(csg.getWorkspaceHandler().getPrimaryStage());
        if(jsonFile != null) {
            if(!csg.getFileHandler().load(jsonFile.getAbsolutePath())) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.LOAD_FAILED_HEADER), props.getProperty(CSGProp.LOAD_FAILED_TEXT));
            } else {
                csg.getWorkspaceHandler().getCourseTab().setBanner();
                csg.getWorkspaceHandler().getCourseTab().setLeftImage();
                csg.getWorkspaceHandler().getCourseTab().setRightImage();
                csg.getDataHandler().setChangeMade(false);
                csg.getTPS().reset();
            }
        }
    }
    
    private boolean checkForChanges() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(csg.getDataHandler().isChangeMade()) {
            Dialogs.showYesNoCancelDialog(props.getProperty(CSGProp.CHANGES_MADE_HEADER), props.getProperty(CSGProp.CHANGES_MADE_TEXT));
            String resp = Dialogs.getResponse();
            if(resp.equals(Dialogs.CANCEL)) {
                return false;
            }
            if(resp.equals(Dialogs.YES)) {
                handleSaveAs();
            }
        }
        return true;
    }
    
    public void handleSave() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String path = csg.getDataHandler().getSavePath();
        if(path == null || path.equals("")) {
            csg.getWorkspaceHandler().getSaveButton().setDisable(true);
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SAVE_FAILED_HEADER), props.getProperty(CSGProp.SAVE_FAILED_TEXT));
            return;
        }
        if(csg.getFileHandler().save(path)) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SAVE_SUCCESS_HEADER), props.getProperty(CSGProp.SAVE_SUCCESS_TEXT));
            csg.getWorkspaceHandler().getSaveButton().setDisable(true);
            csg.getDataHandler().setChangeMade(false);
        } else {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.SAVE_FAILED_HEADER), props.getProperty(CSGProp.SAVE_FAILED_TEXT));
        }
    }
    
    public void handleSaveAs() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("work"));
        ExtensionFilter ef = new ExtensionFilter(props.getProperty(CSGProp.FILE_SELECT_FILTER), "*.json");
        fileChooser.getExtensionFilters().add(ef);
        fileChooser.setTitle(props.getProperty(CSGProp.SAVE_AS_TITLE));
        File jsonFile = fileChooser.showSaveDialog(csg.getWorkspaceHandler().getPrimaryStage());
        if(jsonFile != null) {
            if(csg.getFileHandler().save(jsonFile.getAbsolutePath())) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.SAVE_SUCCESS_HEADER), props.getProperty(CSGProp.SAVE_SUCCESS_TEXT));
                csg.getDataHandler().setSavePath(jsonFile.getAbsolutePath());
                csg.getWorkspaceHandler().getSaveButton().setDisable(true);
                csg.getDataHandler().setChangeMade(false);
            } else {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.SAVE_FAILED_HEADER), props.getProperty(CSGProp.SAVE_FAILED_TEXT));
            }
        }
    }
    
    public void handleExport() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        String exportDir = csg.getDataHandler().exportDirProperty().getValue();
        String templateDir = csg.getDataHandler().templateDirProperty().getValue();
        if(exportDir.equals(props.getProperty(CSGProp.NONE_SELECTED))) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.EXPORT_NONE_SELECTED_HEADER), props.getProperty(CSGProp.EXPORT_NONE_SELECTED_TEXT));
        } else if(templateDir.equals(props.getProperty(CSGProp.NONE_SELECTED))) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.TEMPLATE_NOT_SELECTED_HEADER), props.getProperty(CSGProp.TEMPLATE_NOT_SELECTED_TEXT));
        } else if(csg.getFileHandler().export(exportDir)) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.EXPORT_SUCCESS_HEADER), props.getProperty(CSGProp.EXPORT_SUCCESS_TEXT));
        }
    }
    
    public void handleUndo() {
        csg.getTPS().undoTransaction();
    }
    
    public void handleRedo() {
        csg.getTPS().doTransaction();
    }
    
    public void handleAbout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Dialogs.showMessageDialog(props.getProperty(CSGProp.ABOUT_HEADER), props.getProperty(CSGProp.ABOUT_TEXT));
    }
    
    public void handleExit() {
        if(!checkForChanges()) {
            return;
        }
        System.exit(0);
    }
    
    // GETTERS FOR ALL CONTROLLERS
    
    public CourseController getCourseController() {
        return courseController;
    }

    public ProjectController getProjectController() {
        return projectController;
    }

    public RecitationController getRecitationController() {
        return recitationController;
    }

    public ScheduleController getScheduleController() {
        return scheduleController;
    }

    public TAController getTaController() {
        return taController;
    }
    
}