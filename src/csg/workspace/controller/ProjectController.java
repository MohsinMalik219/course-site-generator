package csg.workspace.controller;

import csg.CSG;
import csg.CSGProp;
import csg.Dialogs;
import csg.actions.AddStudent;
import csg.actions.AddTeam;
import csg.actions.EditStudent;
import csg.actions.EditTeam;
import csg.actions.RemoveStudent;
import csg.actions.RemoveTeam;
import csg.data.Student;
import csg.data.Team;
import csg.workspace.ProjectTab;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 * Represents all method which are called when controls in the Project tab are
 * used.
 * @author Mohsin
 */
public class ProjectController {
    
    private CSG csg;
    
    public ProjectController(CSG csg) {
        this.csg = csg;
    }
    
    public void handleTeamTableClick() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        Team team = (Team) tab.getTeamsTable().getSelectionModel().getSelectedItem();
        if(team != null) {
            String name = team.nameProperty().getValue();
            String link = team.linkProperty().getValue();
            Color color = Color.web(team.colorProperty().getValue());
            Color textColor = Color.web(team.textColorProperty().getValue());
            tab.getNameTextField().setText(name);
            tab.getLinkTextField().setText(link);
            tab.getColorPicker().setValue(color);
            tab.getTextColorPicker().setValue(textColor);
            tab.getTeamsSubmitButton().setText(props.getProperty(CSGProp.BUTTON_UPDATE));
        }
    }
    
    public void handleSubmitTeam() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String addText = props.getProperty(CSGProp.BUTTON_ADD);
        String updateText = props.getProperty(CSGProp.BUTTON_UPDATE);
        
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        String name = tab.getNameTextField().getText();
        String link = tab.getLinkTextField().getText();
        String color = colorToString(tab.getColorPicker().getValue()).replace("#", "").toLowerCase();
        String textColor = colorToString(tab.getTextColorPicker().getValue()).replace("#", "").toLowerCase();
        if(name.equals("") || link.equals("") || color.equals("") || textColor.equals("")) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.EMPTY_FIELDS_HEADER), props.getProperty(CSGProp.EMPTY_FIELDS_TEXT));
            return;
        }
        if(tab.getTeamsSubmitButton().getText().equals(addText)) {
            Team team = csg.getDataHandler().getTeam(name);
            if(team != null) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.TEAM_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.TEAM_ALREADY_EXISTS_TEXT));
                return;
            }
            csg.getTPS().addTransaction(new AddTeam(csg, name, color, textColor, link));
        } else {
            Team team = (Team) tab.getTeamsTable().getSelectionModel().getSelectedItem();
            if(team != null) {
                Team team2 = csg.getDataHandler().getTeam(name);
                if(team2 != null && team2 != team) {
                    Dialogs.showMessageDialog(props.getProperty(CSGProp.TEAM_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.TEAM_ALREADY_EXISTS_TEXT));
                    return;
                }
                String oldName = team.nameProperty().getValue();
                String oldColor = team.colorProperty().getValue();
                String oldTextColor = team.textColorProperty().getValue();
                String oldLink = team.linkProperty().getValue();
                csg.getTPS().addTransaction(new EditTeam(csg, name, color, textColor, link, oldName, oldColor, oldTextColor, oldLink));
            }
        }
        clearTeam();
    }
    
    public void handleKeyPressTeam(KeyCode code) {
        if(code == KeyCode.DELETE) {
            removeTeam();
        }
    }
    
    public void handleDeleteTeam() {
        removeTeam();
    }
    
    public void handleTeamClear() {
        clearTeam();
    }
    
    public void handleDeleteStudent() {
        removeStudent();
    }
    
    public void handleKeyPressStudent(KeyCode code) {
        if(code == KeyCode.DELETE) {
            removeStudent();
        }
    }
    
    public void handleStudentTableClick() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        Student student = (Student) tab.getStudentsTable().getSelectionModel().getSelectedItem();
        if(student != null) {
            String firstName = student.firstNameProperty().getValue();
            String lastName = student.lastNameProperty().getValue();
            String teamName = student.teamProperty().getValue();
            Team team = csg.getDataHandler().getTeam(teamName);
            String role = student.roleProperty().getValue();
            tab.getFirstNameTextField().setText(firstName);
            tab.getLastNameTextField().setText(lastName);
            tab.getTeamsComboBox().setValue(team);
            tab.getRoleTextField().setText(role);
            tab.getStudentsSubmitButton().setText(props.getProperty(CSGProp.BUTTON_UPDATE));
        }
    }
    
    public void handleStudentClear() {
        clearStudent();
    }
    
    public void handleStudentSubmit() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String addText = props.getProperty(CSGProp.BUTTON_ADD);
        String updateText = props.getProperty(CSGProp.BUTTON_UPDATE);
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        String firstName = tab.getFirstNameTextField().getText();
        String lastName = tab.getLastNameTextField().getText();
        String team = tab.getTeamsComboBox().getValue().toString();
        String role = tab.getRoleTextField().getText();
        if(firstName.equals("") || lastName.equals("") || team.equals("") || role.equals("")) {
            Dialogs.showMessageDialog(props.getProperty(CSGProp.EMPTY_FIELDS_HEADER), props.getProperty(CSGProp.EMPTY_FIELDS_TEXT));
            return;
        }
        if(tab.getStudentsSubmitButton().getText().equals(addText)) {
            Student student = csg.getDataHandler().getStudent(firstName, lastName);
            if(student != null) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.STUDENT_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.STUDENT_ALREADY_EXISTS_TEXT));
                return;
            }
            csg.getTPS().addTransaction(new AddStudent(csg, firstName, lastName, team, role));
        } else {
            Student student = (Student) tab.getStudentsTable().getSelectionModel().getSelectedItem();
            Student student2 = csg.getDataHandler().getStudent(firstName, lastName);
            if(student2 != null && student != student2) {
                Dialogs.showMessageDialog(props.getProperty(CSGProp.STUDENT_ALREADY_EXISTS_HEADER), props.getProperty(CSGProp.STUDENT_ALREADY_EXISTS_TEXT));
                return;
            }
            String oldFirstName = student.firstNameProperty().getValue();
            String oldLastName = student.lastNameProperty().getValue();
            String oldTeam = student.teamProperty().getValue();
            String oldRole = student.roleProperty().getValue();
            EditStudent editStudent = new EditStudent(csg, firstName, lastName, team, role, oldFirstName, oldLastName, oldTeam, oldRole);
            csg.getTPS().addTransaction(editStudent);
        }
        clearStudent();
    }
    
    private void removeStudent() {
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        Student student = (Student) tab.getStudentsTable().getSelectionModel().getSelectedItem();
        if(student != null) {
            String firstName = student.firstNameProperty().getValue();
            String lastName = student.lastNameProperty().getValue();
            String team = student.teamProperty().getValue();
            String role = student.roleProperty().getValue();
            RemoveStudent removeStudent = new RemoveStudent(csg, firstName, lastName, team, role);
            csg.getTPS().addTransaction(removeStudent);
        }
    }
    
    private void removeTeam() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        Team team = (Team) tab.getTeamsTable().getSelectionModel().getSelectedItem();
        if(team != null) {
            if(csg.getDataHandler().willStudentsGetRemoved(team)) {
                Dialogs.showYesNoCancelDialog(props.getProperty(CSGProp.STUDENTS_WILL_BE_REMOVED_HEADER), props.getProperty(CSGProp.STUDENTS_WILL_BE_REMOVED_TEXT));
                String resp = Dialogs.getResponse();
                if(resp.equals(Dialogs.NO) || resp.equals(Dialogs.CANCEL)) {
                    return;
                }
            }
            String name = team.nameProperty().getValue();
            String color = team.colorProperty().getValue();
            String textColor = team.textColorProperty().getValue();
            String link = team.linkProperty().getValue();
            csg.getTPS().addTransaction(new RemoveTeam(csg, name, color, textColor, link));
            clearTeam();
        }
    }
    
    public void clearTeam() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        tab.getNameTextField().setText("");
        tab.getColorPicker().setValue(Color.WHITE);
        tab.getTextColorPicker().setValue(Color.WHITE);
        tab.getLinkTextField().setText("");
        tab.getTeamsSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
    }
    
    public void clearStudent() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        tab.getFirstNameTextField().setText("");
        tab.getLastNameTextField().setText("");
        tab.getTeamsComboBox().setValue(null);
        tab.getRoleTextField().setText("");
        tab.getStudentsSubmitButton().setText(props.getProperty(CSGProp.BUTTON_ADD));
    }
    
    private String colorToString(Color color) {
        return String.format("#%02X%02X%02X",
            (int) (color.getRed() * 255),
            (int) (color.getGreen() * 255),
            (int) (color.getBlue() * 255));
    }
    
}