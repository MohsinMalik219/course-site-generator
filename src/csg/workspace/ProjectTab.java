package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * Represents the Projects tab.
 * @author Mohsin
 */
public class ProjectTab extends Tab {
    
    private CSG csg;
    
    private Pane projectPane;
    private Label projectHeader;
    
    // Teams section
    private Pane teamsPane;
    private Label teamsHeader;
    private Label teamsAddEdit;
    private Label nameLabel;
    private Label colorLabel;
    private Label textColorLabel;
    private Label linkLabel;
    private Button teamsDeleteButton;
    private Button teamsSubmitButton;
    private Button teamsClearButton;
    private TableView teamsTable;
    private TextField nameTextField;
    private TextField linkTextField;
    private ColorPicker colorPicker;
    private ColorPicker textColorPicker;
    
    // Students section
    private Pane studentsPane;
    private Label studentsHeader;
    private Label studentsAddEdit;
    private Label firstNameLabel;
    private Label lastNameLabel;
    private Label teamLabel;
    private Label roleLabel;
    private TableView studentsTable;
    private Button studentsDeleteButton;
    private Button studentsSubmitButton;
    private Button studentsClearButton;
    private TextField firstNameTextField;
    private TextField lastNameTextField;
    private TextField roleTextField;
    private ComboBox teamsComboBox;
    
    public ProjectTab(CSG csg, String title) {
        super(title);
        this.csg = csg;
        setClosable(false);
        
        projectPane = buildProjectPane();
        teamsPane = buildTeamsPane();
        studentsPane = buildStudentsPane();
        
        VBox vbox = new VBox(projectPane, teamsPane, studentsPane);
        vbox.setSpacing(20);
        ScrollPane scrollPane = new ScrollPane(vbox);
        scrollPane.setPadding(new Insets(10, 10, 10, 10));
        
        setContent(scrollPane);
    }
    
    private Pane buildProjectPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        projectHeader = new Label(props.getProperty(CSGProp.PROJECTS_HEADER));
        
        return new Pane(projectHeader);
    }
    
    private Pane buildTeamsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        List<String> tableHeaders = props.getPropertyOptionsList(CSGProp.TEAMS_TABLE_HEADERS);
        
        teamsHeader = new Label(props.getProperty(CSGProp.TEAMS_HEADER));
        teamsAddEdit = new Label(props.getProperty(CSGProp.ADD_EDIT));
        nameLabel = new Label(tableHeaders.get(0) + ":");
        colorLabel = new Label(tableHeaders.get(1) + ":");
        textColorLabel = new Label(tableHeaders.get(2) + ":");
        linkLabel = new Label(tableHeaders.get(3) + ":");
        
        teamsDeleteButton = new Button(props.getProperty(CSGProp.BUTTON_DELETE));
        teamsSubmitButton = new Button(props.getProperty(CSGProp.BUTTON_ADD));
        teamsClearButton = new Button(props.getProperty(CSGProp.BUTTON_CLEAR));
        
        nameTextField = new TextField();
        linkTextField = new TextField();
        
        colorPicker = new ColorPicker();
        textColorPicker = new ColorPicker();
        
        teamsTable = new TableView();
        int i = 0;
        TableColumn col1 = new TableColumn(tableHeaders.get(i++));
        TableColumn col2 = new TableColumn(tableHeaders.get(i++));
        TableColumn col3 = new TableColumn(tableHeaders.get(i++));
        TableColumn col4 = new TableColumn(tableHeaders.get(i++));
        teamsTable.getColumns().addAll(col1, col2, col3, col4);
        
        col1.setCellValueFactory(new PropertyValueFactory("name"));
        col2.setCellValueFactory(new PropertyValueFactory("color"));
        col3.setCellValueFactory(new PropertyValueFactory("textColor"));
        col4.setCellValueFactory(new PropertyValueFactory("link"));
        
        teamsTable.setItems(csg.getDataHandler().getTeams());
        
        textColorLabel.setPadding(new Insets(0, 60, 0, 0));
        
        HBox hbox = new HBox(teamsHeader, teamsDeleteButton);
        hbox.setSpacing(5);
        
        GridPane dataInput = new GridPane();
        HBox buttons = new HBox(teamsSubmitButton, teamsClearButton);
        buttons.setSpacing(5);
        
        dataInput.add(teamsAddEdit, 0, 0);
        
        dataInput.add(nameLabel, 0, 1);
        dataInput.add(colorLabel, 0, 2);
        dataInput.add(textColorLabel, 0, 3);
        dataInput.add(linkLabel, 0, 4);
        dataInput.add(buttons, 0, 5);
        
        dataInput.add(nameTextField, 1, 1);
        dataInput.add(colorPicker, 1, 2);
        dataInput.add(textColorPicker, 1, 3);
        dataInput.add(linkTextField, 1, 4);
        
        teamsTable.setMinWidth(1000);
        
        col1.setPrefWidth(250);
        col2.setPrefWidth(250);
        col3.setPrefWidth(250);
        col4.setPrefWidth(250);
        
        VBox vbox = new VBox(hbox, teamsTable, dataInput);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        teamsDeleteButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleDeleteTeam();
        });
        
        teamsSubmitButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleSubmitTeam();
        });
        
        teamsTable.setOnMouseClicked(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleTeamTableClick();
        });
        
        teamsClearButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleTeamClear();
        });
        
        teamsTable.setOnKeyPressed(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleKeyPressTeam(e.getCode());
        });
        
        return new Pane(vbox);
    }
    
    private Pane buildStudentsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        List<String> tableHeaders = props.getPropertyOptionsList(CSGProp.STUDENTS_TABLE_HEADERS);
        
        studentsHeader = new Label(props.getProperty(CSGProp.STUDENTS_HEADER));
        studentsAddEdit = new Label(props.getProperty(CSGProp.ADD_EDIT));
        firstNameLabel = new Label(tableHeaders.get(0) + ":");
        lastNameLabel = new Label(tableHeaders.get(1) + ":");
        teamLabel = new Label(tableHeaders.get(2) + ":");
        roleLabel = new Label(tableHeaders.get(3) + ":");
        
        studentsDeleteButton = new Button(props.getProperty(CSGProp.BUTTON_DELETE));
        studentsSubmitButton = new Button(props.getProperty(CSGProp.BUTTON_ADD));
        studentsClearButton = new Button(props.getProperty(CSGProp.BUTTON_CLEAR));
        
        firstNameTextField = new TextField();
        lastNameTextField = new TextField();
        roleTextField = new TextField();
        
        teamsComboBox = new ComboBox();
        
        studentsTable = new TableView();
        int i = 0;
        TableColumn col1 = new TableColumn(tableHeaders.get(i++));
        TableColumn col2 = new TableColumn(tableHeaders.get(i++));
        TableColumn col3 = new TableColumn(tableHeaders.get(i++));
        TableColumn col4 = new TableColumn(tableHeaders.get(i++));
        studentsTable.getColumns().addAll(col1, col2, col3, col4);
        studentsTable.setMinWidth(1000);
        
        col1.setPrefWidth(250);
        col2.setPrefWidth(250);
        col3.setPrefWidth(250);
        col4.setPrefWidth(250);
        
        col1.setCellValueFactory(new PropertyValueFactory("firstName"));
        col2.setCellValueFactory(new PropertyValueFactory("lastName"));
        col3.setCellValueFactory(new PropertyValueFactory("team"));
        col4.setCellValueFactory(new PropertyValueFactory("role"));
        
        studentsTable.setItems(csg.getDataHandler().getStudents());
        
        firstNameLabel.setPadding(new Insets(0, 60, 0, 0));
        
        teamsComboBox.setItems(csg.getDataHandler().getTeams());
        
        HBox header = new HBox(studentsHeader, studentsDeleteButton);
        header.setSpacing(5);
        
        GridPane dataInput = new GridPane();
        HBox buttons = new HBox(studentsSubmitButton, studentsClearButton);
        buttons.setSpacing(5);
        
        dataInput.add(studentsAddEdit, 0, 0);
        
        dataInput.add(firstNameLabel, 0, 1);
        dataInput.add(lastNameLabel, 0, 2);
        dataInput.add(teamLabel, 0, 3);
        dataInput.add(roleLabel, 0, 4);
        dataInput.add(buttons, 0, 5);
        
        dataInput.add(firstNameTextField, 1, 1);
        dataInput.add(lastNameTextField, 1, 2);
        dataInput.add(teamsComboBox, 1, 3);
        dataInput.add(roleTextField, 1, 4);
        
        VBox vbox = new VBox(header, studentsTable, dataInput);
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        studentsDeleteButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleDeleteStudent();
        });
        
        studentsTable.setOnKeyPressed(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleKeyPressStudent(e.getCode());
        });
        
        studentsTable.setOnMouseClicked(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleStudentTableClick();
        });
        
        studentsClearButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleStudentClear();
        });
        
        studentsSubmitButton.setOnAction(e -> {
            csg.getWorkspaceHandler().getControllerHandler().getProjectController().handleStudentSubmit();
        });
        
        return new Pane(vbox);
    }
    
    // ALL GETTERS

    public Pane getProjectPane() {
        return projectPane;
    }

    public Label getProjectHeader() {
        return projectHeader;
    }

    public Pane getTeamsPane() {
        return teamsPane;
    }

    public Label getTeamsHeader() {
        return teamsHeader;
    }

    public Label getTeamsAddEdit() {
        return teamsAddEdit;
    }

    public Label getNameLabel() {
        return nameLabel;
    }

    public Label getColorLabel() {
        return colorLabel;
    }

    public Label getTextColorLabel() {
        return textColorLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Button getTeamsDeleteButton() {
        return teamsDeleteButton;
    }

    public Button getTeamsSubmitButton() {
        return teamsSubmitButton;
    }

    public Button getTeamsClearButton() {
        return teamsClearButton;
    }

    public TableView getTeamsTable() {
        return teamsTable;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getLinkTextField() {
        return linkTextField;
    }

    public ColorPicker getColorPicker() {
        return colorPicker;
    }

    public ColorPicker getTextColorPicker() {
        return textColorPicker;
    }

    public Pane getStudentsPane() {
        return studentsPane;
    }

    public Label getStudentsHeader() {
        return studentsHeader;
    }

    public Label getStudentsAddEdit() {
        return studentsAddEdit;
    }

    public Label getFirstNameLabel() {
        return firstNameLabel;
    }

    public Label getLastNameLabel() {
        return lastNameLabel;
    }

    public Label getTeamLabel() {
        return teamLabel;
    }

    public Label getRoleLabel() {
        return roleLabel;
    }

    public TableView getStudentsTable() {
        return studentsTable;
    }

    public Button getStudentsDeleteButton() {
        return studentsDeleteButton;
    }

    public Button getStudentsSubmitButton() {
        return studentsSubmitButton;
    }

    public Button getStudentsClearButton() {
        return studentsClearButton;
    }

    public TextField getFirstNameTextField() {
        return firstNameTextField;
    }

    public TextField getLastNameTextField() {
        return lastNameTextField;
    }

    public TextField getRoleTextField() {
        return roleTextField;
    }

    public ComboBox getTeamsComboBox() {
        return teamsComboBox;
    }
    
}