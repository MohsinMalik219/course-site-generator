package csg.workspace;

import csg.CSG;
import csg.CSGProp;
import csg.workspace.controller.ControllerHandler;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class deals with the workspace GUI.
 * @author Mohsin
 */
public class WorkspaceHandler {
    
    private CSG csg;
    private Stage primaryStage;
    private Scene primaryScene;
    private ControllerHandler controllerHandler;
    private VBox everything;
    public static final String FILE_PROTOCOL = "file:";
    public static final String IMAGE_PATH = "./img/";
    
    // All the tabs.
    private TabPane tabPane;
    private CourseTab courseTab;
    private TATab taTab;
    private RecitationTab recitationTab;
    private ScheduleTab scheduleTab;
    private ProjectTab projectTab;
    
    // The toolbar buttons.
    private List<Button> buttons = new ArrayList<>();
    private Button newButton;
    private Button loadButton;
    private Button saveButton;
    private Button saveAsButton;
    private Button exportButton;
    private Button exitButton;
    private Button undoButton;
    private Button redoButton;
    private Button aboutButton;
    
    public WorkspaceHandler(CSG csg, Stage primaryStage) {
        this.csg = csg;
        this.primaryStage = primaryStage;
        controllerHandler = new ControllerHandler(csg, this);
        initWorkspace();
    }
    
    /**
     * This method is only ran one time to initialize the workspace.
     */
    private void initWorkspace() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // Get screen width and height.
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        double screenWidth = primaryScreenBounds.getWidth();
        double screenHeight = primaryScreenBounds.getHeight();
        
        // Set screen width and height.
        double width = screenWidth / 1.3D;
        double height = screenHeight - 100D;
        primaryStage.setX((screenWidth - width) / 2);
        primaryStage.setY(((screenHeight - height) / 2) - 30);
        
        primaryStage.setTitle(props.getProperty(CSGProp.APP_TITLE));
        
        FlowPane toolbarPane = new FlowPane();
        
        // Create new button instances.
        newButton = new Button();
        loadButton = new Button();
        saveButton = new Button();
        saveAsButton = new Button();
        exportButton = new Button();
        undoButton = new Button();
        redoButton = new Button();
        aboutButton = new Button();
        exitButton = new Button();
        
        // Initialize toolbar buttons.
        initToolbarButton(newButton, CSGProp.NEW_BUTTON_FILE_NAME, CSGProp.NEW_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(loadButton, CSGProp.LOAD_BUTTON_FILE_NAME, CSGProp.LOAD_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(saveButton, CSGProp.SAVE_BUTTON_FILE_NAME, CSGProp.SAVE_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(saveAsButton, CSGProp.SAVE_AS_BUTTON_FILE_NAME, CSGProp.SAVE_AS_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(exportButton, CSGProp.EXPORT_BUTTON_FILE_NAME, CSGProp.EXPORT_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(undoButton, CSGProp.UNDO_BUTTON_FILE_NAME, CSGProp.UNDO_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(redoButton, CSGProp.REDO_BUTTON_FILE_NAME, CSGProp.REDO_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(aboutButton, CSGProp.ABOUT_BUTTON_FILE_NAME, CSGProp.ABOUT_BUTTON_TOOLTIP, toolbarPane);
        initToolbarButton(exitButton, CSGProp.EXIT_BUTTON_FILE_NAME, CSGProp.EXIT_BUTTON_TOOLTIP, toolbarPane);
        
        toolbarPane.setPadding(new Insets(10, 10, 10, 10));
        
        // Disable certain buttons.
        saveButton.setDisable(true);
        
        // Initialize all the tabs.
        courseTab = new CourseTab(csg, props.getProperty(CSGProp.COURSE_TAB_TITLE));
        taTab = new TATab(csg, props.getProperty(CSGProp.TA_TAB_TITLE));
        recitationTab = new RecitationTab(csg, props.getProperty(CSGProp.RECITATION_TAB_TITLE));
        scheduleTab = new ScheduleTab(csg, props.getProperty(CSGProp.SCHEDULE_TAB_TITLE));
        projectTab = new ProjectTab(csg, props.getProperty(CSGProp.PROJECT_TAB_TITLE));
        
        // Create our tab pane.
        tabPane = new TabPane();
        tabPane.getTabs().addAll(courseTab, taTab, recitationTab, scheduleTab, projectTab);
        //Pane tabPaneEncap = new Pane(tabPane);
        tabPane.setPadding(new Insets(10, 10, 10, 10));
        
        // Create overall alignment.
        everything = new VBox();
        everything.getChildren().addAll(toolbarPane, tabPane);
        
        // Create a new scene with everything and show it.
        primaryScene = new Scene(everything, width, height);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
        
        primaryScene.setOnKeyPressed(e -> {
            controllerHandler.handleKeyPressed(e.getCode());
        });
        
        primaryScene.setOnKeyReleased(e -> {
            controllerHandler.handleKeyReleased(e.getCode());
        });
        
        // Set toolbar button listeners.
        newButton.setOnAction(e -> {
            controllerHandler.handleNew();
        });
        loadButton.setOnAction(e -> {
            controllerHandler.handleLoad();
        });
        saveButton.setOnAction(e -> {
            controllerHandler.handleSave();
        });
        saveAsButton.setOnAction(e -> {
            controllerHandler.handleSaveAs();
        });
        exportButton.setOnAction(e -> {
            controllerHandler.handleExport();
        });
        undoButton.setOnAction(e -> {
            controllerHandler.handleUndo();
        });
        redoButton.setOnAction(e -> {
            controllerHandler.handleRedo();
        });
        aboutButton.setOnAction(e -> {
            controllerHandler.handleAbout();
        });
        exitButton.setOnAction(e -> {
            controllerHandler.handleExit();
        });
    }
    
    private void initToolbarButton(Button button, CSGProp fileNameProp, CSGProp tooltipProp, FlowPane toolbarPane) {
        button.setMinSize(40, 40);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Image image = new Image(FILE_PROTOCOL + IMAGE_PATH + props.getProperty(fileNameProp));
        button.setGraphic(new ImageView(image));
        toolbarPane.getChildren().add(button);
        Tooltip tooltip = new Tooltip(props.getProperty(tooltipProp));
        button.setTooltip(tooltip);
        buttons.add(button);
    }
    
    // ALL GETTERS

    public Button getNewButton() {
        return newButton;
    }

    public Button getLoadButton() {
        return loadButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getSaveAsButton() {
        return saveAsButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public Button getUndoButton() {
        return undoButton;
    }

    public Button getRedoButton() {
        return redoButton;
    }

    public Button getAboutButton() {
        return aboutButton;
    }
    
    public Button getExportButton() {
        return exportButton;
    }

    public CourseTab getCourseTab() {
        return courseTab;
    }

    public TATab getTaTab() {
        return taTab;
    }

    public RecitationTab getRecitationTab() {
        return recitationTab;
    }

    public ScheduleTab getScheduleTab() {
        return scheduleTab;
    }

    public ProjectTab getProjectTab() {
        return projectTab;
    }

    public Scene getPrimaryScene() {
        return primaryScene;
    }

    public List<Button> getButtons() {
        return buttons;
    }
    
    public TabPane getTabPane() {
        return tabPane;
    }
    
    public VBox getEverything() {
        return everything;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ControllerHandler getControllerHandler() {
        return controllerHandler;
    }
    
}