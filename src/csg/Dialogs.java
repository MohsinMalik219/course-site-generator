package csg;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;

/**
 * This class contains methods which allow easy dialog usage.
 * @author Mohsin
 */
public class Dialogs {
    
    private static String response = "";
    public static String YES = "Yes";
    public static String NO = "No";
    public static String CANCEL = "Cancel";
    
    /**
     * Perform initialization of yes, no, and cancel text buttons.
     * @param yes The text to use for Yes buttons.
     * @param no The text to use for No buttons.
     * @param cancel The text to use for Cancel buttons.
     */
    public static void initYesNoCancel(String yes, String no, String cancel) {
        YES = yes;
        NO = no;
        CANCEL = cancel;
    }
    
    /**
     * Shows a simple dialog message. The program will halt until the user
     * closes the dialog.
     * @param title The title of the dialog box.
     * @param message The message to be displayed.
     */
    public static void showMessageDialog(String title, String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    /**
     * Shows a Yes, No, Cancel dialog box. The program will halt until the
     * user clicks a button. Get the response with getResponse().
     * @param title The title of the dialog box.
     * @param message The message to be displayed.
     */
    public static void showYesNoCancelDialog(String title, String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        
        ButtonType yesButton = new ButtonType(YES);
        ButtonType noButton = new ButtonType(NO);
        ButtonType cancelButton = new ButtonType(CANCEL, ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(yesButton, noButton, cancelButton);
        
        ButtonType result = (ButtonType) alert.showAndWait().get();
        if(result == yesButton) {
            response = YES;
        } else if(result == noButton) {
            response = NO;
        } else {
            response = CANCEL;
        }
    }
    
    public static String getResponse() {
        return response;
    }
    
    public static String getYes() {
        return YES;
    }
    
    public static String getNo() {
        return NO;
    }
    
    public static String getCancel() {
        return CANCEL;
    }
    
}