package csg.file;

import csg.CSG;
import csg.CSGProp;
import csg.data.DataHandler;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import csg.data.SitePage;
import csg.data.Student;
import csg.data.TeachingAssistant;
import csg.data.Team;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.StringProperty;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;

/**
 * The file handler deals with saving/loading and exporting files.
 * @author Mohsin
 */
public class FileHandler {
    
    private DataHandler data;
    private CSG csg;
    private static final String JSON_NAME = "name";
    private static final String JSON_EMAIL = "email";
    private static final String JSON_UNDERGRAD = "undergrad";
    private static final String JSON_DAY = "day";
    private static final String JSON_TIME = "time";
    private static final String JSON_SECTION = "section";
    private static final String JSON_DAY_TIME = "day_time";
    private static final String JSON_TA_1 = "ta_1";
    private static final String JSON_TA_2 = "ta_2";
    private static final String JSON_LOCATION = "location";
    private static final String JSON_MONTH = "month";
    private static final String JSON_TITLE = "title";
    private static final String JSON_LINK = "link";
    private static final String JSON_CRITERIA = "criteria";
    private static final String JSON_TOPIC = "topic";
    private static final String JSON_HOLIDAYS = "holidays";
    private static final String JSON_LECTURES = "lectures";
    private static final String JSON_REFERENCES = "references";
    private static final String JSON_RECITATIONS = "recitations";
    private static final String JSON_HWS = "hws";
    private static final String JSON_COLOR = "color";
    private static final String JSON_TEXT_COLOR = "text_color";
    private static final String JSON_FIRST_NAME = "firstName";
    private static final String JSON_LAST_NAME = "lastName";
    private static final String JSON_TEAM = "team";
    private static final String JSON_ROLE = "role";
    private static final String JSON_TAS = "tas";
    private static final String JSON_OFFICE_HOURS = "officeHours";
    private static final String JSON_SCHEDULE_ITEMS = "scheduleItems";
    private static final String JSON_TEAMS = "teams";
    private static final String JSON_STUDENTS = "students";
    private static final String JSON_EXPORT_DIR = "exportDir";
    private static final String JSON_TEMPLATE_DIR = "templateDir";
    private static final String JSON_INST_NAME = "instName";
    private static final String JSON_INST_HOME = "instHome";
    private static final String JSON_SUBJECT = "subject";
    private static final String JSON_SEMESTER = "semester";
    private static final String JSON_NUMBER = "number";
    private static final String JSON_YEAR = "year";
    private static final String JSON_STYLESHEET = "stylesheet";
    private static final String JSON_BANNER_PATH = "bannerPath";
    private static final String JSON_LEFT_FOOTER_PATH = "leftFooterPath";
    private static final String JSON_RIGHT_FOOTER_PATH = "rightFooterPath";
    private static final String JSON_USE = "use";
    private static final String JSON_INDEX = "index";
    private static final String JSON_START_HOUR = "startHour";
    private static final String JSON_END_HOUR = "endHour";
    private static final String JSON_STARTING_MONDAY = "startingMonday";
    private static final String JSON_ENDING_FRIDAY = "endingFriday";
    private static final String JSON_PAGES = "pages";
    private static final String JSON_INST = "inst";
    private static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    private static final String JSON_GRAD_TAS = "grad_tas";
    private static final String JSON_STARTING_MONDAY_MONTH = "startingMondayMonth";
    private static final String JSON_STARTING_MONDAY_DAY = "startingMondayDay";
    private static final String JSON_ENDING_FRIDAY_MONTH = "endingFridayMonth";
    private static final String JSON_ENDING_FRIDAY_DAY = "endingFridayDay";
    private static final String JSON_PROJECTS = "projects";
    private static final String JSON_WORK = "work";
    private static final String JSON_FILE_NAME = "fileName";
    
    /**
     * Create a new FileHandler.
     * @param csg The app.
     * @param data The DataHandler we will use for all methods.
     */
    public FileHandler(CSG csg, DataHandler data) {
        this.csg = csg;
        this.data = data;
    }
    
    /**
     * Loads data from the given JSON file.
     * @param path The path to the JSON file.
     * @return true if loading was successful. Otherwise false.
     */
    public boolean load(String path) {
        data.resetData();
        
        JsonObject json;
        try {
            json = loadJsonFile(path);
        } catch (IOException ex) {
            return false;
        }
        
        data.setStartAndEndHour(json.getInt(JSON_START_HOUR), json.getInt(JSON_END_HOUR));
        data.exportDirProperty().setValue(json.getString(JSON_EXPORT_DIR));
        data.templateDirProperty().setValue(json.getString(JSON_TEMPLATE_DIR));
        data.titleProperty().setValue(json.getString(JSON_TITLE));
        data.instNameProperty().setValue(json.getString(JSON_INST_NAME));
        data.instHomeProperty().setValue(json.getString(JSON_INST_HOME));
        data.subjectProperty().setValue(json.getString(JSON_SUBJECT));
        data.numberProperty().setValue(json.getString(JSON_NUMBER));
        data.yearProperty().setValue(json.getString(JSON_YEAR));
        data.stylesheetProperty().setValue(json.getString(JSON_STYLESHEET));
        data.setBannerImagePath(json.getString(JSON_BANNER_PATH));
        data.setLeftImagePath(json.getString(JSON_LEFT_FOOTER_PATH));
        data.setRightImagePath(json.getString(JSON_RIGHT_FOOTER_PATH));
        
        int semesterIndex = json.getInt(JSON_SEMESTER);
        if(semesterIndex != -1) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            List<String> semesters = props.getPropertyOptionsList(CSGProp.SEMESTERS);
            data.semesterProperty().setValue(semesters.get(json.getInt(JSON_SEMESTER)));
        } else {
            data.semesterProperty().setValue("");
        }
        
        String sm = json.getString(JSON_STARTING_MONDAY);
        String ef = json.getString(JSON_ENDING_FRIDAY);
        if(!sm.equals("")) {
            String[] startingMonday = sm.split("-");
            LocalDate monday = LocalDate.of(Integer.valueOf(startingMonday[0]), Integer.valueOf(startingMonday[1]), Integer.valueOf(startingMonday[2]));
            if(csg != null) {
                csg.getWorkspaceHandler().getScheduleTab().setPreviousStartingMonday(monday);
                csg.getWorkspaceHandler().getScheduleTab().setPropertyMonday(true);
            }
            data.startingMondayProperty().setValue(monday);
        }
        if(!ef.equals("")) {
            String[] endingFriday = ef.split("-");
            LocalDate friday = LocalDate.of(Integer.valueOf(endingFriday[0]), Integer.valueOf(endingFriday[1]), Integer.valueOf(endingFriday[2]));
            if(csg != null) {
                csg.getWorkspaceHandler().getScheduleTab().setPreviousEndingFriday(friday);
                csg.getWorkspaceHandler().getScheduleTab().setPropertyFriday(true);
            }
            data.endingFridayProperty().setValue(friday);
        }
        
        JsonArray pagesArray = json.getJsonArray(JSON_PAGES);
        for(int i = 0; i < pagesArray.size(); i++) {
            JsonObject pageObject = pagesArray.getJsonObject(i);
            int index = pageObject.getInt(JSON_INDEX);
            boolean use = pageObject.getBoolean(JSON_USE);
            SitePage sitePage = new SitePage(index, use, csg);
            data.getPages().add(sitePage);
        }
        
        JsonArray taArray = json.getJsonArray(JSON_TAS);
        for(int i = 0; i < taArray.size(); i++) {
            JsonObject taObject = taArray.getJsonObject(i);
            String name = taObject.getString(JSON_NAME);
            String email = taObject.getString(JSON_EMAIL);
            boolean undergrad = taObject.getBoolean(JSON_UNDERGRAD);
            data.addTA(name, email, undergrad);
        }
        
        JsonArray officeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for(int i = 0; i < officeHoursArray.size(); i++) {
            JsonObject officeHoursObject = officeHoursArray.getJsonObject(i);
            String key = getKey(officeHoursObject.getString(JSON_TIME), officeHoursObject.getString(JSON_DAY));
            String name = officeHoursObject.getString(JSON_NAME);
            data.addTAToCell(name, key);
        }
        
        JsonArray recitationArray = json.getJsonArray(JSON_RECITATIONS);
        for(int i = 0; i < recitationArray.size(); i++) {
            JsonObject recitationObject = recitationArray.getJsonObject(i);
            String section = recitationObject.getString(JSON_SECTION);
            String dayTime = recitationObject.getString(JSON_DAY_TIME);
            String location = recitationObject.getString(JSON_LOCATION);
            String ta1 = recitationObject.getString(JSON_TA_1);
            String ta2 = recitationObject.getString(JSON_TA_2);
            String inst = recitationObject.getString(JSON_INST);
            Recitation recitation = new Recitation(section, inst, dayTime, location, ta1, ta2);
            data.getRecitations().add(recitation);
        }
        
        JsonObject scheduleItemsObject = json.getJsonObject(JSON_SCHEDULE_ITEMS);
        
        JsonArray holidaysArray = scheduleItemsObject.getJsonArray(JSON_HOLIDAYS);
        JsonArray lecturesArray = scheduleItemsObject.getJsonArray(JSON_LECTURES);
        JsonArray referencesArray = scheduleItemsObject.getJsonArray(JSON_REFERENCES);
        JsonArray recitationsArray = scheduleItemsObject.getJsonArray(JSON_RECITATIONS);
        JsonArray hwsArray = scheduleItemsObject.getJsonArray(JSON_HWS);
        
        for(int i = 0; i < holidaysArray.size(); i++) {
            JsonObject holidayObject = holidaysArray.getJsonObject(i);
            int month = holidayObject.getInt(JSON_MONTH);
            int day = holidayObject.getInt(JSON_DAY);
            int year = holidayObject.getInt(JSON_YEAR);
            String date = month + "/" + day + "/" + year;
            String title = holidayObject.getString(JSON_TITLE);
            String link = holidayObject.getString(JSON_LINK);
            String topic = holidayObject.getString(JSON_TOPIC);
            String time = holidayObject.getString(JSON_TIME);
            String criteria = holidayObject.getString(JSON_CRITERIA);
            
            ScheduleItem scheduleItem = new ScheduleItem(ScheduleItemType.HOLIDAY, date, title, topic, time, link, criteria);
            data.addScheduleItem(scheduleItem);
        }
        for(int i = 0; i < lecturesArray.size(); i++) {
            JsonObject lectureObject = lecturesArray.getJsonObject(i);
            int month = lectureObject.getInt(JSON_MONTH);
            int day = lectureObject.getInt(JSON_DAY);
            int year = lectureObject.getInt(JSON_YEAR);
            String date = month + "/" + day + "/" + year;
            String title = lectureObject.getString(JSON_TITLE);
            String link = lectureObject.getString(JSON_LINK);
            String topic = lectureObject.getString(JSON_TOPIC);
            String time = lectureObject.getString(JSON_TIME);
            String criteria = lectureObject.getString(JSON_CRITERIA);
            
            ScheduleItem scheduleItem = new ScheduleItem(ScheduleItemType.LECTURE, date, title, topic, time, link, criteria);
            data.addScheduleItem(scheduleItem);
        }
        for(int i = 0; i < referencesArray.size(); i++) {
            JsonObject referenceObject = referencesArray.getJsonObject(i);
            int month = referenceObject.getInt(JSON_MONTH);
            int day = referenceObject.getInt(JSON_DAY);
            int year = referenceObject.getInt(JSON_YEAR);
            String date = month + "/" + day + "/" + year;
            String title = referenceObject.getString(JSON_TITLE);
            String link = referenceObject.getString(JSON_LINK);
            String topic = referenceObject.getString(JSON_TOPIC);
            String time = referenceObject.getString(JSON_TIME);
            String criteria = referenceObject.getString(JSON_CRITERIA);
            
            ScheduleItem scheduleItem = new ScheduleItem(ScheduleItemType.REFERENCE, date, title, topic, time, link, criteria);
            data.addScheduleItem(scheduleItem);
        }
        for(int i = 0; i < recitationsArray.size(); i++) {
            JsonObject recitationObject = recitationsArray.getJsonObject(i);
            int month = recitationObject.getInt(JSON_MONTH);
            int day = recitationObject.getInt(JSON_DAY);
            int year = recitationObject.getInt(JSON_YEAR);
            String date = month + "/" + day + "/" + year;
            String title = recitationObject.getString(JSON_TITLE);
            String topic = recitationObject.getString(JSON_TOPIC);
            String time = recitationObject.getString(JSON_TIME);
            String link = recitationObject.getString(JSON_LINK);
            String criteria = recitationObject.getString(JSON_CRITERIA);
            
            ScheduleItem scheduleItem = new ScheduleItem(ScheduleItemType.RECITATION, date, title, topic, time, link, criteria);
            data.addScheduleItem(scheduleItem);
        }
        for(int i = 0; i < hwsArray.size(); i++) {
            JsonObject hwObject = hwsArray.getJsonObject(i);
            int month = hwObject.getInt(JSON_MONTH);
            int day = hwObject.getInt(JSON_DAY);
            int year = hwObject.getInt(JSON_YEAR);
            String date = month + "/" + day + "/" + year;
            String title = hwObject.getString(JSON_TITLE);
            String topic = hwObject.getString(JSON_TOPIC);
            String time = hwObject.getString(JSON_TIME);
            String link = hwObject.getString(JSON_LINK);
            String criteria = hwObject.getString(JSON_CRITERIA);
            
            ScheduleItem scheduleItem = new ScheduleItem(ScheduleItemType.HOMEWORK, date, title, topic, time, link, criteria);
            data.addScheduleItem(scheduleItem);
        }
        
        JsonArray teamArray = json.getJsonArray(JSON_TEAMS);
        for(int i = 0; i < teamArray.size(); i++) {
            JsonObject teamObject = teamArray.getJsonObject(i);
            String name = teamObject.getString(JSON_NAME);
            String color = teamObject.getString(JSON_COLOR);
            String textColor = teamObject.getString(JSON_TEXT_COLOR);
            String link = teamObject.getString(JSON_LINK);
            Team team = new Team(name, color, textColor, link);
            data.getTeams().add(team);
        }
        
        JsonArray studentArray = json.getJsonArray(JSON_STUDENTS);
        for(int i = 0; i < studentArray.size(); i++) {
            JsonObject studentObject = studentArray.getJsonObject(i);
            String firstName = studentObject.getString(JSON_FIRST_NAME);
            String lastName = studentObject.getString(JSON_LAST_NAME);
            String team = studentObject.getString(JSON_TEAM);
            String role = studentObject.getString(JSON_ROLE);
            Student student = new Student(firstName, lastName, team, role);
            data.getStudents().add(student);
        }
        
        return true;
    }
    
    private JsonObject loadJsonFile(String path) throws IOException {
        InputStream is = new FileInputStream(path);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    /**
     * Saves all data to a JSON file at the given path.
     * @param path The path to the JSON file.
     * @return true is saving was successful. Otherwise false.
     */
    public boolean save(String path) {
        // TA JSON data
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        for(TeachingAssistant ta : data.getTas()) {
            JsonObject taObject = Json.createObjectBuilder()
                .add(JSON_NAME, ta.nameProperty().getValue())
                .add(JSON_EMAIL, ta.emailProperty().getValue())
                .add(JSON_UNDERGRAD, ta.undergradProperty().getValue())
                .build();
            taArrayBuilder.add(taObject);
        }
        JsonArray taArray = taArrayBuilder.build();
        
        // Office hours JSON data
        JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Map<String, StringProperty> officeHourStringProps = data.getOfficeHourStringProps();
        for(String key : officeHourStringProps.keySet()) {
            StringProperty prop = officeHourStringProps.get(key);
            if(prop.getValue() != null) {
                int col = getCol(key);
                int row = getRow(key);
                String day = getDay(col);
                String time = getTime(row);
                String[] taNames = prop.getValue().split("\n");
                for(String taName : taNames) {
                    JsonObject taObject = Json.createObjectBuilder()
                        .add(JSON_DAY, day)
                        .add(JSON_TIME, time)
                        .add(JSON_NAME, taName)
                        .build();
                    officeHoursArrayBuilder.add(taObject);
                }
            }
        }
        JsonArray officeHoursArray = officeHoursArrayBuilder.build();
        
        // Recitation JSON data
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        for(Recitation recitation : data.getRecitations()) {
            JsonObject recitationObject = Json.createObjectBuilder()
                .add(JSON_SECTION, recitation.sectionProperty().getValue())
                .add(JSON_DAY_TIME, recitation.dayTimeProperty().getValue())
                .add(JSON_LOCATION, recitation.locationProperty().getValue())
                .add(JSON_TA_1, recitation.ta1Property().getValue())
                .add(JSON_TA_2  , recitation.ta2Property().getValue())
                .add(JSON_INST, recitation.instProperty().getValue())
                .build();
            recitationArrayBuilder.add(recitationObject);
        }
        JsonArray recitationArray = recitationArrayBuilder.build();
        
        // Schedule JSON data
        JsonArrayBuilder scheduleHolidaysArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleLecturesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleReferencesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleRecitationsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleHWsArrayBuilder = Json.createArrayBuilder();
        for(ScheduleItem scheduleItem : data.getScheduleItems()) {
            ScheduleItemType type = scheduleItem.getScheduleItemType();
            String[] dateArray = scheduleItem.dateProperty().getValue().split("/");
            int month = Integer.valueOf(dateArray[0]);
            int day = Integer.valueOf(dateArray[1]);
            int year = Integer.valueOf(dateArray[2]);
            if(type.equals(ScheduleItemType.HOLIDAY)) {
                JsonObject holidayObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_YEAR, year)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleHolidaysArrayBuilder.add(holidayObject);
            } else if(type.equals(ScheduleItemType.LECTURE)) {
                JsonObject lectureObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_YEAR, year)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleLecturesArrayBuilder.add(lectureObject);
            } else if(type.equals(ScheduleItemType.REFERENCE)) {
                JsonObject referenceObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_YEAR, year)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleReferencesArrayBuilder.add(referenceObject);
            } else if(type.equals(ScheduleItemType.RECITATION)) {
                JsonObject recitationObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_YEAR, year)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleRecitationsArrayBuilder.add(recitationObject);
            } else if(type.equals(ScheduleItemType.HOMEWORK)) {
                JsonObject hwObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_YEAR, year)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleHWsArrayBuilder.add(hwObject);
            }
        }
        
        JsonArray scheduleHolidaysArray = scheduleHolidaysArrayBuilder.build();
        JsonArray scheduleLecturesArray = scheduleLecturesArrayBuilder.build();
        JsonArray scheduleReferencesArray = scheduleReferencesArrayBuilder.build();
        JsonArray scheduleRecitationsArray = scheduleRecitationsArrayBuilder.build();
        JsonArray scheduleHWsArray = scheduleHWsArrayBuilder.build();
        
        JsonObject scheduleObject = Json.createObjectBuilder()
            .add(JSON_HOLIDAYS, scheduleHolidaysArray)
            .add(JSON_LECTURES, scheduleLecturesArray)
            .add(JSON_REFERENCES, scheduleReferencesArray)
            .add(JSON_RECITATIONS, scheduleRecitationsArray)
            .add(JSON_HWS, scheduleHWsArray)
            .build();
        
        // Project Json building
        
        JsonArrayBuilder teamsArrayBuilder = Json.createArrayBuilder();
        for(Team team : data.getTeams()) {
            JsonObject teamObject = Json.createObjectBuilder()
                .add(JSON_NAME, team.nameProperty().getValue())
                .add(JSON_COLOR, team.colorProperty().getValue())
                .add(JSON_TEXT_COLOR, team.textColorProperty().getValue())
                .add(JSON_LINK, team.linkProperty().getValue())
                .build();
            teamsArrayBuilder.add(teamObject);
        }
        JsonArray teamsArray = teamsArrayBuilder.build();
        
        JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
        for(Student student : data.getStudents()) {
            JsonObject studentObject = Json.createObjectBuilder()
                .add(JSON_FIRST_NAME, student.firstNameProperty().getValue())
                .add(JSON_LAST_NAME, student.lastNameProperty().getValue())
                .add(JSON_TEAM, student.teamProperty().getValue())
                .add(JSON_ROLE, student.roleProperty().getValue())
                .build();
            studentsArrayBuilder.add(studentObject);
        }
        JsonArray studentsArray = studentsArrayBuilder.build();
        
        JsonArrayBuilder pagesArrayBuilder = Json.createArrayBuilder();
        for(SitePage page : data.getPages()) {
            JsonObject pageObject = Json.createObjectBuilder()
                .add(JSON_INDEX, page.getIndex())
                .add(JSON_USE, page.useProperty().getValue())
                .build();
            pagesArrayBuilder.add(pageObject);
        }
        JsonArray pagesArray = pagesArrayBuilder.build();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        List<String> semesters = props.getPropertyOptionsList(CSGProp.SEMESTERS);
        int semesterIndex = semesters.indexOf(data.semesterProperty().getValue());
        
        String startingMonday = data.startingMondayProperty().getValue() == null ? "" : data.startingMondayProperty().getValue().toString();
        String endingFriday = data.endingFridayProperty().getValue() == null ? "" : data.endingFridayProperty().getValue().toString();
        
        JsonObject everything = Json.createObjectBuilder()
            .add(JSON_TAS, taArray)
            .add(JSON_OFFICE_HOURS, officeHoursArray)
            .add(JSON_RECITATIONS, recitationArray)
            .add(JSON_SCHEDULE_ITEMS, scheduleObject)
            .add(JSON_TEAMS, teamsArray)
            .add(JSON_STUDENTS, studentsArray)
            .add(JSON_PAGES, pagesArray)
            .add(JSON_EXPORT_DIR, data.exportDirProperty().getValue())
            .add(JSON_TEMPLATE_DIR, data.templateDirProperty().getValue())
            .add(JSON_TITLE, data.titleProperty().getValue())
            .add(JSON_INST_NAME, data.instNameProperty().getValue())
            .add(JSON_INST_HOME, data.instHomeProperty().getValue())
            .add(JSON_SUBJECT, data.subjectProperty().getValue())
            .add(JSON_SEMESTER, semesterIndex)
            .add(JSON_NUMBER, data.numberProperty().getValue())
            .add(JSON_YEAR, data.yearProperty().getValue())
            .add(JSON_STYLESHEET, data.stylesheetProperty().getValue())
            .add(JSON_BANNER_PATH, data.getBannerImagePath())
            .add(JSON_LEFT_FOOTER_PATH, data.getLeftImagePath())
            .add(JSON_RIGHT_FOOTER_PATH, data.getRightImagePath())
            .add(JSON_START_HOUR, data.getStartHour())
            .add(JSON_END_HOUR, data.getEndHour())
            .add(JSON_STARTING_MONDAY, startingMonday)
            .add(JSON_ENDING_FRIDAY, endingFriday)
            .build();
        
        return buildJsonFile(everything, path);
    }
    
    /**
     * Exports all data to a working website.
     * @param path The path to where we should export.
     * @return true if exporting was successful. Otherwise false.
     */
    public boolean export(String path) {
        File file = new File(path);
        if(!file.exists()) {
            file.mkdirs();
        }
        
        if(!path.endsWith("/") && !path.endsWith("\\")) {
            path += "/";
        }
        
        File templateDir = new File(data.templateDirProperty().getValue());
        if(!templateDir.exists() || !templateDir.isDirectory()) {
            return false;
        }
        for(File f : templateDir.listFiles()) {
            try {
                if(f.isDirectory()) {
                    FileUtils.copyDirectoryToDirectory(f, file);
                } else if(f.isFile()) {
                    boolean copy = true;
                    for(SitePage page : data.getPages()) {
                        if(page.htmlProperty().getValue().equals(f.getName()) && !page.useProperty().getValue()) {
                            copy = false;
                        }
                    }
                    if(copy) {
                        FileUtils.copyFileToDirectory(f, file);
                    }
                }
            } catch(IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        
        /*List<File> htmlFiles = new ArrayList<>();
        File templateDir = new File(data.templateDirProperty().getValue());
        if(!templateDir.exists() || !templateDir.isDirectory()) {
            return false;
        }
        for(File htmlFile : templateDir.listFiles()) {
            String fileName = htmlFile.getName();
            int index = SitePage.getFileNameIndex(fileName);
            if(index != -1) {
                htmlFiles.add(htmlFile);
            }
        }
        try {
            copyFilesToDir(htmlFiles, file);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }*/
        
        File imagesDir = new File(path + "images");
        if(!imagesDir.exists()) {
            imagesDir.mkdirs();
        }
        List<File> images = new ArrayList<>();
        String banner = data.getBannerImagePath();
        if(!banner.equals("")) {
            File bFile = new File(banner);
            if(bFile.exists() && bFile.isFile()) {
                images.add(bFile);
            } else {
                csg.getWorkspaceHandler().getCourseTab().resetBanner();
                data.setBannerImagePath("");
            }
        }
        String left = data.getLeftImagePath();
        if(!left.equals("")) {
            File lFile = new File(left);
            if(lFile.exists() && lFile.isFile()) {
                images.add(lFile);
            } else {
                csg.getWorkspaceHandler().getCourseTab().resetLeftFooter();
                data.setLeftImagePath("");
            }
        }
        String right = data.getRightImagePath();
        if(!right.equals("")) {
            File rFile = new File(right);
            if(rFile.exists() && rFile.isFile()) {
                images.add(rFile);
            } else {
                csg.getWorkspaceHandler().getCourseTab().resetRightFooter();
                data.setRightImagePath("");
            }
        }
        try {
            copyFilesToDir(images, imagesDir);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        
        File cssDir = new File(path + "css");
        if(!cssDir.exists()) {
            cssDir.mkdirs();
        }
        String cssPath = csg.getDataHandler().stylesheetProperty().getValue();
        if(cssPath != null && !cssPath.equals("")) {
            File cssFile = new File("work/css/" + cssPath);
            if(cssFile.exists() && cssFile.isFile()) {
                try {
                    FileUtils.copyFileToDirectory(cssFile, cssDir);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        
        File jsDir = new File(path + "js");
        if(!jsDir.exists()) {
            jsDir.mkdirs();
        }
        
        File jsWorkDir = new File("work/js");
        for(File jsFile : jsWorkDir.listFiles()) {
            String jsFileName = jsFile.getName();
            if(data.isJSFileUsed(jsFileName)) {
                try {
                    FileUtils.copyFileToDirectory(jsFile, jsDir);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        
        String jsDirStr = jsDir.getAbsolutePath() + "/";
        
        return buildTAsData(jsDirStr + "TAsData.json") && buildRecitationsData(jsDirStr + "RecitationsData.json")
            && buildScheduleData(jsDirStr + "ScheduleData.json") && buildTeamsAndStudents(jsDirStr + "TeamsAndStudents.json")
            && buildProjectsData(jsDirStr + "ProjectsData.json") && buildCourseInfoData(jsDirStr + "CourseInfo.json");
    }
    
    private void copyFilesToDir(List<File> files, File dir) throws IOException {
        for(File file : files) {
            FileUtils.copyFileToDirectory(file, dir);
        }
    }
    
    private boolean buildCourseInfoData(String path) {
        JsonArrayBuilder pagesArrayBuilder = Json.createArrayBuilder();
        Collections.sort(data.getPages());
        for(SitePage page : data.getPages()) {
            if(page.useProperty().getValue()) {
                JsonObject pageObject = Json.createObjectBuilder()
                    .add(JSON_NAME, page.nameProperty().getValue())
                    .add(JSON_FILE_NAME, page.htmlProperty().getValue())
                    .build();
                pagesArrayBuilder.add(pageObject);
            }
        }
        JsonArray pagesArray = pagesArrayBuilder.build();
        
        JsonObject everything = Json.createObjectBuilder()
            .add(JSON_PAGES, pagesArray)
            .add(JSON_BANNER_PATH, "images/" + getFileNameFromPath(data.getBannerImagePath()))
            .add(JSON_LEFT_FOOTER_PATH, "images/" + getFileNameFromPath(data.getLeftImagePath()))
            .add(JSON_RIGHT_FOOTER_PATH, "images/" + getFileNameFromPath(data.getRightImagePath()))
            .add(JSON_STYLESHEET, "css/" + getFileNameFromPath(data.stylesheetProperty().getValue()))
            .add(JSON_SUBJECT, data.subjectProperty().getValue())
            .add(JSON_SEMESTER, data.semesterProperty().getValue())
            .add(JSON_NUMBER, data.numberProperty().getValue())
            .add(JSON_YEAR, data.yearProperty().getValue())
            .add(JSON_TITLE, data.titleProperty().getValue())
            .add(JSON_INST_NAME, data.instNameProperty().getValue())
            .add(JSON_INST_HOME, data.instHomeProperty().getValue())
            .build();
        
        return buildJsonFile(everything, path);
    }
    
    private String getFileNameFromPath(String path) {
        path = path.replace("\\", "/");
        String[] pathSplit = path.split("/");
        return pathSplit[pathSplit.length - 1];
    }
    
    private boolean buildProjectsData(String path) {
        JsonArrayBuilder projectsArrayBuilder = Json.createArrayBuilder();
        for(Team team : data.getTeams()) {
            JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
            for(Student student : data.getStudents()) {
                if(student.teamProperty().getValue().equals(team.nameProperty().getValue())) {
                    String fullName = student.firstNameProperty().getValue() + " " + student.lastNameProperty().getValue();
                    studentsArrayBuilder.add(fullName);
                }
            }
            
            JsonObject teamObject = Json.createObjectBuilder()
                .add(JSON_NAME, team.nameProperty().getValue())
                .add(JSON_STUDENTS, studentsArrayBuilder.build())
                .add(JSON_LINK, team.linkProperty().getValue())
                .build();
            projectsArrayBuilder.add(teamObject);
        }
        JsonArray projectsArray = projectsArrayBuilder.build();
        
        String semester = data.semesterProperty().getValue() + " " + data.yearProperty().getValue();
        
        JsonObject workObject = Json.createObjectBuilder()
            .add(JSON_SEMESTER, semester)
            .add(JSON_PROJECTS, projectsArray)
            .build();
        
        JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
        workArrayBuilder.add(workObject);
        JsonArray workArray = workArrayBuilder.build();
        
        JsonObject everything = Json.createObjectBuilder().add(JSON_WORK, workArray).build();
        
        return buildJsonFile(everything, path);
    }
    
    private boolean buildTeamsAndStudents(String path) {
        JsonArrayBuilder teamsArrayBuilder = Json.createArrayBuilder();
        for(Team team : data.getTeams()) {
            JsonObject teamObject = Json.createObjectBuilder()
                .add(JSON_NAME, team.nameProperty().getValue())
                .add(JSON_COLOR, team.colorProperty().getValue())
                .add(JSON_TEXT_COLOR, team.textColorProperty().getValue())
                .build();
            teamsArrayBuilder.add(teamObject);
        }
        JsonArray teamsArray = teamsArrayBuilder.build();
        
        JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
        for(Student student : data.getStudents()) {
            JsonObject studentObject = Json.createObjectBuilder()
                .add(JSON_FIRST_NAME, student.firstNameProperty().getValue())
                .add(JSON_LAST_NAME, student.lastNameProperty().getValue())
                .add(JSON_TEAM, student.teamProperty().getValue())
                .add(JSON_ROLE, student.roleProperty().getValue())
                .build();
            studentsArrayBuilder.add(studentObject);
        }
        JsonArray studentsArray = studentsArrayBuilder.build();
        
        JsonObject everything = Json.createObjectBuilder()
            .add(JSON_TEAMS, teamsArray)
            .add(JSON_STUDENTS, studentsArray)
            .build();
        
        return buildJsonFile(everything, path);
    }
    
    private boolean buildScheduleData(String path) {
        JsonArrayBuilder scheduleHolidaysArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleLecturesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleReferencesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleRecitationsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder scheduleHWsArrayBuilder = Json.createArrayBuilder();
        
        for(ScheduleItem scheduleItem : data.getScheduleItems()) {
            ScheduleItemType type = scheduleItem.getScheduleItemType();
            String[] dateArray = scheduleItem.dateProperty().getValue().split("/");
            int month = Integer.valueOf(dateArray[0]);
            int day = Integer.valueOf(dateArray[1]);
            if(type.equals(ScheduleItemType.HOLIDAY)) {
                JsonObject holidayObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .build();
                scheduleHolidaysArrayBuilder.add(holidayObject);
            } else if(type.equals(ScheduleItemType.LECTURE)) {
                JsonObject lectureObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .build();
                scheduleLecturesArrayBuilder.add(lectureObject);
            } else if(type.equals(ScheduleItemType.REFERENCE)) {
                JsonObject referenceObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .build();
                scheduleReferencesArrayBuilder.add(referenceObject);
            } else if(type.equals(ScheduleItemType.RECITATION)) {
                JsonObject recitationObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .build();
                scheduleRecitationsArrayBuilder.add(recitationObject);
            } else if(type.equals(ScheduleItemType.HOMEWORK)) {
                JsonObject hwObject = Json.createObjectBuilder()
                    .add(JSON_MONTH, month)
                    .add(JSON_DAY, day)
                    .add(JSON_TITLE, scheduleItem.titleProperty().getValue())
                    .add(JSON_LINK, scheduleItem.linkProperty().getValue())
                    .add(JSON_TOPIC, scheduleItem.topicProperty().getValue())
                    .add(JSON_TIME, scheduleItem.timeProperty().getValue())
                    .add(JSON_CRITERIA, scheduleItem.criteriaProperty().getValue())
                    .build();
                scheduleHWsArrayBuilder.add(hwObject);
            }
        }
        
        JsonArray scheduleHolidaysArray = scheduleHolidaysArrayBuilder.build();
        JsonArray scheduleLecturesArray = scheduleLecturesArrayBuilder.build();
        JsonArray scheduleReferencesArray = scheduleReferencesArrayBuilder.build();
        JsonArray scheduleRecitationsArray = scheduleRecitationsArrayBuilder.build();
        JsonArray scheduleHWsArray = scheduleHWsArrayBuilder.build();
        
        String startingMondayMonth = "";
        String startingMondayDay = "";
        String endingFridayMonth = "";
        String endingFridayDay = "";
        if(data.startingMondayProperty().getValue() != null) {
            String[] startingMonday = data.startingMondayProperty().getValue().toString().split("-");
            startingMondayMonth = startingMonday[1];
            startingMondayDay = startingMonday[2];
        }
        if(data.endingFridayProperty().getValue() != null) {
            String[] endingFriday = data.endingFridayProperty().getValue().toString().split("-");
            endingFridayMonth = endingFriday[1];
            endingFridayDay = endingFriday[2];
        }
        
        if(startingMondayDay.startsWith("0")) {
            startingMondayDay = startingMondayDay.substring(1);
        }
        if(startingMondayMonth.startsWith("0")) {
            startingMondayMonth = startingMondayMonth.substring(1);
        }
        if(endingFridayDay.startsWith("0")) {
            endingFridayDay = endingFridayDay.substring(1);
        }
        if(endingFridayMonth.startsWith("0")) {
            endingFridayMonth = endingFridayMonth.substring(1);
        }
        
        JsonObject everything = Json.createObjectBuilder()
            .add(JSON_STARTING_MONDAY_MONTH, startingMondayMonth)
            .add(JSON_STARTING_MONDAY_DAY, startingMondayDay)
            .add(JSON_ENDING_FRIDAY_MONTH, endingFridayMonth)
            .add(JSON_ENDING_FRIDAY_DAY, endingFridayDay)
            .add(JSON_HOLIDAYS, scheduleHolidaysArray)
            .add(JSON_LECTURES, scheduleLecturesArray)
            .add(JSON_REFERENCES, scheduleReferencesArray)
            .add(JSON_RECITATIONS, scheduleRecitationsArray)
            .add(JSON_HWS, scheduleHWsArray)
            .build();
        
        return buildJsonFile(everything, path);
    }
    
    private boolean buildRecitationsData(String path) {
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        
        for(Recitation recitation : data.getRecitations()) {
            String section = recitation.sectionProperty().getValue() + " (" + recitation.instProperty().getValue() + ")";
            
            JsonObject recitationObject = Json.createObjectBuilder()
                .add(JSON_SECTION, section)
                .add(JSON_DAY_TIME, recitation.dayTimeProperty().getValue())
                .add(JSON_LOCATION, recitation.locationProperty().getValue())
                .add(JSON_TA_1, recitation.ta1Property().getValue())
                .add(JSON_TA_2  , recitation.ta2Property().getValue())
                .build();
            recitationArrayBuilder.add(recitationObject);
        }
        
        JsonArray recitationArray = recitationArrayBuilder.build();
        
        JsonObject everything = Json.createObjectBuilder().add(JSON_RECITATIONS, recitationArray).build();
        
        return buildJsonFile(everything, path);
    }
    
    private boolean buildTAsData(String path) {
        JsonArrayBuilder undergradTAsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gradTAsArrayBuilder = Json.createArrayBuilder();
        
        for(TeachingAssistant ta : data.getTas()) {
            JsonObject taObject = Json.createObjectBuilder()
                .add(JSON_NAME, ta.nameProperty().getValue())
                .add(JSON_EMAIL, ta.emailProperty().getValue())
                .build();
            if(ta.undergradProperty().getValue()) {
                undergradTAsArrayBuilder.add(taObject);
            } else {
                gradTAsArrayBuilder.add(taObject);
            }
        }
        
        JsonArray undergradTAsArray = undergradTAsArrayBuilder.build();
        JsonArray gradTAsArray = gradTAsArrayBuilder.build();
        
        JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Map<String, StringProperty> officeHourStringProps = data.getOfficeHourStringProps();
        
        for(String key : officeHourStringProps.keySet()) {
            StringProperty prop = officeHourStringProps.get(key);
            if(prop.getValue() != null && !prop.getValue().equals("")) {
                int col = getCol(key);
                int row = getRow(key);
                String day = getDay(col);
                String time = getTime(row);
                String[] taNames = prop.getValue().split("\n");
                for(String taName : taNames) {
                    JsonObject taObject = Json.createObjectBuilder()
                        .add(JSON_DAY, day)
                        .add(JSON_TIME, time)
                        .add(JSON_NAME, taName)
                        .build();
                    officeHoursArrayBuilder.add(taObject);
                }
            }
        }
        
        JsonArray officeHoursArray = officeHoursArrayBuilder.build();
        
        JsonObject everything = Json.createObjectBuilder()
            .add(JSON_START_HOUR, data.getStartHour() + "")
            .add(JSON_END_HOUR, data.getEndHour() + "")
            .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
            .add(JSON_GRAD_TAS, gradTAsArray)
            .add(JSON_OFFICE_HOURS, officeHoursArray)
            .build();
        
        return buildJsonFile(everything, path);
    }
    
    private boolean buildJsonFile(JsonObject everything, String path) {
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(everything);
        jsonWriter.close();
        
        try {
            OutputStream os = new FileOutputStream(path);
            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(everything);
            String p = sw.toString();
            PrintWriter pw = new PrintWriter(path);
            pw.write(p);
            pw.close();
        } catch (FileNotFoundException ex) {
            return false;
        }
        
        return true;
    }
    
    private String getDay(int col) {
        switch(col) {
            case 2:
                return "MONDAY";
            case 3:
                return "TUESDAY";
            case 4:
                return "WEDNESDAY";
            case 5:
                return "THURSDAY";
            case 6:
                return "FRIDAY";
            default:
                return null;
        }
    }
    
    private int getColFromDay(String day) {
        switch(day) {
            case "MONDAY":
                return 2;
            case "TUESDAY":
                return 3;
            case "WEDNESDAY":
                return 4;
            case "THURSDAY":
                return 5;
            case "FRIDAY":
                return 6;
            default:
                return -1;
        }
    }
    
    public String getTime(int row) {
        double hour = .5 * (row + 2 * data.getStartHour() - 1);
        String time = "";
        String suffix = "";
        if(hour <= 11.5) {
            if(hour < 1) {
                time += "12_";
            } else {
                time += (int) Math.floor(hour) + "_";
            }
            suffix = "am";
        } else {
            if(hour < 13) {
                time += "12_";
            } else {
                time += (int) Math.floor(hour - 12) + "_";
            }
            suffix = "pm";
        }
        if(Math.floor(hour) == hour) {
            time += "00";
        } else {
            time += "30";
        }
        return time + suffix;
    }
    
    public String getKey(String time, String day) {
        String[] timeSplit = time.split("_");
        int hour = Integer.valueOf(timeSplit[0]);
        if(time.endsWith("am")) {
            if(hour == 12) {
                hour = 0;
            }
        } else if(hour != 12) {
            hour += 12;
        }
        int row = (hour - data.getStartHour()) * 2 + 1;
        if(timeSplit[1].contains("30")) {
            row++;
        }
        int col = getColFromDay(day);
        return buildKey(col, row);
    }
    
    private String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    private int getCol(String key) {
        return Integer.valueOf(key.split("_")[0]);
    }
    
    private int getRow(String key) {
        return Integer.valueOf(key.split("_")[1]);
    }
    
}