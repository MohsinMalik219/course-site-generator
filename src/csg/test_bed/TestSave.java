package csg.test_bed;

import csg.data.DataHandler;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import csg.data.SitePage;
import csg.data.Student;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.file.FileHandler;
import java.io.File;
import java.time.LocalDate;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import properties_manager.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;

/**
 * This class will test saving for this application.
 * @author Mohsin
 */
public class TestSave {
    
    private static final String APP_PROPERTIES = "app_properties.xml";
    private static final String APP_PROPERTIES_SPANISH = "app_properties_spanish.xml";
    private static final String APP_SCHEMA = "properties_schema.xsd";
    private static final String DATA_PATH = "./data/";
    public static final String ENGLISH = "English";
    private static final String SPANISH = "Spanish";
    private static boolean loadedProperties = false;
    
    /**
     * Main method for testing the saving functionality.
     * @return 
     */
    public static DataHandler save() {
        DataHandler data = new DataHandler();
        FileHandler file = new FileHandler(null, data);
        
        File workFile = new File("work");
        String desktop = workFile.getAbsolutePath() + "\\";
        
        // Course data
        data.exportDirProperty().setValue("work/export");
        data.templateDirProperty().setValue("work/templates/CSE308_Template");
        data.titleProperty().setValue("Computer Science III (02)");
        data.instNameProperty().setValue("Richard McKenna");
        data.instHomeProperty().setValue("http://www3.cs.stonybrook.edu/~richard/");
        data.subjectProperty().setValue("CSE");
        data.numberProperty().setValue("219");
        data.semesterProperty().setValue("Spring");
        data.yearProperty().setValue("2017");
        data.setBannerImagePath("work/images/banner.png");
        data.setLeftImagePath("work/images/left.jpg");
        data.setRightImagePath("work/images/right.png");
        data.stylesheetProperty().setValue("sea_wolf.css");
        populateSitePages(data.getPages());
        
        // TA data
        data.setStartAndEndHour(8, 20);
        populateTAs(data.getTas());
        populateGrid(data.getOfficeHourStringProps());
        
        // Recitation data
        populateRecitations(data.getRecitations());
        
        // Schedule data
        populateScheduleItems(data.getScheduleItems());
        data.startingMondayProperty().setValue(LocalDate.of(2017, 3, 13));
        data.endingFridayProperty().setValue(LocalDate.of(2017, 5, 12));
        
        // Project data
        populateTeams(data.getTeams());
        populateStudents(data.getStudents());
        
        // Save data
        file.save(desktop + "SiteSaveTest.json");
        
        return data;
    }
    
    public static DataHandler load() {
        DataHandler data = new DataHandler();
        FileHandler file = new FileHandler(null, data);
        
        File workFile = new File("work");
        file.load(workFile.getAbsolutePath() + "/SiteSaveTest.json");
        
        return data;
    }
    
    /**
     * Fill the site pages list with hard coded data.
     * @param pages 
     */
    private static void populateSitePages(ObservableList<SitePage> pages) {
        pages.add(new SitePage(0, true, null));
        pages.add(new SitePage(1, true, null));
        pages.add(new SitePage(2, true, null));
        pages.add(new SitePage(3, true, null));
        pages.add(new SitePage(4, false, null));
    }
    
    private static void populateTAs(ObservableList<TeachingAssistant> tas) {
        tas.add(new TeachingAssistant("Mohsin Malik", "mohsin.malik@stonybrook.edu", true, null));
        tas.add(new TeachingAssistant("Saad Malik", "saad.malik@stonybrook.edu", true, null));
        tas.add(new TeachingAssistant("Conrad Kenan", "conrad.kenan@stonybrook.edu", true, null));
        tas.add(new TeachingAssistant("Nathan Webster", "nathan.webster@stonybrook.edu", true, null));
        tas.add(new TeachingAssistant("Spencer Kohli", "spencer.kohli@stonybrook.edu", false, null));
        tas.add(new TeachingAssistant("Jacob Drew", "jacob.drew@stonybrook.edu", false, null));
    }
    
    private static void populateGrid(Map<String, StringProperty> gridSlots) {
        gridSlots.put(buildKey(2, 3), new SimpleStringProperty("Mohsin Malik"));
        gridSlots.put(buildKey(2, 4), new SimpleStringProperty("Mohsin Malik"));
        gridSlots.put(buildKey(2, 5), new SimpleStringProperty("Mohsin Malik\nSaad Malik"));
        gridSlots.put(buildKey(2, 6), new SimpleStringProperty("Saad Malik"));
        gridSlots.put(buildKey(2, 7), new SimpleStringProperty("Saad Malik"));
        
        gridSlots.put(buildKey(4, 10), new SimpleStringProperty("Conrad Kenan"));
        gridSlots.put(buildKey(4, 11), new SimpleStringProperty("Conrad Kenan"));
        gridSlots.put(buildKey(4, 12), new SimpleStringProperty("Conrad Kenan\nNathan Webster"));
        gridSlots.put(buildKey(4, 13), new SimpleStringProperty("Nathan Webster"));
        gridSlots.put(buildKey(4, 14), new SimpleStringProperty("Nathan Webster"));
    }
    
    private static String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    private static void populateRecitations(ObservableList<Recitation> recitations) {
        recitations.add(new Recitation("R01", "McKenna", "Monday 5:30-6:23pm", "Old CS 2120", "Mohsin Malik", "Saad Malik"));
        recitations.add(new Recitation("R02", "Banerjee", "Wednesday 2:00-2:53pm", "New CS 106", "Conrad Kenan", "Nathan Webster"));
    }
    
    private static void populateScheduleItems(ObservableList<ScheduleItem> scheduleItems) {
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOLIDAY, "3/13/2017", "SPRING BREAK", "", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOLIDAY, "3/14/2017", "SPRING BREAK", "", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOLIDAY, "3/15/2017", "SPRING BREAK", "", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOLIDAY, "3/16/2017", "SPRING BREAK", "", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOLIDAY, "3/17/2017", "SPRING BREAK", "", "", "", ""));
        
        scheduleItems.add(new ScheduleItem(ScheduleItemType.LECTURE, "4/20/2017", "Lecture 26", "Learning some JavaFX", "", "google.com", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.LECTURE, "4/25/2017", "Lecture 27", "Learning some more JavaFX", "", "google.com", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.LECTURE, "4/27/2017", "Lecture 28", "Learning even more JavaFX", "", "google.com", ""));
        
        scheduleItems.add(new ScheduleItem(ScheduleItemType.REFERENCE, "4/20/2017", "<br /><br />Reference", "JavaFX API", "", "javafxapi.com", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.REFERENCE, "4/27/2017", "<br /><br />Reference", "Example Reference", "", "reference.com", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.REFERENCE, "4/25/2017", "<br /><br />Reference", "Another Reference", "", "anotherreference.com", ""));
        
        scheduleItems.add(new ScheduleItem(ScheduleItemType.RECITATION, "4/17/2017", "Recitation 10", "Sparky/Javadoc", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.RECITATION, "4/18/2017", "Recitation 10", "Sparky/Javadoc", "", "", ""));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.RECITATION, "4/19/2017", "Recitation 10", "Sparky/Javadoc", "", "", ""));
        
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOMEWORK, "4/21/2017", "HW 1", "due @ 11:59pm", "11:59pm", "hw1.com", "hw1criteria.com"));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOMEWORK, "5/1/2017", "HW 2", "due @ 11:59pm", "11:59pm", "hw2.com", "hw2criteria.com"));
        scheduleItems.add(new ScheduleItem(ScheduleItemType.HOMEWORK, "5/10/2017", "HW 3", "due @ 11:59pm", "11:59pm", "hw3.com", "hw3criteria.com"));
    }
    
    private static void populateTeams(ObservableList<Team> teams) {
        teams.add(new Team("Team A", "aaaaaa", "bbbbbb", "teama.com"));
        teams.add(new Team("Team B", "bbbbbb", "cccccc", "teamb.com"));
        teams.add(new Team("Team C", "cccccc", "dddddd", "teamc.com"));
    }
    
    private static void populateStudents(ObservableList<Student> students) {
        students.add(new Student("Josh", "Smith", "Team A", "Data Designer"));
        students.add(new Student("Ben", "Dover", "Team A", "Lead Designer"));
        students.add(new Student("Mick", "Doe", "Team A", "Project Manager"));
        students.add(new Student("Mick", "Doe", "Team A", "Lead Programmer"));
        
        students.add(new Student("Billy", "Bob", "Team B", "Data Designer"));
        students.add(new Student("James", "Bond", "Team B", "Lead Designer"));
        students.add(new Student("Carter", "Calkins", "Team B", "Project Manager"));
        students.add(new Student("Carter", "Calkins", "Team B", "Lead Programmer"));
        
        students.add(new Student("Will", "Smith", "Team C", "Data Designer"));
        students.add(new Student("Drake", "Brown", "Team C", "Lead Designer"));
        students.add(new Student("Joey", "Wrestler", "Team C", "Project Manager"));
        students.add(new Student("Joey", "Wrestler", "Team C", "Lead Programmer"));
    }
    
    public static boolean loadProperties(String lang) {
        if(loadedProperties) {
            return true;
        }
        String path = "";
        if(lang == null) {
            return false;
        } else if(lang.equals(SPANISH)) {
            path = APP_PROPERTIES_SPANISH;
        } else {
            path = APP_PROPERTIES;
        }
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, DATA_PATH);
            props.loadProperties(path, APP_SCHEMA);
            return true;
        } catch (InvalidXMLFileFormatException ex) {
            // TODO: Display error dialog.
            return false;
        }
    }
    
}