package csg.style;

import csg.CSG;
import csg.workspace.CourseTab;
import csg.workspace.ProjectTab;
import csg.workspace.RecitationTab;
import csg.workspace.ScheduleTab;
import csg.workspace.TATab;
import java.net.URL;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * This class deals with the styles which will be applied to the workspace
 * controls and components.
 * @author Mohsin
 */
public class StyleHandler {
    
    private CSG csg;
    private static final String CSS_PATH = "/csg/style/styles.css";
    
    // Classes
    private static final String CLASS_EVERYTHING = "everything";
    private static final String CLASS_OFFICE_HOUR_GRID_PANES = "office_hours_grid_panes";
    private static final String CLASS_HEADER = "header";
    private static final String CLASS_SUB_HEADER = "sub_header";
    private static final String CLASS_BOLD_TEXT = "bold_text";
    private static final String CLASS_SUB_SUB_HEADER = "sub_sub_header";
    private static final String CLASS_OFFICE_HOURS_GRID_HEADER_PANES = "office_hours_grid_header_panes";
    private static final String CLASS_OFFICE_HOURS_GRID_HEADER_LABELS = "office_hours_grid_header_labels";
    private static final String CLASS_OFFICE_HOURS_GRID_TIME_PANES = "office_hours_grid_time_panes";
    private static final String CLASS_OFFICE_HOURS_GRID_TIME_LABELS = "office_hours_grid_time_labels";
    private static final String CLASS_TAB_SECTION = "tab_section";
    
    
    public StyleHandler(CSG csg) {
        this.csg = csg;
        initStylesheet();
        initWorkspaceStyles();
    }
    
    public void styleGrid() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        
        for(Node n : tab.getOfficeHoursGrid().getChildren()) {
            if(n instanceof Pane) {
                Pane p = (Pane) n;
                p.getStyleClass().add(CLASS_OFFICE_HOUR_GRID_PANES);
            }
        }
        
        for(Pane pane : tab.getOfficeHoursGridHeaderPanes().values()) {
            pane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_HEADER_PANES);
        }
        for(Label label : tab.getOfficeHoursGridHeaderLabels().values()) {
            label.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_HEADER_LABELS);
        }
        
        for(Pane pane : tab.getOfficeHoursGridTimePanes().values()) {
            pane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_PANES);
        }
        for(Label label : tab.getOfficeHoursGridTimeLabels().values()) {
            label.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_LABELS);
        }
    }
    
    /**
     * Applies certain classes to certain nodes.
     */
    private void initWorkspaceStyles() {
        // Universal styles
        csg.getWorkspaceHandler().getEverything().getStyleClass().add(CLASS_EVERYTHING);
        
        // Init tab styles
        initCourseTab();
        initTATab();
        initRecitationTab();
        initScheduleTab();
        initProjectTab();
    }
    
    private void initCourseTab() {
        CourseTab tab = csg.getWorkspaceHandler().getCourseTab();
        
        tab.getCourseInfoHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getSiteTemplateHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getPageStyleHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getExportDirLabel().getStyleClass().add(CLASS_BOLD_TEXT);
        tab.getSiteTemplateDirLabel().getStyleClass().add(CLASS_BOLD_TEXT);
        tab.getSitePagesLabel().getStyleClass().add(CLASS_BOLD_TEXT);
        
        tab.getCourseInfoPane().getStyleClass().add(CLASS_TAB_SECTION);
        tab.getSiteTemplatePane().getStyleClass().add(CLASS_TAB_SECTION);
        tab.getPageStylePane().getStyleClass().add(CLASS_TAB_SECTION);
    }
    
    private void initTATab() {
        TATab tab = csg.getWorkspaceHandler().getTaTab();
        
        styleGrid();
        
        tab.getTaHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getOfficeHoursHeaderLabel().getStyleClass().add(CLASS_HEADER);
    }
    
    private void initRecitationTab() {
        RecitationTab tab = csg.getWorkspaceHandler().getRecitationTab();
        
        tab.getRecitationsHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getAddEditHeaderLabel().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getAddEditPane().getStyleClass().add(CLASS_TAB_SECTION);
    }
    
    private void initScheduleTab() {
        ScheduleTab tab = csg.getWorkspaceHandler().getScheduleTab();
        
        tab.getScheduleHeaderLabel().getStyleClass().add(CLASS_HEADER);
        tab.getCalendarBoundariesHeader().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getScheduleItemsHeader().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getAddEditLabel().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getCalendarBoundariesPane().getStyleClass().add(CLASS_TAB_SECTION);
        tab.getScheduleItemsPane().getStyleClass().add(CLASS_TAB_SECTION);
    }
    
    private void initProjectTab() {
        ProjectTab tab = csg.getWorkspaceHandler().getProjectTab();
        
        tab.getProjectHeader().getStyleClass().add(CLASS_HEADER);
        tab.getTeamsHeader().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getTeamsAddEdit().getStyleClass().add(CLASS_SUB_SUB_HEADER);
        tab.getStudentsHeader().getStyleClass().add(CLASS_SUB_HEADER);
        tab.getStudentsAddEdit().getStyleClass().add(CLASS_SUB_SUB_HEADER);
        tab.getTeamsPane().getStyleClass().add(CLASS_TAB_SECTION);
        tab.getStudentsPane().getStyleClass().add(CLASS_TAB_SECTION);
    }
    
    /**
     * Loads the stylesheet.
     */
    private void initStylesheet() {
        URL url = getClass().getResource(CSS_PATH);
        csg.getWorkspaceHandler().getPrimaryScene().getStylesheets().add(url.toExternalForm());
    }
    
}