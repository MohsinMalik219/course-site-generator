/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import csg.data.Recitation;
import csg.data.TeachingAssistant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class RemoveTA implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String email;
    private boolean undergrad;
    private Map<String, String> savedGridText;
    private List<SavedRecitation> savedRecitations;
    
    public RemoveTA(CSG csg, String name, String email, boolean undergrad) {
        this.csg = csg;
        this.name = name;
        this.email = email;
        this.undergrad = undergrad;
        savedGridText = new HashMap<>();
        savedRecitations = new ArrayList<>();
    }

    @Override
    public void doTransaction() {
        TeachingAssistant ta = csg.getDataHandler().getTA(name);
        if(ta != null) {
            csg.getDataHandler().getTas().remove(ta);
        }
        savedGridText.clear();
        for(String key : csg.getDataHandler().getOfficeHourStringProps().keySet()) {
            String text = csg.getDataHandler().getOfficeHourStringProps().get(key).getValue();
            if(text != null && text.contains(name)) {
                savedGridText.put(key, text);
            }
        }
        savedRecitations.clear();
        savedRecitations.addAll(csg.getDataHandler().replaceTAInRecitation(name, ""));
        csg.getDataHandler().removeTAFromAllCells(name);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getDataHandler().addTA(name, email, undergrad);
        for(SavedRecitation savedRecitation : savedRecitations) {
            Recitation recitation = csg.getDataHandler().getRecitation(savedRecitation.getSection());
            if(recitation != null) {
                if(savedRecitation.isTa1()) {
                    recitation.ta1Property().setValue(name);
                } else {
                    recitation.ta2Property().setValue(name);
                }
            }
        }
        savedRecitations.clear();
        for(String key : savedGridText.keySet()) {
            StringProperty prop = csg.getDataHandler().getOfficeHourStringProps().get(key);
            prop.setValue(savedGridText.get(key));
        }
        csg.getDataHandler().setAsChanged();
    }
    
}