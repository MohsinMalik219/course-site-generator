/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import csg.data.Student;
import jtps.jTPS_Transaction;

/**
 * Represents editing a student.
 * @author Mohsin
 */
public class EditStudent implements jTPS_Transaction {
    
    private CSG csg;
    private String firstName;
    private String lastName;
    private String team;
    private String role;
    private String oldFirstName;
    private String oldLastName;
    private String oldTeam;
    private String oldRole;
    
    public EditStudent(CSG csg, String firstName, String lastName, String team, String role, String oldFirstName, String oldLastName, String oldTeam, String oldRole) {
        this.csg = csg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.role = role;
        this.oldFirstName = oldFirstName;
        this.oldLastName = oldLastName;
        this.oldTeam = oldTeam;
        this.oldRole = oldRole;
    }

    @Override
    public void doTransaction() {
        Student student = csg.getDataHandler().getStudent(oldFirstName, oldLastName);
        if(student != null) {
            student.firstNameProperty().setValue(firstName);
            student.lastNameProperty().setValue(lastName);
            student.teamProperty().setValue(team);
            student.roleProperty().setValue(role);
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        Student student = csg.getDataHandler().getStudent(firstName, lastName);
        if(student != null) {
            student.firstNameProperty().setValue(oldFirstName);
            student.lastNameProperty().setValue(oldLastName);
            student.teamProperty().setValue(oldTeam);
            student.roleProperty().setValue(oldRole);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}