package csg.actions;

import csg.CSG;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import jtps.jTPS_Transaction;

/**
 * Represents removing a schedule item.
 * @author Mohsin
 */
public class RemoveScheduleItem implements jTPS_Transaction {
    
    private CSG csg;
    private int id;
    private ScheduleItemType type;
    private String date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    
    public RemoveScheduleItem(CSG csg, int id, ScheduleItemType type, String date, String time, String title, String topic, String link, String criteria) {
        this.csg = csg;
        this.id = id;
        this.type = type;
        this.date = date;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
    }

    @Override
    public void doTransaction() {
        ScheduleItem scheduleItem = csg.getDataHandler().getScheduleItem(id);
        if(scheduleItem != null) {
            csg.getDataHandler().removeScheduleItem(scheduleItem);
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        ScheduleItem scheduleItem = new ScheduleItem(id, type, date, title, topic, time, link, criteria);
        csg.getDataHandler().addScheduleItem(scheduleItem);
        csg.getDataHandler().setAsChanged();
    }
    
}