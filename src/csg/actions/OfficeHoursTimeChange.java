/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class OfficeHoursTimeChange implements jTPS_Transaction {
    
    private CSG csg;
    private int newStartHour;
    private int newEndHour;
    private int oldStartHour;
    private int oldEndHour;
    private Map<String, String> oldLabels;
    
    public OfficeHoursTimeChange(CSG csg, int newStartHour, int newEndHour, int oldStartHour, int oldEndHour) {
        this.csg = csg;
        this.newStartHour = newStartHour;
        this.newEndHour = newEndHour;
        this.oldStartHour = oldStartHour;
        this.oldEndHour = oldEndHour;
        oldLabels = new HashMap<>();
    }

    @Override
    public void doTransaction() {
        oldLabels.clear();
        
        Map<String, StringProperty> slotStringProps = csg.getDataHandler().getOfficeHourStringProps();
        for(String key : slotStringProps.keySet()) {
            String text = csg.getDataHandler().getOfficeHourStringProps().get(key).getValue();
            if(text != null && !text.equals("")) {
                oldLabels.put(key, text);
            }
        }
        
        csg.getDataHandler().setStartAndEndHour(newStartHour, newEndHour);
        //csg.getWorkspaceHandler().getTaTab().buildOfficeHoursGrid(newStartHour, newEndHour);
        
        for(String key : oldLabels.keySet()) {
            int offset = 0;
            if(newStartHour != oldStartHour) {
                offset = (oldStartHour - newStartHour) * 2;
            }
            int col = getCol(key);
            int row = getRow(key) + offset;
            String newKey = buildKey(col, row);
            if(row != 0 && csg.getDataHandler().getOfficeHourStringProps().containsKey(newKey)) {
                String labelText = oldLabels.get(key);
                csg.getDataHandler().getOfficeHourStringProps().get(newKey).setValue(labelText);
            }
        }
        
        csg.getWorkspaceHandler().getTaTab().updateComboBoxes();
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getDataHandler().setStartAndEndHour(oldStartHour, oldEndHour);
        //csg.getWorkspaceHandler().getTaTab().buildOfficeHoursGrid(oldStartHour, oldEndHour);
        
        Map<String, StringProperty> slotStringProps = csg.getDataHandler().getOfficeHourStringProps();
        for(String key : oldLabels.keySet()) {
            String text = oldLabels.get(key);
            slotStringProps.get(key).setValue(text);
        }
        
        oldLabels.clear();
        
        csg.getWorkspaceHandler().getTaTab().updateComboBoxes();
        csg.getDataHandler().setAsChanged();
    }
    
    private String buildKey(int col, int row) {
        return col + "_" + row;
    }
    
    private int getCol(String key) {
        return Integer.valueOf(key.split("_")[0]);
    }
    
    private int getRow(String key) {
        return Integer.valueOf(key.split("_")[1]);
    }
    
}