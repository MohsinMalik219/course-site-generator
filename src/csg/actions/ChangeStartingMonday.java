package csg.actions;

import csg.CSG;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 * Represents changing the starting Monday.
 * @author Mohsin
 */
public class ChangeStartingMonday implements jTPS_Transaction {
    
    private CSG csg;
    private LocalDate oldDate;
    private LocalDate newDate;
    private boolean firstTime = true;
    
    public ChangeStartingMonday(CSG csg, LocalDate newDate, LocalDate oldDate) {
        this.csg = csg;
        this.oldDate = oldDate;
        this.newDate = newDate;
    }

    @Override
    public void doTransaction() {
        if(firstTime) {
            firstTime = false;
        } else {
            csg.getWorkspaceHandler().getScheduleTab().setPropertyMonday(true);
            csg.getWorkspaceHandler().getScheduleTab().getStartingMondayDatePicker().setValue(newDate);
        }
        csg.getWorkspaceHandler().getScheduleTab().setPreviousStartingMonday(newDate);
        csg.getDataHandler().fixScheduleTable();
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getWorkspaceHandler().getScheduleTab().setPropertyMonday(true);
        csg.getWorkspaceHandler().getScheduleTab().getStartingMondayDatePicker().setValue(oldDate);
        csg.getWorkspaceHandler().getScheduleTab().setPreviousStartingMonday(oldDate);
        csg.getDataHandler().fixScheduleTable();
        csg.getDataHandler().setAsChanged();
    }
    
}