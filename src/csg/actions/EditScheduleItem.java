package csg.actions;

import csg.CSG;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import jtps.jTPS_Transaction;

/**
 * Represents editing a schedule item.
 * @author Mohsin
 */
public class EditScheduleItem implements jTPS_Transaction {
    
    private CSG csg;
    private int id;
    private ScheduleItemType type;
    private String date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private ScheduleItemType oldType;
    private String oldDate;
    private String oldTime;
    private String oldTitle;
    private String oldTopic;
    private String oldLink;
    private String oldCriteria;
    
    public EditScheduleItem(CSG csg, int id, ScheduleItemType type, String date, String time, String title, String topic, String link, String criteria,
        ScheduleItemType oldType, String oldDate, String oldTime, String oldTitle, String oldTopic, String oldLink, String oldCriteria) {
        this.csg = csg;
        this.type = type;
        this.date = date;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        this.oldType = oldType;
        this.oldDate = oldDate;
        this.oldTime = oldTime;
        this.oldTitle = oldTitle;
        this.oldTopic = oldTopic;
        this.oldLink = oldLink;
        this.oldCriteria = oldCriteria;
    }

    @Override
    public void doTransaction() {
        ScheduleItem scheduleItem = csg.getDataHandler().getScheduleItem(id);
        if(scheduleItem != null) {
            scheduleItem.typeProperty().setValue(type.toString());
            scheduleItem.dateProperty().setValue(date);
            scheduleItem.timeProperty().setValue(time);
            scheduleItem.titleProperty().setValue(title);
            scheduleItem.topicProperty().setValue(topic);
            scheduleItem.linkProperty().setValue(link);
            scheduleItem.criteriaProperty().setValue(criteria);
            csg.getDataHandler().updateScheduleItem(scheduleItem);
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        ScheduleItem scheduleItem = csg.getDataHandler().getScheduleItem(id);
        if(scheduleItem != null) {
            scheduleItem.typeProperty().setValue(oldType.toString());
            scheduleItem.dateProperty().setValue(oldDate);
            scheduleItem.timeProperty().setValue(oldTime);
            scheduleItem.titleProperty().setValue(oldTitle);
            scheduleItem.topicProperty().setValue(oldTopic);
            scheduleItem.linkProperty().setValue(oldLink);
            scheduleItem.criteriaProperty().setValue(oldCriteria);
            csg.getDataHandler().updateScheduleItem(scheduleItem);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}