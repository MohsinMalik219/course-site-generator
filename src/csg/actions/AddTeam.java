package csg.actions;

import csg.CSG;
import csg.data.Team;
import jtps.jTPS_Transaction;

/**
 * Represents adding a team.
 * @author Mohsin
 */
public class AddTeam implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String color;
    private String textColor;
    private String link;
    
    public AddTeam(CSG csg, String name, String color, String textColor, String link) {
        this.csg = csg;
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
    }

    @Override
    public void doTransaction() {
        Team team = new Team(name, color, textColor, link);
        csg.getDataHandler().getTeams().add(team);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        Team team = csg.getDataHandler().getTeam(name);
        if(team != null) {
            csg.getDataHandler().getTeams().remove(team);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}