package csg.actions;

import csg.CSG;
import csg.data.Student;
import csg.data.Team;
import java.util.ArrayList;
import java.util.List;
import jtps.jTPS_Transaction;

/**
 * Represents removing a team.
 * @author Mohsin
 */
public class RemoveTeam implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String color;
    private String textColor;
    private String link;
    private List<Student> savedStudents;
    
    public RemoveTeam(CSG csg, String name, String color, String textColor, String link) {
        this.csg = csg;
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
        savedStudents = new ArrayList<>();
    }

    @Override
    public void doTransaction() {
        Team team = csg.getDataHandler().getTeam(name);
        if(team != null) {
            csg.getDataHandler().getTeams().remove(team);
            savedStudents.addAll(csg.getDataHandler().removeStudents(team));
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        Team team = new Team(name, color, textColor, link);
        csg.getDataHandler().getTeams().add(team);
        for(Student student : savedStudents) {
            csg.getDataHandler().getStudents().add(student);
        }
        savedStudents.clear();
        csg.getDataHandler().setAsChanged();
    }
    
}