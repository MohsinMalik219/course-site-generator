/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import csg.CSGProp;
import csg.data.SitePage;
import java.io.File;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author Mohsin
 */
public class ChangeTemplateDir implements jTPS_Transaction {
    
    private CSG csg;
    private String newTemplateDir;
    private String oldTemplateDir;
    private String noneSelected;
    
    public ChangeTemplateDir(CSG csg, String newTemplateDir, String oldTemplateDir) {
        this.csg = csg;
        this.newTemplateDir = newTemplateDir;
        this.oldTemplateDir = oldTemplateDir;
        noneSelected = PropertiesManager.getPropertiesManager().getProperty(CSGProp.NONE_SELECTED);
    }

    @Override
    public void doTransaction() {
        detectPagesAndAdd(newTemplateDir);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        if(oldTemplateDir.equals(noneSelected)) {
            reset();
        } else {
            detectPagesAndAdd(oldTemplateDir);
        }
        csg.getDataHandler().setAsChanged();
    }
    
    private void detectPagesAndAdd(String path) {
        File file = new File(path);
        if(file.exists() && file.isDirectory() && csg.getDataHandler().isTemplateValid(path)) {
            csg.getDataHandler().templateDirProperty().setValue(path);
            csg.getDataHandler().getPages().clear();
            for(File f : file.listFiles()) {
                String fileName = f.getName();
                int index = SitePage.getFileNameIndex(fileName);
                if(index != -1) {
                    csg.getDataHandler().getPages().add(new SitePage(index, true, csg));
                }
            }
        } else {
            reset();
        }
    }
    
    private void reset() {
        csg.getDataHandler().getPages().clear();
        csg.getDataHandler().templateDirProperty().setValue(noneSelected);
    }
    
}