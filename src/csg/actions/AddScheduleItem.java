package csg.actions;

import csg.CSG;
import csg.data.ScheduleItem;
import csg.data.ScheduleItem.ScheduleItemType;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class AddScheduleItem implements jTPS_Transaction {
    
    private CSG csg;
    private ScheduleItemType type;
    private String date;
    private String title;
    private String topic;
    private String time;
    private String link;
    private String criteria;
    private int id = -1;
    
    public AddScheduleItem(CSG csg, ScheduleItemType type, String date, String title, String topic, String time, String link, String criteria) {
        this.csg = csg;
        this.type = type;
        this.date = date;
        this.title = title;
        this.topic = topic;
        this.time = time;
        this.link = link;
        this.criteria = criteria;
    }

    @Override
    public void doTransaction() {
        ScheduleItem scheduleItem = null;
        if(id == -1) {
            scheduleItem = new ScheduleItem(type, date, title, topic, time, link, criteria);
            id = scheduleItem.getId();
        } else {
            scheduleItem = new ScheduleItem(id, type, date, title, topic, time, link, criteria);
        }
        csg.getDataHandler().addScheduleItem(scheduleItem);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        ScheduleItem scheduleItem = csg.getDataHandler().getScheduleItem(id);
        if(scheduleItem != null) {
            csg.getDataHandler().removeScheduleItem(scheduleItem);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}