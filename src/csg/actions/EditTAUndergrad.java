/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import csg.data.TeachingAssistant;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class EditTAUndergrad implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private boolean newUndergradState;
    
    public EditTAUndergrad(CSG csg, String name, boolean newUndergradState) {
        this.csg = csg;
        this.name = name;
        this.newUndergradState = newUndergradState;
    }

    @Override
    public void doTransaction() {
        TeachingAssistant ta = csg.getDataHandler().getTA(name);
        if(ta != null) {
            ta.undergradProperty().setValue(newUndergradState);
        }
    }

    @Override
    public void undoTransaction() {
        TeachingAssistant ta = csg.getDataHandler().getTA(name);
        if(ta != null) {
            ta.undergradProperty().setValue(!newUndergradState);
        }
    }
    
}