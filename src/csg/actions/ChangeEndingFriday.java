package csg.actions;

import csg.CSG;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 * Represents changing the starting Monday.
 * @author Mohsin
 */
public class ChangeEndingFriday implements jTPS_Transaction {
    
    private CSG csg;
    private LocalDate oldDate;
    private LocalDate newDate;
    private boolean firstTime = true;
    
    public ChangeEndingFriday(CSG csg, LocalDate newDate, LocalDate oldDate) {
        this.csg = csg;
        this.oldDate = oldDate;
        this.newDate = newDate;
    }

    @Override
    public void doTransaction() {
        if(firstTime) {
            firstTime = false;
        } else {
            csg.getWorkspaceHandler().getScheduleTab().setPropertyFriday(true);
            csg.getWorkspaceHandler().getScheduleTab().getEndingFridayDatePicker().setValue(newDate);
        }
        csg.getWorkspaceHandler().getScheduleTab().setPreviousEndingFriday(newDate);
        csg.getDataHandler().fixScheduleTable();
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getWorkspaceHandler().getScheduleTab().setPropertyFriday(true);
        csg.getWorkspaceHandler().getScheduleTab().getEndingFridayDatePicker().setValue(oldDate);
        csg.getWorkspaceHandler().getScheduleTab().setPreviousEndingFriday(oldDate);
        csg.getDataHandler().fixScheduleTable();
        csg.getDataHandler().setAsChanged();
    }
    
}