/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class ChangeExportDir implements jTPS_Transaction {
    
    private CSG csg;
    private String newExportDir;
    private String oldExportDir;
    
    public ChangeExportDir(CSG csg, String newExportDir, String oldExportDir) {
        this.csg = csg;
        this.newExportDir = newExportDir;
        this.oldExportDir = oldExportDir;
    }

    @Override
    public void doTransaction() {
        csg.getDataHandler().exportDirProperty().setValue(newExportDir);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getDataHandler().exportDirProperty().setValue(oldExportDir);
        csg.getDataHandler().setAsChanged();
    }
    
}