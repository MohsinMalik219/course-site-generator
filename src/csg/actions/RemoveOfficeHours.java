/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class RemoveOfficeHours implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String key;
    
    public RemoveOfficeHours(CSG csg, String name, String key) {
        this.csg = csg;
        this.name = name;
        this.key = key;
    }

    @Override
    public void doTransaction() {
        csg.getDataHandler().removeTAFromCell(name, key);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getDataHandler().addTAToCell(name, key);
        csg.getDataHandler().setAsChanged();
    }
    
}