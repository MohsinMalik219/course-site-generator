package csg.actions;

import csg.CSG;
import csg.data.Recitation;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class EditRecitation implements jTPS_Transaction {
    
    private CSG csg;
    private String section;
    private String inst;
    private String dayTime;
    private String location;
    private String ta1;
    private String ta2;
    private String oldSection;
    private String oldInst;
    private String oldDayTime;
    private String oldLocation;
    private String oldTA1;
    private String oldTA2;
    
    public EditRecitation(CSG csg, String section, String inst, String dayTime, String location, String ta1, String ta2,
        String oldSection, String oldInst, String oldDayTime, String oldLocation, String oldTA1, String oldTA2) {
        this.csg = csg;
        this.section = section;
        this.inst = inst;
        this.dayTime = dayTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
        this.oldSection = oldSection;
        this.oldInst = oldInst;
        this.oldDayTime = oldDayTime;
        this.oldLocation = oldLocation;
        this.oldTA1 = oldTA1;
        this.oldTA2 = oldTA2;
    }

    @Override
    public void doTransaction() {
        Recitation recitation = csg.getDataHandler().getRecitation(oldSection);
        if(recitation != null) {
            recitation.sectionProperty().setValue(section);
            recitation.instProperty().setValue(inst);
            recitation.dayTimeProperty().setValue(dayTime);
            recitation.locationProperty().setValue(location);
            recitation.ta1Property().setValue(ta1);
            recitation.ta2Property().setValue(ta2);
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        Recitation recitation = csg.getDataHandler().getRecitation(section);
        if(recitation != null) {
            recitation.sectionProperty().setValue(oldSection);
            recitation.instProperty().setValue(oldInst);
            recitation.dayTimeProperty().setValue(oldDayTime);
            recitation.locationProperty().setValue(oldLocation);
            recitation.ta1Property().setValue(oldTA1);
            recitation.ta2Property().setValue(oldTA2);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}