/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

/**
 * A saved recitation for editing/deleting TAs.
 * @author Mohsin
 */
public class SavedRecitation {
    
    private String section;
    private boolean ta1;

    public SavedRecitation(String section, boolean ta1) {
        this.section = section;
        this.ta1 = ta1;
    }

    public String getSection() {
        return section;
    }

    public boolean isTa1() {
        return ta1;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setTa1(boolean ta1) {
        this.ta1 = ta1;
    }
    
}