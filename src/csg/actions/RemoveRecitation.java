package csg.actions;

import csg.CSG;
import csg.data.Recitation;
import jtps.jTPS_Transaction;

/**
 * Represents removing a Recitation.
 * @author Mohsin
 */
public class RemoveRecitation implements jTPS_Transaction {
    
    private CSG csg;
    private String section;
    private String inst;
    private String dayTime;
    private String location;
    private String ta1;
    private String ta2;
    
    public RemoveRecitation(CSG csg, String section, String inst, String dayTime, String location, String ta1, String ta2) {
        this.csg = csg;
        this.section = section;
        this.inst = inst;
        this.dayTime = dayTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
    }

    @Override
    public void doTransaction() {
        Recitation recitation = csg.getDataHandler().getRecitation(section);
        csg.getDataHandler().getRecitations().remove(recitation);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        Recitation recitation = new Recitation(section, inst, dayTime, location, ta1, ta2);
        csg.getDataHandler().getRecitations().add(recitation);
        csg.getDataHandler().setAsChanged();
    }
    
}