package csg.actions;

import csg.CSG;
import csg.data.Student;
import jtps.jTPS_Transaction;

/**
 * Represents adding a student.
 * @author Mohsin
 */
public class AddStudent implements jTPS_Transaction {
    
    private CSG csg;
    private String firstName;
    private String lastName;
    private String team;
    private String role;
    
    public AddStudent(CSG csg, String firstName, String lastName, String team, String role) {
        this.csg = csg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.role = role;
    }

    @Override
    public void doTransaction() {
        Student student = new Student(firstName, lastName, team, role);
        csg.getDataHandler().getStudents().add(student);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        Student student = csg.getDataHandler().getStudent(firstName, lastName);
        if(student != null) {
            csg.getDataHandler().getStudents().remove(student);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}