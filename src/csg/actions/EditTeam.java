package csg.actions;

import csg.CSG;
import csg.data.Team;
import jtps.jTPS_Transaction;

/**
 * Represents editing a team.
 * @author Mohsin
 */
public class EditTeam implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String color;
    private String textColor;
    private String link;
    private String oldName;
    private String oldColor;
    private String oldTextColor;
    private String oldLink;
    
    public EditTeam(CSG csg, String name, String color, String textColor, String link, String oldName, String oldColor, String oldTextColor, String oldLink) {
        this.csg = csg;
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
        this.oldName = oldName;
        this.oldColor = oldColor;
        this.oldTextColor = oldTextColor;
        this.oldLink = oldLink;
    }

    @Override
    public void doTransaction() {
        Team team = csg.getDataHandler().getTeam(oldName);
        if(team != null) {
            team.nameProperty().setValue(name);
            team.colorProperty().setValue(color);
            team.textColorProperty().setValue(textColor);
            team.linkProperty().setValue(link);
            csg.getDataHandler().updateStudents(oldName, name);
            csg.getDataHandler().setAsChanged();
        }
    }

    @Override
    public void undoTransaction() {
        Team team = csg.getDataHandler().getTeam(name);
        if(team != null) {
            team.nameProperty().setValue(oldName);
            team.colorProperty().setValue(oldColor);
            team.textColorProperty().setValue(oldTextColor);
            team.linkProperty().setValue(oldLink);
            csg.getDataHandler().updateStudents(name, oldName);
            csg.getDataHandler().setAsChanged();
        }
    }
    
}