package csg.actions;

import csg.CSG;
import csg.data.Recitation;
import jtps.jTPS_Transaction;

public class AddRecitation implements jTPS_Transaction {
    
    private CSG csg;
    private String section;
    private String inst;
    private String dayTime;
    private String location;
    private String ta1;
    private String ta2;
    
    public AddRecitation(CSG csg, String section, String inst, String dayTime, String location, String ta1, String ta2) {
        this.csg = csg;
        this.section = section;
        this.inst = inst;
        this.dayTime = dayTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
    }

    @Override
    public void doTransaction() {
        csg.getDataHandler().getRecitations().add(new Recitation(section, inst, dayTime, location, ta1, ta2));
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        Recitation r = csg.getDataHandler().getRecitation(section);
        if(r == null) {
            return;
        }
        csg.getDataHandler().getRecitations().remove(r);
        csg.getDataHandler().setAsChanged();
    }
    
}