package csg.actions;

import csg.CSG;
import csg.data.TeachingAssistant;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;

/**
 * Represents editing a TA.
 * @author Mohsin
 */
public class EditTA implements jTPS_Transaction {
    
    private CSG csg;
    private String newName;
    private String oldName;
    private String newEmail;
    private String oldEmail;
    
    public EditTA(CSG csg, String newName, String newEmail, String oldName, String oldEmail) {
        this.csg = csg;
        this.newName = newName;
        this.newEmail = newEmail;
        this.oldName = oldName;
        this.oldEmail = oldEmail;
    }

    @Override
    public void doTransaction() {
        TeachingAssistant ta = csg.getDataHandler().getTA(oldName);
        ta.nameProperty().setValue(newName);
        ta.emailProperty().setValue(newEmail);
        csg.getDataHandler().replaceTAInRecitation(oldName, newName);
        for(StringProperty prop : csg.getDataHandler().getOfficeHourStringProps().values()) {
            String text = prop.getValue();
            if(text != null && text.contains(oldName)) {
                prop.setValue(text.replace(oldName, newName));
            }
        }
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        TeachingAssistant ta = csg.getDataHandler().getTA(newName);
        ta.nameProperty().setValue(oldName);
        ta.emailProperty().setValue(oldEmail);
        csg.getDataHandler().replaceTAInRecitation(newName, oldName);
        for(StringProperty prop : csg.getDataHandler().getOfficeHourStringProps().values()) {
            String text = prop.getValue();
            if(text != null && text.contains(newName)) {
                prop.setValue(text.replace(newName, oldName));
            }
        }
        csg.getDataHandler().setAsChanged();
    }
    
}