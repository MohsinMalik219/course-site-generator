/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.actions;

import csg.CSG;
import jtps.jTPS_Transaction;

/**
 *
 * @author Mohsin
 */
public class AddTA implements jTPS_Transaction {
    
    private CSG csg;
    private String name;
    private String email;
    
    public AddTA(CSG csg, String name, String email) {
        this.csg = csg;
        this.name = name;
        this.email = email;
    }

    @Override
    public void doTransaction() {
        csg.getDataHandler().addTA(name, email);
        csg.getDataHandler().setAsChanged();
    }

    @Override
    public void undoTransaction() {
        csg.getDataHandler().removeTA(name);
        csg.getDataHandler().setAsChanged();
    }
    
}