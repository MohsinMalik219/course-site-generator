package csg;

import csg.data.DataHandler;
import csg.file.FileHandler;
import csg.style.StyleHandler;
import csg.workspace.WorkspaceHandler;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import jtps.jTPS;
import properties_manager.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;

/**
 * The runner class.
 * @author Mohsin
 */
public class CSG extends Application {
    
    private jTPS tps = new jTPS();
    
    private FileHandler fileHandler;
    private DataHandler dataHandler;
    private StyleHandler styleHandler;
    private WorkspaceHandler workspaceHandler;
    
    private static final String APP_PROPERTIES = "app_properties.xml";
    private static final String APP_PROPERTIES_SPANISH = "app_properties_spanish.xml";
    private static final String APP_SCHEMA = "properties_schema.xsd";
    private static final String DATA_PATH = "./data/";
    private static final String ENGLISH = "English";
    private static final String SPANISH = "Spanish";
    private static final String SELECT_LANG = "Select a language";
    private static final String SELECT_LANG_SPAN = "Selecciona un idioma";

    /**
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Get the language the user wants to use.
        String lang = chooseLanguage();
        // Load the properties file.
        if(loadProperties(lang)) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            // Init the language of the dialogs.
            Dialogs.initYesNoCancel(props.getProperty(CSGProp.YES), props.getProperty(CSGProp.NO), props.getProperty(CSGProp.CANCEL));
            // Initialize handlers.
            dataHandler = new DataHandler(this);
            fileHandler = new FileHandler(this, dataHandler);
            workspaceHandler = new WorkspaceHandler(this, primaryStage);
            styleHandler = new StyleHandler(this);
        }
    }
    
    /**
     * Displays a dialog which allows the user to choose a language.
     */
    private String chooseLanguage() {
        // Create a new alert.
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(SELECT_LANG + "/" + SELECT_LANG_SPAN);
        alert.setHeaderText(null);
        alert.setContentText(SELECT_LANG + "\n" + SELECT_LANG_SPAN);
        
        // Create buttons for the alert.
        ButtonType english = new ButtonType(ENGLISH);
        ButtonType spanish = new ButtonType(SPANISH);
        
        // Add the buttons.
        alert.getButtonTypes().setAll(english, spanish);
        
        // Get and check the result.
        ButtonType result = alert.showAndWait().get();
        if(result == spanish) {
            return SPANISH;
        } else {
            return ENGLISH;
        }
    }
    
    /**
     * Loads properties file.
     * @return true if the properties file loads properly. Otherwise, false.
     */
    private boolean loadProperties(String lang) {
        String path = "";
        if(lang == null) {
            return false;
        } else if(lang.equals(SPANISH)) {
            path = APP_PROPERTIES_SPANISH;
        } else {
            path = APP_PROPERTIES;
        }
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, DATA_PATH);
            props.loadProperties(path, APP_SCHEMA);
            return true;
        } catch (InvalidXMLFileFormatException ex) {
            // TODO: Display error dialog.
            return false;
        }
    }
    
    public jTPS getTPS() {
        return tps;
    }
    
    // GETTERS FOR ALL HANDLERS
    
    public FileHandler getFileHandler() {
        return fileHandler;
    }
    
    public DataHandler getDataHandler() {
        return dataHandler;
    }
    
    public StyleHandler getStyleHandler() {
        return styleHandler;
    }
    
    public WorkspaceHandler getWorkspaceHandler() {
        return workspaceHandler;
    }
    
}