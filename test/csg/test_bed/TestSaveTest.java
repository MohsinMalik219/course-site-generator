/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import csg.data.DataHandler;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.SitePage;
import csg.data.Student;
import csg.data.TeachingAssistant;
import csg.data.Team;
import javafx.beans.property.StringProperty;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mohsin
 */
public class TestSaveTest {
    
    @BeforeClass
    public static void setUpClass() {
        TestSave.loadProperties(TestSave.ENGLISH);
    }

    /**
     * Test of load method, of class TestSave.
     */
    @Test
    public void testLoad() {
        System.out.println("load");
        DataHandler e = TestSave.save();
        DataHandler r = TestSave.load();
        
        assertEquals(e.exportDirProperty().getValue(), r.exportDirProperty().getValue());
        assertEquals(e.templateDirProperty().getValue(), r.templateDirProperty().getValue());
        assertEquals(e.titleProperty().getValue(), r.titleProperty().getValue());
        assertEquals(e.instNameProperty().getValue(), r.instNameProperty().getValue());
        assertEquals(e.instHomeProperty().getValue(), r.instHomeProperty().getValue());
        assertEquals(e.subjectProperty().getValue(), r.subjectProperty().getValue());
        assertEquals(e.semesterProperty().getValue(), r.semesterProperty().getValue());
        assertEquals(e.numberProperty().getValue(), r.numberProperty().getValue());
        assertEquals(e.yearProperty().getValue(), r.yearProperty().getValue());
        assertEquals(e.stylesheetProperty().getValue(), r.stylesheetProperty().getValue());
        assertEquals(e.getBannerImagePath(), r.getBannerImagePath());
        assertEquals(e.getLeftImagePath(), r.getLeftImagePath());
        assertEquals(e.getRightImagePath(), r.getRightImagePath());
        assertEquals(e.getStartHour(), r.getStartHour());
        assertEquals(e.getEndHour(), r.getEndHour());
        assertEquals(e.startingMondayProperty().getValue().toString(), r.startingMondayProperty().getValue().toString());
        assertEquals(e.endingFridayProperty().getValue().toString(), r.endingFridayProperty().getValue().toString());
        assertEquals(e.getPages().size(), r.getPages().size());
        assertEquals(e.getOfficeHourStringProps().size(), r.getOfficeHourStringProps().size());
        assertEquals(e.getTas().size(), r.getTas().size());
        assertEquals(e.getRecitations().size(), r.getRecitations().size());
        assertEquals(e.getScheduleItems().size(), r.getScheduleItems().size());
        assertEquals(e.getTeams().size(), r.getTeams().size());
        assertEquals(e.getStudents().size(), r.getStudents().size());
        
        for(int i = 0; i < e.getPages().size(); i++) {
            SitePage pageE = e.getPages().get(i);
            SitePage pageR = r.getPages().get(i);
            assertEquals(pageE.getIndex(), pageR.getIndex());
            assertEquals(pageE.useProperty().getValue(), pageR.useProperty().getValue());
        }
        
        for(String key : e.getOfficeHourStringProps().keySet()) {
            StringProperty propE = e.getOfficeHourStringProps().get(key);
            StringProperty propR = r.getOfficeHourStringProps().get(key);
            assertEquals(propE.getValue(), propR.getValue());
        }
        
        for(int i = 0; i < e.getTas().size(); i++) {
            TeachingAssistant taE = e.getTas().get(i);
            TeachingAssistant taR = r.getTas().get(i);
            assertEquals(taE.nameProperty().getValue(), taR.nameProperty().getValue());
            assertEquals(taE.emailProperty().getValue(), taR.emailProperty().getValue());
            assertEquals(taE.undergradProperty().getValue(), taR.undergradProperty().getValue());
        }
        
        for(int i = 0; i < e.getRecitations().size(); i++) {
            Recitation recitationE = e.getRecitations().get(i);
            Recitation recitationR = r.getRecitations().get(i);
            assertEquals(recitationE.sectionProperty().getValue(), recitationR.sectionProperty().getValue());
            assertEquals(recitationE.instProperty().getValue(), recitationR.instProperty().getValue());
            assertEquals(recitationE.dayTimeProperty().getValue(), recitationR.dayTimeProperty().getValue());
            assertEquals(recitationE.locationProperty().getValue(), recitationR.locationProperty().getValue());
        }
        
        for(int i = 0; i < e.getScheduleItems().size(); i++) {
            ScheduleItem scheduleItemE = e.getScheduleItems().get(i);
            ScheduleItem scheduleItemR = r.getScheduleItems().get(i);
            assertEquals(scheduleItemE.typeProperty().getValue(), scheduleItemR.typeProperty().getValue());
            assertEquals(scheduleItemE.dateProperty().getValue(), scheduleItemR.dateProperty().getValue());
            assertEquals(scheduleItemE.criteriaProperty().getValue(), scheduleItemR.criteriaProperty().getValue());
            assertEquals(scheduleItemE.linkProperty().getValue(), scheduleItemR.linkProperty().getValue());
            assertEquals(scheduleItemE.topicProperty().getValue(), scheduleItemR.topicProperty().getValue());
            assertEquals(scheduleItemE.titleProperty().getValue(), scheduleItemR.titleProperty().getValue());
            assertEquals(scheduleItemE.timeProperty().getValue(), scheduleItemR.timeProperty().getValue());
        }
        
        for(int i = 0; i < e.getTeams().size(); i++) {
            Team teamE = e.getTeams().get(i);
            Team teamR = r.getTeams().get(i);
            assertEquals(teamE.nameProperty().getValue(), teamR.nameProperty().getValue());
            assertEquals(teamE.colorProperty().getValue(), teamR.colorProperty().getValue());
            assertEquals(teamE.textColorProperty().getValue(), teamR.textColorProperty().getValue());
            assertEquals(teamE.linkProperty().getValue(), teamR.linkProperty().getValue());
        }
        
        for(int i = 0; i < e.getStudents().size(); i++) {
            Student studentE = e.getStudents().get(i);
            Student studentR = r.getStudents().get(i);
            assertEquals(studentE.firstNameProperty().getValue(), studentR.firstNameProperty().getValue());
            assertEquals(studentE.lastNameProperty().getValue(), studentR.lastNameProperty().getValue());
            assertEquals(studentE.teamProperty().getValue(), studentR.teamProperty().getValue());
            assertEquals(studentE.roleProperty().getValue(), studentR.roleProperty().getValue());
        }
    }
    
}